package chesslogic;

public class Bishop extends Piece {
    public Bishop(Point point, char type, boolean color) {
        super(point, type, color);
    }

    public Bishop(Bishop other) {
        super(other._point, other._type, other._color);
    }

    @Override
    OpCodes validateMove(Board boardArr, int new_x, int new_y) {
        if (new_x == _point.get_x() && new_y == _point.get_y()){
            return OpCodes.SAME_INDEX;
        }
        else if(boardArr.getPiece(new Point(new_x, new_y )) != null && _color == boardArr.getPiece(new Point(new_x, new_y ))._color){
            return OpCodes.SELF_PLAYER;
        }
        else if(Math.abs(new_x - _point.get_x()) != Math.abs(new_y - _point.get_y())){
            return OpCodes.INVALID_MOVE;
        }

        else if ((new_x > _point.get_x() && new_y < _point.get_y()) ||
                (new_x < _point.get_x() && new_y > _point.get_y())){
            if (ChessHelper.positiveIncline(boardArr, new_x, new_y, _point) == OpCodes.INVALID_MOVE)
                return OpCodes.INVALID_MOVE;
        }
        else{
            if (ChessHelper.negativeIncline(boardArr, new_x, new_y, _point) == OpCodes.INVALID_MOVE)
                return OpCodes.INVALID_MOVE;
        }

        return OpCodes.VALID;
    }

    @Override
    public Piece clone() {
        return new Bishop(this);
    }
}

