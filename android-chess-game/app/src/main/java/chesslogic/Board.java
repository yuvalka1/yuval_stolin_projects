package chesslogic;

import android.text.Spannable;
import android.util.Log;

import java.util.Collection;
import java.util.Collections;

import chesslogic.Bishop;

public class Board {
    private final String TAG = "BOARD";
    public  static  final int BOARD_SIZE = 8;
    public static final int ROW_WHITE  = 1;
    public static final int ROW_BLACK  = 6;

    Piece[][] boardArr;
    public Board(String boardStr){
        boardArr = new Piece[BOARD_SIZE][Board.BOARD_SIZE];
        resetBoard(boardStr);
    }

    public Board(Board board){
        copy(board.boardArr);
    }

    public void copy(Piece[][] temp)  {
        if(boardArr == null) {
            this.boardArr = new Piece[BOARD_SIZE][Board.BOARD_SIZE];
        }
        for(int i = 0 ; i < BOARD_SIZE; i++){
            for (int j = 0; j  < BOARD_SIZE; j++){
                Piece p = temp[i][j];
                if (i == 3 && j == 3){
                    Log.d(TAG, "debug");
                }
                if(p == null){
                    this.boardArr[i][j] = null;
                    continue;
                }
                boardArr[i][j] = p.clone();
            }
        }
    }

    public Piece[][] getBoardArr() {
        return boardArr;
    }


    public void resetBoard(String boardStr){
        int index = 0;
        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                Point point = new Point(i, j);
                char type = boardStr.charAt(index);
                switch (type)
                {
                    case 'r': // lowwercase - white
                        boardArr[i][j] = new Rook(point, type, true);
                        break;
                    case 'n':
                        boardArr[i][j] = new Knight(point, type, true);
                        break;
                    case 'b':
                        boardArr[i][j] = new Bishop(point, type, true);
                        break;
                    case 'k':
                        boardArr[i][j] = new King(point, type, true);
                        break;
                    case 'q':
                        boardArr[i][j] = new Queen(point, type, true);
                        break;
                    case 'p':
                        boardArr[i][j] = new Pawn(point, type, true);
                        break;
                    case 'R': // uppercase - black
                        boardArr[i][j] = new Rook(point, type, false);
                        break;
                    case 'N':
                        boardArr[i][j] = new Knight(point, type, false);
                        break;
                    case 'B':
                        boardArr[i][j] = new Bishop(point, type, false);
                        break;
                    case 'K':
                        boardArr[i][j] = new King(point, type, false);
                        break;
                    case 'Q':
                        boardArr[i][j] = new Queen(point, type, false);
                        break;
                    case 'P':
                        boardArr[i][j] = new Pawn(point, type, false);
                        break;
                    case '#':
                        boardArr[i][j] = null;
                        break;
                    default:
                        Log.d(TAG, "Invalid char in board arr"); //maybe should change to exception

                }
                index++;
            }
        }
    }



    Piece getPiece(Point point){
        return boardArr[point.get_x()][point.get_y()];
    }

    void setPiece(Point new_point, Piece piece){
        this.boardArr[new_point.get_x()][new_point.get_y()] = piece;
    }

    public void setPiece(Point src, Point dst){
        this.boardArr[dst.get_x()][dst.get_y()] = this.boardArr[src.get_x()][src.get_y()];
        this.boardArr[src.get_x()][src.get_y()] = null;
        this.boardArr[dst.get_x()][dst.get_y()].setNewPoint(dst);
    }

    void setPieceWhitoutHistory(Point src, Point dst){
        this.boardArr[dst.get_x()][dst.get_y()] = this.boardArr[src.get_x()][src.get_y()];
        this.boardArr[src.get_x()][src.get_y()] = null;
    }
}
