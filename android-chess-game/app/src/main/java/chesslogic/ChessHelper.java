package chesslogic;

public class ChessHelper {

    public static OpCodes clearPathRow(Board boardArr, int new_x, int new_y, Point _point){
        for(int i = Math.min(new_y, _point.get_y()) + 1 ; i < Math.max(new_y, _point.get_y()); i++){
            Piece piece = boardArr.getPiece(new Point(_point.get_x(), i));
            if (piece != null){
                return OpCodes.INVALID_MOVE;
            }
        }
        return OpCodes.VALID;
    }

    public static OpCodes clearPathCol(Board boardArr, int new_x, int new_y, Point _point){
        for(int i = Math.min(new_x, _point.get_x()) + 1 ; i < Math.max(new_x, _point.get_x()); i++){
            Piece piece = boardArr.getPiece(new Point(i, new_y));
            if (piece != null){
                return OpCodes.INVALID_MOVE;
            }
        }
        return OpCodes.VALID;
    }

    private static OpCodes enpassent(Piece piece, boolean _color){
        if (piece == null) {
            return OpCodes.INVALID_MOVE;
        }
        else if (piece._color != _color && Character.toLowerCase(piece.get_type()) == 'p')
        {
            Pawn pawn = (Pawn) piece.clone();
            Move lastMove = Game.lastMove;

            Move pawnJumpingTwoStepsMove = new Move(new Point(!Game.boardDirection ? Board.ROW_WHITE: Board.ROW_BLACK, pawn._point.get_y()), pawn._point);
            if (!lastMove.equals(pawnJumpingTwoStepsMove)){
                return OpCodes.INVALID_MOVE;
            }
            return pawn.isJumped() && !pawn.isCan_jump_two_steps() ? OpCodes.ENPASSENT :
                    OpCodes.INVALID_MOVE;
        }
        return OpCodes.INVALID_MOVE;
    }

    public static OpCodes enpassent(Board boardArr, int new_x, int new_y, Point _point, boolean _color){
        Piece p = boardArr.getPiece(new Point(new_x, new_y));
        int y = _point.get_y() - new_y > 0? _point.get_y() - 1: _point.get_y() + 1;
        Move lastMove = Game.lastMove;



        return enpassent(boardArr.getPiece(new Point(_point.get_x(), y)), _color);
    }

    public static OpCodes negativeIncline(Board boardArr, int new_x, int new_y, Point _point){
        int row = Math.min(new_x, _point.get_x()) +1;
        int col = Math.min(new_y, _point.get_y()) + 1;
        for(row = row; row < Math.max(new_x, _point.get_x()); row++){
            Piece piece = boardArr.getPiece(new Point(row, col));
            if (piece != null){
                return OpCodes.INVALID_MOVE;
            }
            col++;
        }
        return OpCodes.VALID;
    }

    public static OpCodes positiveIncline(Board boardArr, int new_x, int new_y, Point _point){
        int row = Math.min(new_x, _point.get_x()) + 1;
        int col = Math.max(new_y, _point.get_y()) - 1;
        for(row = row; row <  Math.max(new_x, _point.get_x()); row++){
            Piece piece = boardArr.getPiece(new Point(row, col));
            if (piece != null){
                return OpCodes.INVALID_MOVE;
            }
            col--;
        }
        return OpCodes.VALID;
    }


}
