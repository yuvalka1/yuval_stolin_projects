package chesslogic;

import android.content.Context;
import android.util.Log;

import org.eazegraph.lib.charts.PieChart;

import java.util.ArrayList;

public class Game {
    private Board _gameBoard;
    private Board _boardNextMove;
    private boolean _turn; // true - white, false - black
    private Point whiteKingLocation;
    private Point blackKingLocation;
    private final int KingFirstLocation = 3;
    private final int MIN_INDEX = 0;
    private final String TAG = "Game";
    private ArrayList<Piece> attackers;

    public static Move lastMove;
    public static boolean boardDirection;


    Context context;

    public Game(String boardStr, boolean starting, Context context){
        this.context = context;
        lastMove = null;
        boardDirection = Character.isLowerCase(boardStr.charAt(0));
        _gameBoard = new Board(boardStr);
        _boardNextMove = new Board(boardStr);
        _turn = starting;
        if (boardDirection) {
            whiteKingLocation = new Point(0, KingFirstLocation);
            blackKingLocation = new Point(Board.BOARD_SIZE - 1, KingFirstLocation);
        }
        else {
            blackKingLocation = new Point(0, KingFirstLocation+1);
            whiteKingLocation = new Point(Board.BOARD_SIZE - 1, KingFirstLocation+1);
        }
    }

    public Game(String boardStr){//review game
        boardDirection = Character.isLowerCase(boardStr.charAt(0));
        _gameBoard = new Board(boardStr);
    }



    public void play_without_validation(Move move){
        Point src = move.get_src();
        Point dst = move.get_dst();
        String promoting = move.getPromotingPiece();
        OpCodes code = move.getMoveCode();
        if(promoting != null && !promoting.equals("")){
            code = OpCodes.PROMOTING;
        }
        switch (code){
            case CASTLING:
                int rook_location_y, dst_rook_location;
                if (dst.get_y() < src.get_y()){
                    rook_location_y = 0;
                    dst_rook_location = src.get_y() - 1;
                }
                else{
                    rook_location_y =  Board.BOARD_SIZE - 1;
                    dst_rook_location = src.get_y() +1;
                }
                Point rook_point = new Point(src.get_x(), rook_location_y);
                Point rook_dst = new Point(src.get_x(), dst_rook_location);

                _gameBoard.setPiece(src, dst);
                King k = (King)(_gameBoard.getPiece(dst));
                k.setCastled(true);
                _gameBoard.setPiece(dst, k);

                _gameBoard.setPiece(rook_point,rook_dst);
                Rook r = (Rook)(_gameBoard.getPiece(rook_dst));
                r.setCastled(true);
                _gameBoard.setPiece(rook_dst, r);

                break;
            case VALID:
            case CHECK:
            case MATE:
            case EAT:
            case STALEMATE:
                _gameBoard.setPiece(src, dst);
                break;
            case ENPASSENT:
                _gameBoard.setPiece(src, dst);
                _gameBoard.setPiece(new Point(src.get_x(), dst.get_y()), (Piece) null);
                break;

            case PROMOTING:
                Piece piece;
                char type = move.getPromotingPiece().charAt(0);
                switch (Character.toLowerCase(type)){
                    case 'r':
                        piece = new Rook(dst, type, !_turn);
                        break;
                    case 'n':
                        piece = new Knight(dst, type, !_turn);
                        break;
                    case 'b':
                        piece = new Bishop(dst, type, !_turn);
                        break;
                    case 'q':
                        piece = new Queen(dst, type, !_turn);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + Character.toLowerCase(type));
                }
                _gameBoard.setPiece(dst, piece);
                _gameBoard.setPiece(src, (Piece) null);
                break;
            default:
                break;
        }
    }

    public void play(Move move){
        Point src = move.get_src();
        Point dst = move.get_dst();
        attackers = new ArrayList<>();
        ArrayList<OpCodes> moveCodesForNotation = new ArrayList<>();
        boolean move_successful = true, king_location_updated = false;
        //Log.d(TAG, _turn? "white move" : "black move");
        Log.d(TAG, "src = " + src.toString() + "    dst = " + dst.toString());
        _boardNextMove.copy(_gameBoard.getBoardArr());


        OpCodes code = turnValidation(src);
        if(code == OpCodes.INVALID_PLAYER){move.setMoveCode(code);return;}

        code = _boardNextMove.getPiece(src).validateMove(_boardNextMove, dst.get_x(), dst.get_y());
        moveCodesForNotation.add(code);
        move.setMoveCode(code);
        switch (code){
            case CASTLING:
                //if (previous == OpCodes.CHECK){return OpCodes.INVALID_MOVE;}
                int rook_location_y, dst_rook_location, start, end;
                if (dst.get_y() < src.get_y()){
                    rook_location_y = 0;
                    dst_rook_location = src.get_y() - 1;
                    start = 1;
                    end = src.get_y();
                }
                else{
                    rook_location_y =  Board.BOARD_SIZE - 1;
                    dst_rook_location = src.get_y() + 1;
                    start = src.get_y();
                    end = dst.get_y();
                }

                Point rook_point = new Point(src.get_x(), rook_location_y);
                Point rook_dst = new Point(src.get_x(), dst_rook_location);
                boolean king_color = _boardNextMove.getPiece(src)._color;

                for(int i = start; i <= end; i++){
                    Point new_location = new Point(src.get_x(), i);
                    if (checkAttackOnKing(new_location, king_color)){
                        code= OpCodes.INVALID_PLAYER;
                        move.setMoveCode(code);
                        return;

                    }

                }

                _boardNextMove.setPiece(src, dst);
                King k = (King)(_boardNextMove.getPiece(dst));
                k.setCastled(true);
                _boardNextMove.setPiece(dst, k);

                _boardNextMove.setPiece(rook_point,rook_dst);
                Rook r = (Rook)(_boardNextMove.getPiece(rook_dst));
                r.setCastled(true);
                _boardNextMove.setPiece(rook_dst, r);

                break;
            case VALID:
                if(_boardNextMove.getPiece(dst) != null){
                    moveCodesForNotation.set(0, OpCodes.EAT);}
                _boardNextMove.setPiece(src, dst);
                break;
            case ENPASSENT:
                moveCodesForNotation.add(OpCodes.EAT);
                _boardNextMove.setPiece(src, dst);
                _boardNextMove.setPiece(new Point(src.get_x(), dst.get_y()), (Piece) null);
                break;

            case PROMOTING:
                if(_boardNextMove.getPiece(dst) != null){moveCodesForNotation.add(OpCodes.EAT);}
                _boardNextMove.setPiece(src, dst);
                move.setMoveCode(OpCodes.PROMOTING);
                if(checkAttackOnKing(_turn)){move.setMoveCode(OpCodes.INVALID_PLAYER);}
                return;
            default:
                move_successful = false;
        }

        if (move_successful) {
            //update king location
            if (_boardNextMove.getPiece(dst)._type == 'k') {
                whiteKingLocation.set_x(dst.get_x());
                whiteKingLocation.set_y(dst.get_y());
                king_location_updated = true;
            } else if (_boardNextMove.getPiece(dst)._type == 'K') {
                blackKingLocation.set_x(dst.get_x());
                blackKingLocation.set_y(dst.get_y());
                king_location_updated = true;
            }

            //check for self check
            if (OpCodes.CASTLING != code && checkAttackOnKing(_turn)) {
                code = OpCodes.SELF_PLAYER;
                move.setMoveCode(code);
            }
            //check for check
            else if (checkAttackOnKing(!_turn)) {
                code = OpCodes.CHECK;
                if(is_checkmate(!_turn)){
                    code = OpCodes.MATE;
                }
                moveCodesForNotation.add(code);
                move.setMoveCode(code);
            }
            //check for stalemate
            else if(is_stalemate()){
                code = OpCodes.STALEMATE;
            }

        }
        if (code != OpCodes.SELF_PLAYER && move_successful) {
            //_turn = !_turn;
            _gameBoard.copy(_boardNextMove.getBoardArr());
            move.setMoveNotaion(getMoveNotaion(move, moveCodesForNotation));
        }
        else if(king_location_updated) {
            if (_boardNextMove.getPiece(dst)._type == 'k') {
                whiteKingLocation.set_x(src.get_x());
                whiteKingLocation.set_y(src.get_y());
            } else {
                blackKingLocation.set_x(src.get_x());
                blackKingLocation.set_y(src.get_y());
            }
        }

        Log.d(TAG, "results =  " + code.name());

        printBoard();
    }

    public void executePromotion(Move move, char selectedPiece){
        Point src = move.get_src();
        Point dst = move.get_dst();
        ArrayList<OpCodes> moveCodesForNotation = new ArrayList<>();
        OpCodes code;
        moveCodesForNotation.add(move.getMoveCode());
        if(_gameBoard.getPiece(dst) != null){moveCodesForNotation.add(OpCodes.EAT);}
        Piece piece;
        Log.d(TAG, String.valueOf(selectedPiece));
        switch (selectedPiece){
            case 'r':
                piece = new Rook(dst, selectedPiece, _turn);
                break;
            case 'n':
                piece = new Knight(dst, selectedPiece, _turn);
                break;
            case 'b':
                piece = new Bishop(dst, selectedPiece, _turn);
                break;
            case 'q':
                piece = new Queen(dst, selectedPiece, _turn);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + selectedPiece);
        }
        move.setPromotingPiece(String.valueOf(selectedPiece));
        _boardNextMove.setPiece(dst, piece);
        if (checkAttackOnKing(!_turn)) {
            code = OpCodes.CHECK;
            if(is_checkmate(!_turn)){
                code = OpCodes.MATE;
            }
            moveCodesForNotation.add(code);
            move.setMoveCode(code);
        }
        _gameBoard.copy(_boardNextMove.getBoardArr());
        move.setMoveNotaion(getMoveNotaion(move, moveCodesForNotation));
    }

    public void setOpponentMove(Move move){
        move.reverse();
        Point src = move.get_src();
        Point dst = move.get_dst();

        String promoting = move.getPromotingPiece();
        OpCodes code = move.getMoveCode();
        if(promoting != null){
            code = OpCodes.PROMOTING;
        }
        switch (code){
            case CASTLING:
                int rook_location_y, dst_rook_location;
                if (dst.get_y() < src.get_y()){
                    rook_location_y = 0;
                    dst_rook_location = src.get_y() - 1;
                }
                else{
                    rook_location_y =  Board.BOARD_SIZE - 1;
                    dst_rook_location = src.get_y() +1;
                }
                Point rook_point = new Point(src.get_x(), rook_location_y);
                Point rook_dst = new Point(src.get_x(), dst_rook_location);

                _gameBoard.setPiece(src, dst);
                King k = (King)(_gameBoard.getPiece(dst));
                k.setCastled(true);
                _gameBoard.setPiece(dst, k);

                _gameBoard.setPiece(rook_point,rook_dst);
                Rook r = (Rook)(_gameBoard.getPiece(rook_dst));
                r.setCastled(true);
                _gameBoard.setPiece(rook_dst, r);

                break;
            case VALID:
            case CHECK:
            case MATE:
            case EAT:
            case STALEMATE:
                _gameBoard.setPiece(src, dst);
                break;
            case ENPASSENT:
                _gameBoard.setPiece(src, dst);
                _gameBoard.setPiece(new Point(src.get_x(), dst.get_y()), (Piece) null);
                break;

            case PROMOTING:
                Piece piece;
                char type = move.getPromotingPiece().charAt(0);
                switch (Character.toLowerCase(type)){
                    case 'r':
                        piece = new Rook(dst, type, !_turn);
                        break;
                    case 'n':
                        piece = new Knight(dst, type, !_turn);
                        break;
                    case 'b':
                        piece = new Bishop(dst, type, !_turn);
                        break;
                    case 'q':
                        piece = new Queen(dst, type, !_turn);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + Character.toLowerCase(type));
                }
                _gameBoard.setPiece(dst, piece);
                _gameBoard.setPiece(src, (Piece) null);
                break;
            default:
                break;
        }
        if (_gameBoard.getPiece(dst)._type == 'k' && whiteKingLocation != null) {
            whiteKingLocation.set_x(dst.get_x());
            whiteKingLocation.set_y(dst.get_y());
        } else if (_gameBoard.getPiece(dst)._type == 'K' && blackKingLocation != null) {
            blackKingLocation.set_x(dst.get_x());
            blackKingLocation.set_y(dst.get_y());
        }
    }

    public String getMoveNotaion(Move move, ArrayList<OpCodes> code){
        Point dst = move.get_dst();
        char eat = '\0', check = '\0', mate = '\0';
        char colName, moveType = '\0';
        int row = dst.get_x() + 1;
        int col = dst.get_y();
        String casteling = "", promoting = "";

        char type = Character.toUpperCase(_gameBoard.getPiece(dst)._type);

        if (type == 'P'){type = Character.MIN_VALUE;}
        if(!_turn)// player is black
        {
            colName = (char)(Point.reverseIndex(col) + 65);
        }
        else{
            row = Point.reverseIndex(row - 1) + 1;
            colName = (char)(col + 65);
        }

        if(code.contains(OpCodes.CASTLING)){
            if (!boardDirection){ casteling =  move.get_src().get_y() < col ? "O-O" : "O-O-O";}
            else {casteling = move.get_src().get_y() > col ? "O-O" : "O-O-O";}
        }
        if(code.contains(OpCodes.EAT)){
            eat = 'x';
        }
        if(code.contains(OpCodes.CHECK)){check = '+';}
        else if(code.contains(OpCodes.MATE)){
            mate = '#';
        }
        if (code.contains(OpCodes.PROMOTING)){
            promoting = "=" + Character.toUpperCase(move.getPromotingPiece().charAt(0)) + " ";
        }

        return casteling.equals("") ? type + String.valueOf(eat) + String.valueOf(colName) + Integer.toString(row) + promoting +
                String.valueOf(check) + String.valueOf(mate) : casteling +  String.valueOf(check) + String.valueOf(mate);
    }

    public boolean checkSelfPiece(Point point){
        Piece piece = _gameBoard.getPiece(point);
        return piece._color == _turn;
    }

    public boolean pieceExist(Point point){return  _gameBoard.getPiece(point) != null;}

    public Board get_gameBoard() {
        return new Board(_gameBoard);
    }

    public void printBoard()
    {
        StringBuilder board = new StringBuilder();
        for (int i = 0; i < Board.BOARD_SIZE; i++)
        {
            for (int j = 0; j < Board.BOARD_SIZE; j++)
            {
                Piece piece = _gameBoard.getPiece(new Point(i, j));
                if (piece != null)
                    board.append(piece._type).append(" ");
			    else
                    board.append("# ");
            }
            Log.d(TAG, String.valueOf(board));
            board = new StringBuilder();
        }
    }

    public boolean is_turn() {
        return _turn;
    }

    private boolean is_stalemate(){
        for(Piece[] pieces: this._gameBoard.getBoardArr()){
            for(Piece piece: pieces){
                if(piece == null || piece._color == this._turn){
                    continue;
                }
                else{
                    for(int i = 0; i < Board.BOARD_SIZE; i++){
                        for(int j = 0; j < Board.BOARD_SIZE; j++){
                            if(piece.validateMove(this._boardNextMove, i, j) == OpCodes.VALID){
                                if(Character.toLowerCase(piece._type) == 'k'){
                                    Point king_new_location = new Point(i, j);
                                    Point king_old_location = new Point(piece._point);
                                    Piece old_piece = _boardNextMove.getPiece(king_new_location);
                                    _boardNextMove.setPiece(king_old_location, king_new_location);//try move
                                    boolean check = checkAttackOnKing(king_new_location, piece._color);
                                    _boardNextMove.setPiece(king_new_location, king_old_location);//undo move
                                    if (old_piece != null){
                                        _boardNextMove.setPiece(king_new_location, old_piece);
                                    }
                                    if (!check) {
                                        return false;
                                    }
                                }
                                else {
                                    return false;
                                }
                            }

                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean is_checkmate(final boolean whichKing){
        Piece king = whichKing ? this._boardNextMove.getPiece(whiteKingLocation):
                this._boardNextMove.getPiece(blackKingLocation);
        final int[][] kingmoves = { {1,-1},{1,0},{1,1},{-1,-1},{-1,0},{-1,1},{0,-1},{0,1} };
        //can king move
        boolean can_king_move = false;
        int king_new_x = 0;
        int king_new_y = 0;
        int i = 0;//check if king can move
        for(int[] move : kingmoves){
            king_new_x = king._point.get_x() + move[0];
            king_new_y = king._point.get_y() + move[1];
            if(!(king_new_x < 0 || king_new_x >= Board.BOARD_SIZE || king_new_y < 0 || king_new_y >= Board.BOARD_SIZE)) {
                if (king.validateMove(_boardNextMove, king_new_x, king_new_y) == OpCodes.VALID) {
                    Point king_new_location = new Point(king_new_x, king_new_y);
                    Point king_old_location = new Point(king._point);
                    Piece old_piece = _boardNextMove.getPiece(king_new_location);
                    _boardNextMove.setPiece(king_old_location, king_new_location);//try move
                    boolean check = checkAttackOnKing(king_new_location, king._color);
                    _boardNextMove.setPiece(king_new_location, king_old_location);//undo move
                    if (old_piece != null){
                        _boardNextMove.setPiece(king_new_location, old_piece);
                    }
                    if (!check) {
                        return false;
                    }
                }
            }
        }

        if(attackers.size() > 1){return true;}
        else{// if one attacker check if piece can be eaten of blocked
            Piece attacker = attackers.get(0);
            //check if attacker can be eaten
            for (Piece[] row : _boardNextMove.boardArr) {
                for (Piece piece : row) {
                    if (piece == null || piece._color == attacker._color || Character.toLowerCase(piece._type) == 'k') {
                        continue;
                    }
                    if (piece.validateMove(_boardNextMove, attacker._point.get_x(), attacker._point.get_y()) == OpCodes.VALID) {
                        //check if moves cuases check
                        _boardNextMove.setPiece(piece._point, attacker._point);//do move
                        boolean check = checkAttackOnKing(whichKing);
                        _boardNextMove.setPiece(attacker._point, piece._point);//undo move
                        _boardNextMove.setPiece(attacker._point, attacker);
                        if (!check) {
                            return false;
                        }
                    }
                }
            }
            //check if attacker can be blocked
            if (Character.toLowerCase(attacker._type) == 'n' || Character.toLowerCase(attacker._type) == 'p' ){return true;}
            if(Math.abs(king._point.get_x() - attacker._point.get_x()) == Math.abs(attacker._point.get_y() - king._point.get_y())){
                //attacker may be bishop or queen
                //attacking in hypotenuse

                if ((attacker._point.get_x() > king._point.get_x() && attacker._point.get_y() < king._point.get_y()) ||
                        ((attacker._point.get_x() < king._point.get_x() && (attacker._point.get_y() > king._point.get_y())))){
                    int row = Math.min(attacker._point.get_x(),king._point.get_x()) + 1;
                    int col = Math.max(attacker._point.get_y(), king._point.get_y()) - 1;
                    for(row = row; row <  Math.max(attacker._point.get_x(), king._point.get_x()); row++){
                        Point point = pointCanBeBlocked(row, col, !king._color);
                        if(point != null &&  Character.toLowerCase(_boardNextMove.getPiece(point)._type) != 'k'){
                            _boardNextMove.setPiece(point, new Point(row, col));
                            boolean check = checkAttackOnKing(whichKing);
                            _boardNextMove.setPiece(new Point(row, col), point);
                            if (!check){ return false;}
                        }
                    }

                }
                else{
                    int row = Math.min(attacker._point.get_x(), king._point.get_x()) +1;
                    int col = Math.min(attacker._point.get_y(), king._point.get_y()) + 1;
                    for(row = row; row < Math.max(attacker._point.get_x(), king._point.get_x()); row++){
                        Point point = pointCanBeBlocked(row, col, !king._color);
                        if(point != null &&  Character.toLowerCase(_boardNextMove.getPiece(point)._type) != 'k'){
                            _boardNextMove.setPiece(point, new Point(row, col));
                            boolean check = checkAttackOnKing(whichKing);
                            _boardNextMove.setPiece(new Point(row, col), point);
                            if (!check){ return false;}
                        }
                    }
                }

            }
            else{
                //attacker may be rook or queen
                //attacking in straight line
                if(king._point.get_y() == attacker._point.get_y()) // same column
                {
                    for(i = Math.min(king._point.get_x() , attacker._point.get_x()) + 1 ; i < Math.max(attacker._point.get_x(), king._point.get_x()); i++){
                        Point point = pointCanBeBlocked(i, king._point.get_y(), !king._color);
                        if(point != null &&  Character.toLowerCase(_boardNextMove.getPiece(point)._type) != 'k'){
                            _boardNextMove.setPiece(point, new Point(i, king._point.get_y()));
                            boolean check = checkAttackOnKing(whichKing);
                            _boardNextMove.setPiece(new Point(i, king._point.get_y()), point);
                            if (!check){ return false;}
                        }
                    }
                }
                else{//row
                    for(i = Math.min(king._point.get_y() , attacker._point.get_y()) + 1 ; i < Math.max(attacker._point.get_y(), king._point.get_y()); i++){
                        Point point = pointCanBeBlocked( king._point.get_x(), i,  !king._color);
                        if(point != null && Character.toLowerCase(_boardNextMove.getPiece(point)._type) != 'k'){
                            _boardNextMove.setPiece(point, new Point(king._point.get_x(), i));//do move
                            boolean check = checkAttackOnKing(whichKing);
                            _boardNextMove.setPiece(new Point(king._point.get_x(), i), point);//undo move
                            if (!check){return  false;}
                        }
                    }
                }

            }


        }

        return true;

    }

    private Point pointCanBeBlocked(int attack_x, int attack_y, boolean color){
        for(Piece[] row : _boardNextMove.boardArr){
            for(Piece piece: row){
                if(piece == null || piece._color == color){continue;}
                if(piece.validateMove(_boardNextMove, attack_x, attack_y) == OpCodes.VALID){
                    return piece._point;
                }
            }
        }
        return null;
    }

    private boolean checkAttackOnKing(final Point kinglocation, boolean color){
        for(Piece [] row : this._boardNextMove.getBoardArr()){
            for(Piece piece : row){
                if (piece != null){
                    if (piece._color == color){
                        continue;
                    }
                    if (piece.validateMove(_boardNextMove, kinglocation.get_x(), kinglocation.get_y()) == OpCodes.VALID){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkAttackOnKing(final boolean whichKing){
        attackers = new ArrayList<>();
        Piece king = whichKing ? this._boardNextMove.getPiece(whiteKingLocation):
                this._boardNextMove.getPiece(blackKingLocation);
        for(Piece [] row : this._boardNextMove.getBoardArr()){
            for(Piece piece : row){
                if (piece != null){

                    if (piece._color == king._color){
                       continue;
                    }
                    if (piece.validateMove(_boardNextMove, king._point.get_x(), king._point.get_y()) == OpCodes.VALID){
                        this.attackers.add(piece);
                        //return true;
                    }
                    if(attackers.size() == 2){return true;}

                }
            }
        }
        return attackers.size() > 0;
    }

    private OpCodes turnValidation(Point src){
        if(this._boardNextMove.getPiece(src) == null){
            return OpCodes.INVALID_PLAYER;
        }
        return OpCodes.VALID;
    }


}
