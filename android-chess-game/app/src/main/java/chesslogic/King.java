package chesslogic;

public class King extends Piece {

    private boolean castled = false;

    public King(Point point, char type, boolean color) {
        super(point, type, color);
    }

    public King(King other) {
        super(other._point, other._type, other._color);
        this.castled = other.castled;
    }


    public void setCastled(boolean castled) {
        this.castled = castled;
    }

    private OpCodes checkCasteling(Board boardArr , int rook_x, int rook_y, int new_x, int new_y) {
        Piece rook = boardArr.getPiece(new Point(rook_x, rook_y));
        if (rook == null) {//cant castled without rook
            return OpCodes.INVALID_MOVE;

        } else if (Character.toLowerCase(rook._type) != 'r' || rook._color != _color) {
            return OpCodes.INVALID_MOVE; //cant castled with different object
        }
        try {
            Rook r = (Rook) rook;
            if (r.isCastled()) {
                return OpCodes.INVALID_MOVE;
            }// cant castled if rook already moved
        } catch (Exception e) {
            return OpCodes.INVALID_MOVE;
        }

        for(int i = Math.min(_point.get_y(), new_y) + 1; i < _point.get_y();i++){
            if (boardArr.getPiece(new Point(_point.get_x(), i)) != null){
                return OpCodes.INVALID_MOVE;
            }
        }

        return OpCodes.CASTLING;
    }


    @Override
    OpCodes validateMove(Board boardArr, int new_x, int new_y){

        if (new_x == _point.get_x() && new_y == _point.get_y()){//same index play
            return OpCodes.SAME_INDEX;
        }
        else if(boardArr.getPiece(new Point(new_x, new_y)) != null //king play on self solder
                && boardArr.getPiece(new Point(new_x, new_y))._color == this._color){
            return OpCodes.SELF_PLAYER;
        }
        else if(Math.abs(new_x - _point.get_x()) > 1 || Math.abs(new_y - _point.get_y()) > 2){
            return OpCodes.INVALID_MOVE;
        }
        //move will cause two king be near each other
        //else if (findKing(boardArr, new_x, new_y) == OpCodes.INVALID_MOVE){
        //    return OpCodes.INVALID_MOVE;
        //}
        else if(Math.abs(new_y - _point.get_y()) == 2) // casteling
        {
            if (castled) {
                return OpCodes.INVALID_MOVE;
            }
            else if(new_y < _point.get_y()){ // castling left
                return checkCasteling(boardArr,_point.get_x() ,0, new_x, new_y);
            }
            else{// castling right
                return checkCasteling(boardArr, _point.get_x(), Board.BOARD_SIZE - 1, new_x, new_y);
            }

        }
        return OpCodes.VALID;

    }

    @Override
    public Piece clone() {
        return new King(this);
    }




    /*
    private OpCodes checkForCheck(Board boardArr, int new_x,int new_y){

        for (Piece[] row : boardArr.getBoardArr()) {
            for (Piece piece : row) {
                if (piece == null || piece._color == this._color){continue;}

                if (piece.validateMove(boardArr, new_x, new_y) == OpCodes.VALID) {
                    return OpCodes.INVALID_MOVE;
                }
            }
        }

        return OpCodes.VALID;
    }
    private OpCodes findKing(Board boardArr, int new_x, int new_y){
        int kingPosition[][] = { {1, -1}, {1, 0}, {1, 1}, {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}};

        //check if king moves near to king
        Point point = new Point(new_x, new_y);
        for (int[] a : kingPosition) {

            if(point.get_y() + a[1] > Board.BOARD_SIZE-1 || point.get_y() + a[1] < 0 ||
                    point.get_x() + a[0] > Board.BOARD_SIZE-1 || point.get_x() + a[0] < 0){
                continue;
            }
            Piece piece = boardArr.getPiece(new Point(point.get_x() + a[0],
                    point.get_y() + a[1]));
            if (piece != null && (piece._type == 'k' || piece._type == 'K')) {
                return OpCodes.INVALID_MOVE;
            }
        }
        return OpCodes.VALID;
    }

     */

}
