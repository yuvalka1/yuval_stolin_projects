package chesslogic;

import org.jetbrains.annotations.NotNull;

public class Knight extends Piece {
    private final int ROW_DIFFERENCE = 1;
    private final int COL_DIFFERENCE = 2;

    public Knight(Point point, char type, boolean color) {
        super(point, type, color);
    }

    public Knight(Knight other) {
        super(other._point, other._type, other._color);

    }

    @Override
    OpCodes validateMove(Board boardArr, int new_x, int new_y) {
        if (new_x == _point.get_x() && new_y == _point.get_y()){
            return OpCodes.SAME_INDEX;
        }
        else if(boardArr.getPiece(new Point(new_x, new_y )) != null && _color == boardArr.getPiece(new Point(new_x, new_y ))._color){
            return OpCodes.SELF_PLAYER;
        }
        else if (!((Math.abs(_point.get_x() - new_x) == ROW_DIFFERENCE &&
                Math.abs(_point.get_y() - new_y) == COL_DIFFERENCE) ||
                (Math.abs(_point.get_x() - new_x) == COL_DIFFERENCE &&
                        Math.abs(_point.get_y() - new_y) == ROW_DIFFERENCE)))
        {
            return OpCodes.INVALID_MOVE;
        }
        return OpCodes.VALID;
    }

    @NotNull
    @Override
    public Knight clone() {
        return new Knight(this);
    }
}
