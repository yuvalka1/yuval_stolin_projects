package chesslogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Move {
    private Point _src;
    private Point _dst;
    private String _moveNotaion;
    private String promotingPiece;
    private OpCodes moveCode;


    public Move(){}
    public Move(Point src, Point dst){
        _src = new Point(src);
        _dst = new Point(dst);
    }

    public Move(int src_x, int src_y, int dst_x, int dst_y){
        _src = new Point(src_x, src_y);
        _dst = new Point(dst_x, dst_y);
    }

    public Move(Point src, Point dst, String moveNotaion) {
        this._src = src;
        this._dst = dst;
        this._moveNotaion = moveNotaion;
    }

    public Move(Point _src, Point _dst, OpCodes moveCodes) {
        this._src = _src;
        this._dst = _dst;
        this.moveCode = moveCodes;
        promotingPiece = "";
    }

    public Move(Move move) {
        this._src = new Point(move._src);
        this._dst = new Point(move._dst);
        moveCode = move.moveCode;
        promotingPiece = move.promotingPiece;
        _moveNotaion = move._moveNotaion;
    }


    public Point get_dst() {
        return _dst;
    }

    public void reverse(){
        this._src.reverse();
        this._dst.reverse();
    }

    public Point get_src() {
        return _src;
    }

    public void set_dst(Point _dst) {
        this._dst = _dst;
    }

    public void set_src(Point _src) {
        this._src = _src;
    }


    public String getMoveNotaion() {
        return _moveNotaion;
    }

    public void setMoveNotaion(String moveNotaion) {
        this._moveNotaion = moveNotaion;
    }

    public OpCodes getMoveCode() {
        return moveCode;
    }

    public void setMoveCode(OpCodes moveCode) {
        this.moveCode = moveCode;
    }

    public String getPromotingPiece() {
        return promotingPiece;
    }

    public void setPromotingPiece(String promotingPiece) {
        this.promotingPiece = promotingPiece;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return _src.equals(move._src) &&
                _dst.equals(move._dst) &&
                _moveNotaion.equals(move._moveNotaion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_src, _dst);
    }

    @Override
    public String toString() {
        return "Move{" +
                "_src=" + _src +
                ", _dst=" + _dst +
                ", _moveNotaion='" + _moveNotaion + '\'' +
                ", promotingPiece=" + promotingPiece +
                '}';
    }
}
