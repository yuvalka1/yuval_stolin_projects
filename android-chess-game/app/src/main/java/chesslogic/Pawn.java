package chesslogic;

import android.graphics.Path;

import dalvik.bytecode.Opcodes;

public class Pawn extends Piece {

    private  boolean jumped = false;
    private boolean can_jump_two_steps = true;

    public boolean isCan_jump_two_steps() {
        return can_jump_two_steps;
    }

    public void setCan_jump_two_steps(boolean can_jump_two_steps) {
        this.can_jump_two_steps = can_jump_two_steps;
    }

    public boolean isJumped() {
        return jumped;
    }

    public void setJumped(boolean jumped) {
        this.jumped = jumped;
    }

    public Pawn(Point point, char type, boolean color) {
        super(point, type, color);
    }
    public Pawn(Pawn other){super(other._point, other._type, other._color); jumped = other.jumped; can_jump_two_steps = other.can_jump_two_steps;}


    @Override
    OpCodes validateMove(Board boardArr, int new_x, int new_y) {
        Piece p = boardArr.getPiece(new Point(new_x, new_y));
        if (new_x == _point.get_x() && new_y == _point.get_y()){return OpCodes.SAME_INDEX;} //same index
        else if(new_x == _point.get_x()){return OpCodes.INVALID_MOVE;}//cant go sideways
        else if(new_y == _point.get_y() && p != null){return OpCodes.INVALID_MOVE;}
        else if(boardArr.getPiece(new Point(new_x, new_y )) != null && _color == boardArr.getPiece(new Point(new_x, new_y ))._color){
            return OpCodes.SELF_PLAYER;
        }
        //cant go forward if not empty

        else if (Game.boardDirection && ((new_x < _point.get_x() &&  _color) || (new_x > _point.get_x() && !_color )) ||
                (!Game.boardDirection && ((new_x > _point.get_x() &&  _color) || (new_x < _point.get_x() && !_color )))){//can't go backwards
            return OpCodes.INVALID_MOVE;
        }
        else if(Math.abs(new_x - _point.get_x()) > 2 || Math.abs(new_y - _point.get_y()) > 1){
            return OpCodes.INVALID_MOVE;
        }
        else if(Math.abs(new_x - _point.get_x()) == 1 && Math.abs(new_y - _point.get_y()) == 1){ //eat in hypotenuse

            if (p == null) {//enpassent
                return  ChessHelper.enpassent(boardArr, new_x, new_y, _point, _color);
            }
            else if (p._color == _color)
            {
                return OpCodes.SAME_PLAYER;
            }

        }
        else if(Math.abs(new_x - _point.get_x()) == 2)
        {
            if (new_y != _point.get_y() || !can_jump_two_steps  || p != null) {
                return OpCodes.INVALID_MOVE;
            }
            int x;
            if (Game.boardDirection){
                x = _color ? _point.get_x() + 1: _point.get_x() - 1;
            }
            else{
                x = _color ? _point.get_x() - 1: _point.get_x() + 1;
            }

            if(boardArr.getPiece(new Point(x, new_y)) != null) {
                return OpCodes.INVALID_MOVE;
            }
            jumped = true;
            //move was suceesful

        }
        if(new_x == 0 || new_x == 7){
            can_jump_two_steps = false;
            return OpCodes.PROMOTING;
        }
        can_jump_two_steps = false;

        return OpCodes.VALID;
    }

    @Override
    public Piece clone() {
        return new Pawn(this);
    }
}

