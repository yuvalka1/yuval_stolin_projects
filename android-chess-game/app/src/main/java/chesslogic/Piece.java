package chesslogic;

public abstract class Piece {

    protected Point _point;
    protected char _type;
    protected boolean _color; //true - white, false - black
    //public Piece(){}
    public Piece(Point p, char type, boolean color){
        this._point = new Point(p);
        this._color = color;
        this._type = type;
    }

    abstract OpCodes validateMove(Board boardArr, int new_x, int new_y);

    public abstract Piece clone();

    void setNewPoint(Point newPoint){this._point.set_x(newPoint.get_x());
    this._point.set_y(newPoint.get_y());}

    public char get_type() {
        return _type;
    }
}
