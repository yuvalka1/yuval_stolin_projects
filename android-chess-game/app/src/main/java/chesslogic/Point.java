package chesslogic;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.Objects;

public class Point {
    private int _x,_y;
    public Point(){}
    public Point(int x, int y){
        _x = x;
        _y = y;
    }
    public Point(Point other){
        this._x = other._x;
        this._y = other._y;
    }
    private static final  HashMap<Integer, Integer> reverseIndexes = new HashMap<Integer, Integer>(){{
        put(0,7);
        put(1,6);
        put(2,5);
        put(3,4);
        put(4,3);
        put(5,2);
        put(6,1);
        put(7,0);
    }};

    public static int reverseIndex(int value){
        return reverseIndexes.get(value);
    }

    public void reverse(){
        this._x = reverseIndexes.get(this._x);
        this._y = reverseIndexes.get(this._y);
    }
    public int get_x() {
        return _x;
    }

    public int get_y() {
        return _y;
    }

    public void set_x(int _x) {
        this._x = _x;
    }

    public void set_y(int _y) {
        this._y = _y;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return _x == point._x &&
                _y == point._y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(_x, _y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "_x=" + _x +
                ", _y=" + _y +
                '}';
    }
}
