package chesslogic;

public class Queen extends Piece  {

    public Queen(Point point, char type, boolean color) {
        super(point, type, color);
    }
    public Queen(Queen other){super(other._point, other._type, other._color);}
    @Override
    OpCodes validateMove(Board boardArr, int new_x, int new_y) {

        if (this._point.get_y() == new_y && this._point.get_x() == new_x){
            return OpCodes.SAME_INDEX;
        }
        else if(boardArr.getPiece(new Point(new_x, new_y )) != null && _color == boardArr.getPiece(new Point(new_x, new_y ))._color){
            return OpCodes.SELF_PLAYER;
        }

        else if (this._point.get_x() == new_x || this._point.get_y() == new_y) // moves like rook
        {
            if(new_x == _point.get_x()){//check row
                if (ChessHelper.clearPathRow(boardArr, new_x, new_y, this._point) == OpCodes.INVALID_MOVE){
                    return OpCodes.INVALID_MOVE;
                }
            }
            else if(ChessHelper.clearPathCol(boardArr, new_x, new_y, this._point) == OpCodes.INVALID_MOVE){// check col
                return OpCodes.INVALID_MOVE;
            }
            return OpCodes.VALID;
        }
        else{ //moves like a bishop
            if(Math.abs(new_x - _point.get_x()) != Math.abs(new_y - _point.get_y())){
                return OpCodes.INVALID_MOVE;
            }

            if ((new_x > _point.get_x() && new_y < _point.get_y()) ||
                    (new_x < _point.get_x() && new_y > _point.get_y())){
                if (ChessHelper.positiveIncline(boardArr, new_x, new_y, _point) == OpCodes.INVALID_MOVE)
                    return OpCodes.INVALID_MOVE;
            }
            else{
                if (ChessHelper.negativeIncline(boardArr, new_x, new_y, _point) == OpCodes.INVALID_MOVE)
                    return OpCodes.INVALID_MOVE;
            }

            return OpCodes.VALID;
        }
    }

    @Override
    public Piece clone() {
        return new Queen(this);
    }
}
