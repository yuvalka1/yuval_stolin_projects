package chesslogic;

import android.graphics.Path;

import com.example.chess.R;

public class Rook extends Piece {
    
    private boolean castled = false;

    public boolean isCastled() {
        return castled;
    }

    public void setCastled(boolean castled) {
        this.castled = castled;
    }

    public Rook(Point point, char type, boolean color) {
        super(point, type, color);
    }
    public Rook(Rook other){super(other._point, other._type, other._color);this.castled = other.castled;}


    @Override
    OpCodes validateMove(Board boardArr, int new_x, int new_y) {
        if (this._point.get_y() == new_y && this._point.get_x() == new_x){
            return OpCodes.SAME_INDEX;
        }
        else if(boardArr.getPiece(new Point(new_x, new_y )) != null && _color == boardArr.getPiece(new Point(new_x, new_y ))._color){
            return OpCodes.SELF_PLAYER;
        }
        else if (new_x != this._point.get_x() && new_y != this._point.get_y()) //rook has to move in a strate line
        {
            return OpCodes.INVALID_MOVE;
        }
        else if(new_x == _point.get_x()){//check row
            if (ChessHelper.clearPathRow(boardArr, new_x, new_y, this._point) == OpCodes.INVALID_MOVE){
                return OpCodes.INVALID_MOVE;
            }
        }
        else if(ChessHelper.clearPathCol(boardArr, new_x, new_y, this._point) == OpCodes.INVALID_MOVE){// check col
            return OpCodes.INVALID_MOVE;
        }
        this.castled = true;
        return OpCodes.VALID;
    }

    @Override
    public Piece clone() {
        return new Rook(this);
    }


}
