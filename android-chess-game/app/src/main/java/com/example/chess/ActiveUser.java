package com.example.chess;

import com.google.firebase.database.Exclude;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import gameEngine.classes.GameInfo;

public class ActiveUser {
    String _email;
    Map<String, Integer> _scores;
    ArrayList<GameInfo> _gameArchive;
    ArrayList<String> _friends_list;
    String _public_key;

    public ActiveUser() {

    }

    public ActiveUser(String email, String public_key) {
        int min =100;// Resources.getSystem().getInteger(R.integer.minimun_score);
        _scores = new HashMap<String, Integer>(){};
        _scores.put("Bullet", min);
        _scores.put("Blitz", min);
        _scores.put("Rapid", min);
        _public_key = public_key;
        _email = email;
        _gameArchive = new ArrayList<>();
        _friends_list = new ArrayList<>();

    }

    public ArrayList<String> get_friends_list() {
        return _friends_list;
    }

    public void set_friends_list(ArrayList<String> _friends_list) {
        this._friends_list = _friends_list;
    }

    public String get_public_key() {
        return _public_key;
    }

    public void set_public_key(String _public_key) {
        this._public_key = _public_key;
    }


    public ArrayList<GameInfo> get_gameArchive() {
        return _gameArchive;
    }

    public void set_gameArchive(ArrayList<GameInfo> _gameArchive) {
        this._gameArchive = _gameArchive;
    }



    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_email() {
        return _email;
    }


    public Map<String, Integer> get_scores() {
        return _scores;
    }

    public void set_scores(Map<String, Integer> _scores) {
        this._scores = _scores;
    }

    public void addPoints(String key, int points){
        int level = _scores.get(key);
        _scores.put(key,(level + points));

    }

    public void addGame(String key, GameInfo gameInfo){
        if(_gameArchive == null){_gameArchive = new ArrayList<>();}
        _gameArchive.add(gameInfo);
    }

    @Exclude
    public Map<String,Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("_public_key", _public_key);
        result.put("_email", _email);
        result.put("_scores", _scores);
        result.put("_gameArchive", _gameArchive);
        result.put("_friends_list", _friends_list);
        return result;
    }

    @NotNull
    @Override
    public String toString() {
        return "ActiveUser{" +
                "_email='" + _email + '\'' +
                ", _scores=" + _scores +
                ", _gameArchive=" + _gameArchive +
                ", _public_key='" + _public_key + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActiveUser that = (ActiveUser) o;
        return _email.equals(that._email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_email, _public_key, _scores, _gameArchive);
    }
}
