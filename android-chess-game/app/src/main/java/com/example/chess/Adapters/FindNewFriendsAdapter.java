package com.example.chess.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chess.Globals;
import com.example.chess.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import gameEngine.classes.UserInfo;

public class FindNewFriendsAdapter extends RecyclerView.Adapter<FindNewFriendsAdapter.FriendsViewHolder> {

    ArrayList<UserInfo> friendsList;
    ArrayList<String> friends_ids;
    Context context;

    public FindNewFriendsAdapter(ArrayList<UserInfo> friendsList, ArrayList<String> friends_ids, Context context) {
        this.friendsList = friendsList;
        this.friends_ids = friends_ids;
        this.context = context;
    }


    public FindNewFriendsAdapter(Context context) {
        this.context = context;
    }

    public void setFriends_ids(ArrayList<String> friends_ids) {
        this.friends_ids = friends_ids;
    }

    public void setFriendsList(ArrayList<UserInfo> friendsList) {
        this.friendsList = friendsList;
    }

    public ArrayList<String> getFriends_ids() {
        return friends_ids;
    }

    public ArrayList<UserInfo> getFriendsList() {
        return friendsList;
    }

    @NonNull
    @NotNull
    @Override
    public FriendsViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.friends_info, null);
        return new FriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull FriendsViewHolder holder, int position) {
        UserInfo userInfo = friendsList.get(position);
        holder.tvName.setText(userInfo.get_display_name());
        holder.tvCountry.setText(userInfo.get_country());
        holder.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(position);
            }
        });
    }

    private void alertDialog(int position){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Are you sure you want to become friends with " + friendsList.get(position).get_display_name());
        builder1.setMessage("confirm choice");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                        firebaseFirestore.collection(context.getString(R.string.userDB)).
                                document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid())).
                                update("_friends_list", FieldValue.arrayUnion(friends_ids.get(position)));
                        friends_ids.remove(position);
                        friendsList.remove(position);
                        Globals.user.set_friends_list(friends_ids);
                        notifyItemRemoved(position);
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }



    @Override
    public int getItemCount() {
        return friendsList.size();
    }


    class FriendsViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvCountry;
        FloatingActionButton floatingActionButton;
        public FriendsViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvCountry = itemView.findViewById(R.id.friend_country);
            tvName = itemView.findViewById(R.id.friend_name);
            floatingActionButton = itemView.findViewById(R.id.addFriend);
        }
    }
}
