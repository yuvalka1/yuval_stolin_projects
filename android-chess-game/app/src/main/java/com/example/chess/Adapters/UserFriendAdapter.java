package com.example.chess.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chess.Globals;
import com.example.chess.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import gameEngine.SendNotificationPack.SendNotification;
import gameEngine.classes.UserInfo;

public class UserFriendAdapter extends RecyclerView.Adapter<UserFriendAdapter.UserFriendsViewHolder> {
    private static final int height  = 100;
    private static final int width = 120;
    Context context;
    public  ArrayList<ArrayList<Object>> friends;
    public UserFriendAdapter(Context context,  ArrayList<ArrayList<Object>> arrayLists) {
        this.context = context;
        friends = arrayLists;
    }

    public UserFriendAdapter(Context context) {
        this.context = context;
    }

    public void setFriends(ArrayList<ArrayList<Object>> friends) {
        this.friends = friends;
    }



    @NonNull
    @NotNull
    @Override
    public UserFriendsViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.friends_costum_layout, null);
        return new UserFriendAdapter.UserFriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UserFriendsViewHolder holder, int position) {
        UserInfo userInfo = (UserInfo) friends.get(position).get(0);
        if(friends.get(position).get(2) == null){
            holder.imageView.setImageResource(R.drawable.ic_baseline_person_24);
        }
        else {
            byte[] image = (byte[]) friends.get(position).get(2);
            Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
            holder.imageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, width, height, false));
        }
        holder.tvCountry.setText(userInfo.get_country());
        holder.tvName.setText(userInfo.get_display_name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] tasks = {"play", "remove friend"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Pick a task");
                builder.setItems(tasks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            Intent intent = new Intent(context, SendNotification.class);
                            intent.putExtra("user_friend_id", (String) friends.get(position).get(1));
                            context.startActivity(intent);
                        }
                        else{

                            String id = (String) friends.get(position).get(1);
                            Globals.user.get_friends_list().remove(id);
                            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                            firebaseFirestore.collection(context.getString(R.string.userDB)).document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()))
                                    .update("_friends_list", FieldValue.arrayRemove(id));
                            friends.remove(position);
                            notifyDataSetChanged();
                        }

                    }
                });
                builder.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    class UserFriendsViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView tvName, tvCountry;
        public UserFriendsViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvCountry = itemView.findViewById(R.id.friend_country);
            tvName = itemView.findViewById(R.id.friend_name);
            imageView = itemView.findViewById(R.id.friendImage);
        }
    }
}
