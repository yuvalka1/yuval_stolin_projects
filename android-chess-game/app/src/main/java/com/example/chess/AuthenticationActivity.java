package com.example.chess;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;
import android.os.Handler;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.royrodriguez.transitionbutton.TransitionButton;


import java.util.Objects;

import chesslogic.Game;
import gameEngine.GameFinder;
import gameEngine.SendNotificationPack.MyFirebaseMessagingService;
import gameEngine.SendNotificationPack.SendNotification;
import gameEngine.classes.UserInfo;

public class AuthenticationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG ="AuthenticationActivity";
    EditText etUsername, etPassword;

    Button  forgot_login;
    private TransitionButton transitionButton;
    private FirebaseAuth mAuth;
    FirebaseFirestore db;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_authentication);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        etPassword = findViewById(R.id.et_password);
        etUsername = findViewById(R.id.et_email);
        transitionButton = (TransitionButton) findViewById(R.id.btn_login);
        forgot_login =(Button) findViewById(R.id.btn_forgotLogin);
        transitionButton.setOnClickListener(this);
        forgot_login.setOnClickListener(AuthenticationActivity.this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Chess");

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){
            toolbar.setElevation(10f);
        }
        toolbar.inflateMenu(R.menu.login_toll_bar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.signUp) {
                    Intent intent = new Intent(AuthenticationActivity.this, SignupActivity.class);
                    startActivity(intent);
                    //signup activity
                }
                return false;
            }
        });

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private boolean validatePassword(){
        String password = etPassword.getText().toString();
        String username = etUsername.getText().toString();
        Log.d(TAG, password  + " "  + username);
        if (password.isEmpty() || username.isEmpty()) {return false;}
        return password.length() >= 6;
    }
    @Override
    public void onClick(View v) {
        if (v == this.transitionButton){
            if (validatePassword()) {
                signIn(etUsername.getText().toString(), etPassword.getText().toString());
            }
        }
        else if (v == forgot_login){
            String email = etUsername.getText().toString();
            if(email.isEmpty()){
                Toast.makeText(AuthenticationActivity.this, "Please enter email", Toast.LENGTH_LONG).show();
            }
            else {
                FirebaseAuth.getInstance().sendPasswordResetEmail(etUsername.getText().toString().replaceAll("\\s+",""))
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "Email sent.");
                                }
                                else{
                                    Log.e(TAG, Objects.requireNonNull(task.getException()).getLocalizedMessage());
                                }
                            }
                        });
            }
        }
    }

    private void loadData(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        //FirebaseAuth.getInstance().signOut();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Log.d(TAG, "check if user already sign in " + currentUser);
        if(currentUser != null) {
            Log.d(TAG, currentUser.toString());
            DocumentReference userReference = db.collection(getString(R.string.userDB)).document(currentUser.getUid());
            userReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable @org.jetbrains.annotations.Nullable DocumentSnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                    if (error != null) {
                        Log.d(TAG, error.getLocalizedMessage());
                    } else if (value != null && value.exists()) {
                        ActiveUser user = value.toObject(ActiveUser.class);
                        Globals.setUser(user);
                        assert user != null;
                        String public_data = user.get_public_key();
                        DocumentReference publicUserDataReference = db.collection(getString(R.string.userPublicData)).document(public_data);
                        publicUserDataReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                UserInfo userInfo = documentSnapshot.toObject(UserInfo.class);
                                Globals.setUserInfo(userInfo);
                                assert userInfo != null;
                                String image_url = userInfo.get_image_url();
                                String jsonByteImage = getSharedPreferences(getString(R.string.userImage), Activity.MODE_PRIVATE).getString(getString(R.string.image), null);
                                if (jsonByteImage != null) {
                                    Gson gson = new Gson();
                                    Globals.imageByteArray = gson.fromJson(jsonByteImage, byte[].class);
                                    saveToken();
                                } else if (image_url != null) {
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(userInfo.get_image_url());
                                    storageReference.getBytes(UserFriends.ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                        @Override
                                        public void onSuccess(byte[] bytes) {
                                            Globals.imageByteArray = bytes;
                                            saveToken();
                                        }
                                    });
                                }
                                saveToken();

                            }
                        });

                    } else {
                        Log.d(TAG, "Current data: null");
                    }
                }
            });
        }
    }

    private void saveToken(){
        Toast.makeText(AuthenticationActivity.this, "Authentication succeeded.",
                Toast.LENGTH_LONG).show();
        MyFirebaseMessagingService messagingService = new MyFirebaseMessagingService();
        String token = messagingService.getToken(AuthenticationActivity.this);
        if(token != null){
            messagingService.updateToken(token);
        }
        FirebaseUser user = mAuth.getCurrentUser();
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.userPassword), Activity.MODE_PRIVATE).edit();
        editor.putString(getString(R.string.password), etPassword.getText().toString().replaceAll("\\s+",""));
        editor.apply();
        updateUI(user);
    }


    public void signIn(String email, String password) {
        // [START sign_in_with_email]
        email = email.replaceAll("\\s+","");
        password = password.replaceAll("\\s+","");
        String finalPassword = password;
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            loadData();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(AuthenticationActivity.this,
                                    Objects.requireNonNull(task.getException()).
                                            getLocalizedMessage() , Toast.LENGTH_LONG).show();
                            updateUI(null);
                        }
                    }
                });
        // [END sign_in_with_email]
    }

    private void reload(boolean isSuccessful) {
        transitionButton.startAnimation();

        // Do your networking task or background work here.
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isSuccessful) {
                    transitionButton.stopAnimation(TransitionButton.StopAnimationStyle.EXPAND, new TransitionButton.OnAnimationStopEndListener() {
                        @Override
                        public void onAnimationStopEnd() {
                            Intent intent = new Intent(getBaseContext(), MainMenu.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            finish();
                        }
                    });
                } else {
                    transitionButton.stopAnimation(TransitionButton.StopAnimationStyle.SHAKE, null);
                }
            }
        }, 2000);
    }




    private void updateUI(FirebaseUser user) {
        if (user != null) {
            reload(true);
        }
        else{
            reload(false);
        }
    }

}