package com.example.chess;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.NonNull;

public class ChangeGameTypeDialog extends Dialog implements View.OnClickListener {
    Context mContext;
    Button rapid, blitz, bullet;
    public interface OnUserChoice{
        public void run(String choice);
    }

    OnUserChoice onUserChoice;

    public ChangeGameTypeDialog(@NonNull Context context, OnUserChoice userChoice) {
        super(context);
        mContext = context;
        onUserChoice = userChoice;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.change_game_type);
        rapid = findViewById(R.id.rapid);
        rapid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onUserChoice.run("Rapid");
            }
        });
        blitz = findViewById(R.id.blitz);
        blitz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onUserChoice.run("Blitz");
            }
        });

        bullet = findViewById(R.id.bullet);
        bullet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onUserChoice.run("Bullet");
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}
