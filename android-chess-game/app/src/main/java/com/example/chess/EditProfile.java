package com.example.chess;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.royrodriguez.transitionbutton.TransitionButton;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Random;


public class EditProfile extends AppCompatActivity {

    private static final String TAG = "EditProfile";
    ImageView imageview_account_profile;
    EditText editUserName, editEmail, editPassword, confirmPassword;
    Toolbar toolbar;
    TransitionButton confirmEdit;
    FloatingActionButton editImage;
    FirebaseFirestore db;
    FirebaseStorage storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_edit_profile);
        storage = FirebaseStorage.getInstance();
        toolbar = findViewById(R.id.tb);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runAnimation(true);
            }
        });
        db = FirebaseFirestore.getInstance();
        imageview_account_profile = findViewById(R.id.imageview_account_profile);
        if(Globals.imageByteArray != null){
            Bitmap bmp = BitmapFactory.decodeByteArray(Globals.imageByteArray, 0, Globals.imageByteArray.length);
            imageview_account_profile.setImageBitmap(Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false));
        }

        editPassword = findViewById(R.id.editPassword);
        editEmail = findViewById(R.id.editEmail);
        confirmEdit = findViewById(R.id.confirmEdit);
        editUserName = findViewById(R.id.editUserName);
        confirmPassword = findViewById(R.id.confirmPassword);
        editImage = findViewById(R.id.editImage);
        editImage.setDrawingCacheEnabled(true);
        editImage.buildDrawingCache();


        confirmEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = editUserName.getText().toString();
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();
                String password1 = confirmPassword.getText().toString();
                if(username.isEmpty() && email.isEmpty() && password.isEmpty()){
                    Toast.makeText(EditProfile.this, "enter personal data to update", Toast.LENGTH_LONG).show();
                    return;
                }
                if(!password.equals(password1)){
                    runAnimation(false);
                }
                if(!email.isEmpty()){
                    isCheckEmail(email, new OnDataFound() {
                        @Override
                        public void onSuccess(boolean isRegistered) {
                            if(isRegistered){
                                runAnimation(false);
                            }
                            else{
                                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                                Objects.requireNonNull(mAuth.getCurrentUser()).
                                        updateEmail(email).
                                        addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Toast.makeText(EditProfile.this, "updated email", Toast.LENGTH_LONG).show();
                                        Log.d(TAG, "updated email");
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull @NotNull Exception e) {
                                        Toast.makeText(EditProfile.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });

                            }
                        }
                    });
                }
                if(!password.isEmpty()){
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    FirebaseUser user = mAuth.getCurrentUser();

                    assert user != null;
                    AuthCredential credential = EmailAuthProvider
                            .getCredential(Objects.requireNonNull(user.getEmail()),
                                    getSharedPreferences(getString(R.string.userPassword), Activity.MODE_PRIVATE).
                                            getString(getString(R.string.password), "123456"));

                    user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<Void> task) {
                            if(task.isSuccessful()){
                                user.updatePassword(password).
                                        addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void unused) {
                                                Toast.makeText(EditProfile.this, "updated password", Toast.LENGTH_LONG).show();
                                                Log.d(TAG, "updated password");

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull @NotNull Exception e) {
                                        Toast.makeText(EditProfile.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                        Log.w(TAG, e.getLocalizedMessage());
                                    }
                                });
                            }
                            else{
                                Log.d(TAG, "Error auth failed");
                            }
                        }
                    });

                }
                if(!username.isEmpty()){
                    DocumentReference users = db.collection(getString(R.string.userPublicData)).
                            document(Globals.getUser()._public_key);
                    users.update("_display_name", username).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Log.d(TAG, "updated user name");
                            Toast.makeText(EditProfile.this, "updated user name", Toast.LENGTH_LONG).show();
                        }
                    });

                }



            }
        });

        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.with(EditProfile.this)
                        .crop()	    			//Crop image(Optional), Check Customization for more option
                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(Activity.RESULT_OK == resultCode){
            assert data != null;
            Uri uri = data.getData();
            InputStream iStream = null;
            try {
                iStream = getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                assert iStream != null;
                byte[] inputData = getBytes(iStream);
                if(Globals.imageByteArray == null || Globals.imageByteArray != inputData) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(inputData, 0, inputData.length);
                    Globals.imageByteArray = inputData;
                    imageview_account_profile.setImageBitmap(Bitmap.createScaledBitmap(bmp, imageview_account_profile.getWidth(), imageview_account_profile.getHeight(), false));
                    uploadImage();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }
    private byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private void uploadImage() {
        Bitmap bitmap = ((BitmapDrawable) imageview_account_profile.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.userImage), Activity.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String array = gson.toJson(data);
        editor.putString(getString(R.string.image), array);
        editor.apply();

        StorageReference storageReference = storage.getReference();
        String image_url = Globals.getUserInfo().get_image_url();
        String reference = image_url == null ? random() : image_url;
        StorageReference imageRef = storageReference.child(reference);
        String path = imageRef.getPath();
        UploadTask uploadTask = imageRef.putBytes(data);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d(TAG, exception.getLocalizedMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "image was uploaded");
            }
        });

        FirebaseFirestore.getInstance().collection(getString(R.string.userPublicData)).document(Globals.getUser().get_public_key()).
                update("_image_url", path).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Log.d(TAG, "image was saved");
            }
        });

    }
    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(20);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    private void isCheckEmail(final String email, final OnDataFound listener) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.fetchSignInMethodsForEmail(email).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<SignInMethodQueryResult> task) {
                boolean result = !Objects.requireNonNull(Objects.requireNonNull(task.getResult()).getSignInMethods()).isEmpty();
                listener.onSuccess(result);
            }
        });
    }

    private void runAnimation(boolean isSuccessful){
        confirmEdit.startAnimation();

        // Do your networking task or background work here.
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isSuccessful) {
                    confirmEdit.stopAnimation(TransitionButton.StopAnimationStyle.EXPAND, new TransitionButton.OnAnimationStopEndListener() {
                        @Override
                        public void onAnimationStopEnd() {
                            Intent intent = getIntent();
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    });
                } else {
                    confirmEdit.stopAnimation(TransitionButton.StopAnimationStyle.SHAKE, null);
                }
            }
        }, 2000);
    }
}