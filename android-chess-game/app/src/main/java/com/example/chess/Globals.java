package com.example.chess;

import gameEngine.classes.UserInfo;

public class Globals {

    public static ActiveUser user;
    public static void setUser(ActiveUser user) {
        Globals.user = user;
    }
    public static ActiveUser getUser() {
        return user;
    }

    public static UserInfo userInfo;

    public static UserInfo getUserInfo() {
        return userInfo;
    }

    public static void setUserInfo(UserInfo userInfo) {
        Globals.userInfo = userInfo;
    }


    public static byte[] imageByteArray;

    public static  byte[] getImageByteArray() {
        return imageByteArray;
    }

    public static void setImageByteArray(byte[] image_uri) {
        Globals.imageByteArray = image_uri;
    }


}
