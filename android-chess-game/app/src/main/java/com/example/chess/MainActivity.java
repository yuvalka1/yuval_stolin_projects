package com.example.chess;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import gameEngine.GameFinder;
import gameEngine.SendNotificationPack.SendNotification;
import gameEngine.classes.UserInfo;

public class MainActivity extends AppCompatActivity {

    private static final String TAG="CHESS_PROJECT";
    public static int SPLASH_TIME_OUT = 6000;
    View one, two, three, four, five, six;
    TextView tagLine;
    CircularImageView imageView;
    Animation top, bottom, middle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_main);

        top = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        bottom = AnimationUtils.loadAnimation(this,R.anim.bottom_animation);
        middle = AnimationUtils.loadAnimation(this,R.anim.middle_animation);

        one = findViewById(R.id.first_line);
        two = findViewById(R.id.second_line);
        three = findViewById(R.id.third_line);
        four = findViewById(R.id.fourth_line);
        five = findViewById(R.id.fifth_line);
        six = findViewById(R.id.sixth_line);
        tagLine = findViewById(R.id.tagLine);
        imageView = findViewById(R.id.ivIcon);

        one.setAnimation(top);
        two.setAnimation(top);
        three.setAnimation(top);
        four.setAnimation(top);
        five.setAnimation(top);
        six.setAnimation(top);
        imageView.setAnimation(middle);
        tagLine.setAnimation(bottom);



        //FirebaseAuth.getInstance().signOut();
        loadData();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this,FirebaseAuth.getInstance().getCurrentUser() != null ?MainMenu.class : AuthenticationActivity.class);
                startActivity(intent,
                        ActivityOptions.makeSceneTransitionAnimation(MainActivity.this).toBundle());
                finish();
            }
        }, SPLASH_TIME_OUT);

        //startActivity(new Intent(this, SendNotification.class));



    }


    private void loadData(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        //FirebaseAuth.getInstance().signOut();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Log.d(TAG, "check if user already sign in " + currentUser);
        if(currentUser != null){
            Log.d(TAG, currentUser.toString());
            DocumentReference userReference = db.collection(getString(R.string.userDB)).document(currentUser.getUid());
            userReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    ActiveUser user = documentSnapshot.toObject(ActiveUser.class);
                    Globals.setUser(user);
                    assert user != null;
                    String public_data = user.get_public_key();
                    DocumentReference publicUserDataReference = db.collection(getString(R.string.userPublicData)).document(public_data);
                    publicUserDataReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            UserInfo userInfo = documentSnapshot.toObject(UserInfo.class);
                            Globals.setUserInfo(userInfo);
                            assert userInfo != null;
                            String image_url = userInfo.get_image_url();
                            String jsonByteImage = getSharedPreferences(getString(R.string.userImage), Activity.MODE_PRIVATE).getString(getString(R.string.image), null);
                            if(jsonByteImage != null){
                                Gson gson = new Gson();
                                Globals.imageByteArray = gson.fromJson(jsonByteImage, byte[].class);
                            }
                            else if(image_url != null){
                                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(userInfo.get_image_url());
                                storageReference.getBytes(UserFriends.ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                    @Override
                                    public void onSuccess(byte[] bytes) {
                                        Globals.imageByteArray = bytes;

                                    }
                                });
                            }
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull @NotNull Exception e) {

                    Log.d(TAG, "Current data: null " + e.getLocalizedMessage());

                }
            });

        }


    }
    @Override
    protected void onStart() {
        super.onStart();
    }
}