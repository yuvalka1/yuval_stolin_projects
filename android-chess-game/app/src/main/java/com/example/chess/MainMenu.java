package com.example.chess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Objects;

import gameEngine.GameFinder;
import gameEngine.PuzzlesActivity;

public class MainMenu extends AppCompatActivity {

    LinearLayout goToGameFinder, goToUserAccount, goToSettings, goToPuzzles;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_main_menu);
        goToGameFinder = findViewById(R.id.goToGameFinder);
        goToUserAccount = findViewById(R.id.goToUserAcount);
        goToPuzzles = findViewById(R.id.goToPuzzers);
        goToSettings = findViewById(R.id.goToSettings);

        goToUserAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, UserAccount.class));
            }
        });

        goToGameFinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, GameFinder.class));
            }
        });

        goToSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, Settings.class));
            }
        });

        goToPuzzles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, PuzzlesActivity.class));
            }
        });
    }
}