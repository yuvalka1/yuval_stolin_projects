package com.example.chess;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.example.chess.Adapters.FindNewFriendsAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import gameEngine.classes.UserInfo;

public class NewFriendsActivity extends AppCompatActivity {
    private static final String TAG = "FriendsActivity";
    ArrayList<UserInfo> users;
    ArrayList<String> ids;
    FirebaseFirestore firestore;
    RecyclerView listFriends;
    EditText searchFriends;
    SwipeRefreshLayout pullToRefresh;
    FindNewFriendsAdapter friendsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_friends);
        firestore = FirebaseFirestore.getInstance();
        listFriends = findViewById(R.id.listFriends);
        searchFriends = findViewById(R.id.searchFriends);
        pullToRefresh = findViewById(R.id.pullToRefresh);

        users = new ArrayList<>();
        ids = new ArrayList<>();
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadAllData();
                pullToRefresh.setRefreshing(false);
            }
        });
        reloadAllData();

    }



    private void reloadAllData(){
        //users = friendsAdapter.getFriendsList();
        //ids = friendsAdapter.getFriends_ids();
        users.clear();
        ids.clear();
        firestore.collection(getString(R.string.userPublicData)).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for(DocumentSnapshot documentSnapshot: queryDocumentSnapshots.getDocuments()){
                    String id = documentSnapshot.getId();
                    if(!Globals.user._public_key.equals(id) && !Globals.user.get_friends_list().contains(id)){
                        users.add(documentSnapshot.toObject(UserInfo.class));
                        ids.add(documentSnapshot.getId());
                    }
                }
                if(!users.isEmpty()){
                    if(friendsAdapter == null){
                        friendsAdapter = new FindNewFriendsAdapter(users,ids, NewFriendsActivity.this);
                        listFriends.setAdapter(friendsAdapter);
                    }
                    else {
                        friendsAdapter.setFriends_ids(ids);
                        friendsAdapter.setFriendsList(users);
                        friendsAdapter.notifyDataSetChanged();
                    }

                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull @NotNull Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
        });
    }


}