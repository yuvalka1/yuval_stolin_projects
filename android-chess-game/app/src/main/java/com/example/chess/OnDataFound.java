package com.example.chess;

public interface OnDataFound {
    void onSuccess(boolean found);
}
