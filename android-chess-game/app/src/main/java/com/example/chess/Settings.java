package com.example.chess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class Settings extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_settings);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnEdit){
            startActivity(new Intent(Settings.this, EditProfile.class));
        }
        else if(v.getId() == R.id.logOut){
            FirebaseAuth.getInstance().signOut();
            finish();
            startActivity(new Intent(Settings.this, AuthenticationActivity.class));

        }
        else if(v.getId() == R.id.reportError){
            startActivity(new Intent(Settings.this, SendSms.class));
        }
    }
}