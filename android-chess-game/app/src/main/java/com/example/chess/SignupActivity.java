package com.example.chess;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import gameEngine.SendNotificationPack.MyFirebaseMessagingService;
import gameEngine.classes.UserInfo;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG ="SignupActivity";
    EditText etEmail, etPassword, etRepeatPassword, etUsername, et_country;
    Button signup;
    Toolbar toolbar;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_signup);
        mAuth = FirebaseAuth.getInstance();
        etPassword = findViewById(R.id.et_password);
        etEmail = findViewById(R.id.et_email);
        etUsername = findViewById(R.id.et_username);
        etRepeatPassword = findViewById(R.id.et_repeatpassword);
        signup = (Button)findViewById(R.id.btn_signup);
        et_country = findViewById(R.id.et_country);
        toolbar = findViewById(R.id.toolbar);


        toolbar.setTitle("Chess");
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateSignup()){

                    createAccount(etEmail.getText().toString(), etPassword.getText().toString());
                }
            }
        });


    }



    private void checkIfNameExist(OnDataFound onDataFound){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(getString(R.string.userDB)).whereEqualTo("_display_name", etUsername.getText().toString()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    boolean found = false;
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        found = true;
                        break;
                    }
                    onDataFound.onSuccess(found);
                }else {
                    Log.d(TAG, "Error getting documents: ", task.getException());
                }
            }
        });
    }

    private boolean validateSignup(){
        return etRepeatPassword.getText().toString().equals(etPassword.getText().toString())
                && ! etEmail.getText().toString().equals("") && ! etUsername.getText().toString().equals("") && !et_country.getText().toString().equals("");
    }
    public void sendEmailVerification() {
        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        assert user != null;
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // Email sent
                    }
                });
        // [END send_email_verification]
    }
    public void createAccount(String email, String password) {
        // [START create_user_with_email]
        email = email.replaceAll("\\s+","");
        password = password.replaceAll("\\s+","");
        String finalPassword = password;
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            sendEmailVerification();
                            FirebaseUser user = mAuth.getCurrentUser();
                            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.userPassword), Activity.MODE_PRIVATE).edit();
                            editor.putString(getString(R.string.password), finalPassword);
                            editor.apply();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignupActivity.this, "Authentication failed.",
                                    Toast.LENGTH_LONG).show();
                            updateUI(null);
                        }
                    }
                });
        // [END create_user_with_email]
    }

    private void reload() {Intent intent = new Intent(this, MainMenu.class);
        startActivity(intent); }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            CollectionReference public_user_info = db.collection(getString(R.string.userPublicData));
            UserInfo userInfo = new UserInfo(etUsername.getText().toString(), et_country.getText().toString(), null);
            DocumentReference new_public_user = public_user_info.document();
            new_public_user.set(userInfo).addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    Globals.setUserInfo(userInfo);
                    ActiveUser activeUser = new ActiveUser(etEmail.getText().toString(), new_public_user.getId());
                    CollectionReference users = db.collection(getString(R.string.userDB));
                    users.document(user.getUid()).set(activeUser).addOnSuccessListener(unused -> {
                        Globals.setUser(activeUser);
                        MyFirebaseMessagingService messagingService = new MyFirebaseMessagingService();
                        String token = messagingService.getToken(SignupActivity.this);
                        if(token != null){
                            messagingService.updateToken(token);
                        }
                        reload();
                    });
                }
            });

        }
    }
}