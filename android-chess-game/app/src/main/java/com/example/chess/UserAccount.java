package com.example.chess;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.MetadataChanges;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import gameEngine.GameFinder;
import gameEngine.classes.UserInfo;

public class UserAccount extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener{
    public static final String TAG = "UserAccount";
    BottomNavigationView bottomNavigationView;
    private int mMenuId;
    UserFriends  userFriends;
    UserStats userStats;
    UserProfile userProfile;
    private FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        db = FirebaseFirestore.getInstance();
        setContentView(R.layout.activity_user_account);
        bottomNavigationView = findViewById(R.id.bottomNavigationview);
        bottomNavigationView.setOnItemSelectedListener(this);
        bottomNavigationView.setItemIconTintList(null);
        userFriends = new UserFriends();
        userStats = new UserStats();
        userProfile = new UserProfile();
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_container, userProfile)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*
        DocumentReference userReference = db.collection(getString(R.string.userPublicData)).document(Globals.getUser().get_public_key());
        userReference.addSnapshotListener(this, MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable @org.jetbrains.annotations.Nullable DocumentSnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                if (error != null){
                    Log.d(TAG, error.getLocalizedMessage());}
                else if(value != null && value.exists()){
                    UserInfo user = value.toObject(UserInfo.class);
                    Globals.setUserInfo(user);
                }
                else{
                    Log.d(TAG, "Current data: null");
                    Intent intent = new Intent(UserAccount.this, AuthenticationActivity.class);
                    startActivity(intent);
                }
            }
        });

         */
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
        mMenuId = item.getItemId();
        for (int i = 0; i < bottomNavigationView.getMenu().size(); i++) {
            MenuItem menuItem = bottomNavigationView.getMenu().getItem(i);
            boolean isChecked = menuItem.getItemId() == item.getItemId();
            menuItem.setChecked(isChecked);
        }

        Fragment selectedFragment = null;

        switch (item.getItemId()) {
            case R.id.bbn_Freinds:
                selectedFragment = userFriends;
                break;
            case R.id.bbn_profile:
                selectedFragment = userProfile;
                break;
            case R.id.bbn_Stats:
                selectedFragment = userStats;
                break;
            case R.id.bbn_Theme:
                selectedFragment = new UserTheames();
                break;
            default:break;
        }
        if(selectedFragment != null) {
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.fragment_container, selectedFragment)
                    .commit();
        }
        return true;

    }
}