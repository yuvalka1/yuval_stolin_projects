package com.example.chess;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.chess.Adapters.UserFriendAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import gameEngine.classes.UserInfo;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserFriends#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFriends extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "UserFriends";
    public static final long ONE_MEGABYTE = 1024*1024;
    public static final int ERROR = -1;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private   ArrayList<ArrayList<Object>> friends;
    private UserFriendAdapter userFriendAdapter;

    public UserFriends() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserFriends.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFriends newInstance(String param1, String param2) {
        UserFriends fragment = new UserFriends();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_friends, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.listFriends);
        Button findNewFriends = view.findViewById(R.id.findNewFriends);
        RotateLoading rotateloading = view.findViewById(R.id.rotateloading);
        if(Globals.user.get_friends_list() != null && !Globals.user.get_friends_list().isEmpty()) {
            rotateloading.start();
        }
        findNewFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(container.getContext(), NewFriendsActivity.class));
            }
        });
        SwipeRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFriends(recyclerView);
                pullToRefresh.setRefreshing(false);
            }
        });
        userFriendAdapter = new UserFriendAdapter(container.getContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(new Runnable() {
            @Override
            public void run() {
                ((Activity) container.getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadFriends(recyclerView);
                        rotateloading.stop();
                    }
                });

            }
        });


        FirebaseFirestore db = FirebaseFirestore.getInstance();
        /*
        db.collection(getString(R.string.userDB)).
                document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid())).
                addSnapshotListener((Activity) container.getContext(), new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable @org.jetbrains.annotations.Nullable DocumentSnapshot value,
                                @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                if (error != null) {
                    Log.w(TAG, "Listen failed.", error);
                    return;
                }

                if (value != null && value.exists()) {
                    Log.d(TAG, "Current data: " + value.getData());
                    ActiveUser activeUser = value.toObject(ActiveUser.class);
                    assert activeUser != null;
                    if(Globals.user._friends_list.size() != activeUser._friends_list.size()){
                        Globals.user.set_friends_list(activeUser._friends_list);
                        loadFriends(recyclerView);
                        return;
                    }
                    Collections.sort(Globals.user._friends_list);
                    Collections.sort(activeUser._friends_list);
                    if(Globals.user._friends_list.hashCode() != activeUser.get_friends_list().hashCode()){
                        Globals.user.set_friends_list(activeUser._friends_list);
                        loadFriends(recyclerView);
                    }
                } else {
                    Log.d(TAG, "Current data: null");
                }
            }
        });

         */
        return view;
    }

    private int findFromList(String id){
        for(int i = 0; i < friends.size(); i++){
            if(friends.get(i).get(1).equals(id)){
                return i;
            }
        }
        return ERROR;
    }

    private int removeFromList(String id){
        for(int i = 0; i < friends.size(); i++){
            if(friends.get(i).get(1).equals(id)){
                friends.remove(i);
                return i;
            }
        }
        return ERROR;
    }


    private void loadFriends(RecyclerView recyclerView){
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        friends = new ArrayList<>();
        int i = 0;
        for( i = 0; i < Globals.user.get_friends_list().size(); i++){
            String friend = Globals.getUser()._friends_list.get(i);
            Log.d(TAG, friend);
            int finalI = i;
            firebaseFirestore.collection(getString(R.string.userPublicData)).document(friend).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    UserInfo userInfo = documentSnapshot.toObject(UserInfo.class);

                    assert userInfo != null;
                    if (userInfo.get_image_url() != null) {
                        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(userInfo.get_image_url());
                        storageReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                            @Override
                            public void onSuccess(byte[] bytes) {
                                ArrayList<Object> array = new ArrayList<>();
                                array.add(userInfo);
                                array.add(documentSnapshot.getId());
                                array.add(bytes);
                                friends.add(array);
                                if (finalI == Globals.user.get_friends_list().size() - 1) {
                                    userFriendAdapter.setFriends(friends);
                                    if (recyclerView.getAdapter() == null) {
                                        recyclerView.setAdapter(userFriendAdapter);
                                    } else {
                                        userFriendAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        });
                    }else{
                        ArrayList<Object> array = new ArrayList<>();
                        array.add(userInfo);
                        array.add(documentSnapshot.getId());
                        array.add(null);
                        friends.add(array);
                        if (finalI == Globals.user.get_friends_list().size() - 1) {
                            userFriendAdapter.setFriends(friends);
                            if (recyclerView.getAdapter() == null) {
                                recyclerView.setAdapter(userFriendAdapter);
                            } else {
                                userFriendAdapter.notifyDataSetChanged();

                            }
                        }
                    }
                }
            });

        }

    }


}