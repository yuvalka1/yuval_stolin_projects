package com.example.chess;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.jetbrains.annotations.NotNull;

import gameEngine.ReviewGame;
import gameEngine.classes.GameDB;
import gameEngine.classes.GameInfo;
import gameEngine.classes.GameInfoAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserProfile extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "UserProfile";
    private static  final int code = 1;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ImageView userImageView;

    GameInfoAdapter gameInfoAdapter;

    public UserProfile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static UserProfile newInstance(String param1, String param2) {
        UserProfile fragment = new UserProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        ImageView edit = view.findViewById(R.id.editProfile);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(container.getContext(), EditProfile.class);
                startActivityForResult(intent, code);
            }
        });

        userImageView = view.findViewById(R.id.userImageView);
        if(Globals.imageByteArray != null){
            Bitmap bmp = BitmapFactory.decodeByteArray(Globals.imageByteArray, 0, Globals.imageByteArray.length);
            userImageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false));
        }

        ImageView userCountryIV = view.findViewById(R.id.userCountryIV);

        TextView userDisplayName = view.findViewById(R.id.userDisplayName);
        TextView blitzScore = view.findViewById(R.id.blitzScore);
        TextView rapidScore = view.findViewById(R.id.rapidScore);
        TextView bulletScore = view.findViewById(R.id.bulletScore);
        TextView num_games = view.findViewById(R.id.num_games);
        ListView listView = view.findViewById(R.id.gameListView);
        ActiveUser user = Globals.getUser();
        userCountryIV.setImageResource(R.drawable.ic_baseline_person_24);
        userDisplayName.setText(Globals.getUserInfo().get_display_name() + " from " + Globals.userInfo.get_country());
        blitzScore.setText(String.valueOf(user._scores.get("Blitz")));
        rapidScore.setText(String.valueOf(user._scores.get("Rapid")));
        bulletScore.setText(String.valueOf(user._scores.get("Bullet")));
        num_games.setText(String.valueOf(user._gameArchive.size()));

        gameInfoAdapter = new GameInfoAdapter(container.getContext(), 0, 0, user._gameArchive);
        listView.setAdapter(gameInfoAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                GameInfo gameInfo = gameInfoAdapter.getItem(position);

                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection(getString(R.string.gameDB)).
                        document(gameInfo.getKey()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        GameDB gameDB = documentSnapshot.toObject(GameDB.class);
                        Intent intent = new Intent(container.getContext(), ReviewGame.class);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(gameDB);
                        intent.putExtra("game", myJson);
                        startActivity(intent);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Log.d(TAG, e.getLocalizedMessage());
                    }
                });
                return true;
            }
        });




        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode  == Activity.RESULT_OK){
            if(Globals.imageByteArray != null){
                Bitmap bmp = BitmapFactory.decodeByteArray(Globals.imageByteArray, 0, Globals.imageByteArray.length);
                userImageView.setImageBitmap(Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false));
            }
        }
    }
}