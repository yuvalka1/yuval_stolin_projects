package com.example.chess;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.PieModel;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;

import java.text.DecimalFormat;
import java.util.ArrayList;

import gameEngine.classes.GameInfo;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserStats#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserStats extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    String game_type = "Rapid";
    public UserStats() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment stats.
     */
    // TODO: Rename and change types and number of parameters
    public static UserStats newInstance(String param1, String param2) {
        UserStats fragment = new UserStats();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_stats, container, false);
        ImageView imageView = view.findViewById(R.id.ivGameType);
        Button changeGameType = view.findViewById(R.id.changeGameType);
        TextView score = view.findViewById(R.id.gameTypeCurrentScore);
        TextView num_games = view.findViewById(R.id.gamesNum);
        TextView winPercentage = view.findViewById(R.id.winPercentage);

        ChangeGameTypeDialog changeGameTypeDialog = new ChangeGameTypeDialog(container.getContext(), new ChangeGameTypeDialog.OnUserChoice() {
            @Override
            public void run(String choice) {
                changeGameType.setText(choice);
                game_type = choice;

                if("Rapid".equals(choice)){
                    imageView.setImageResource(R.drawable.rapid);
                    score.setText(String.valueOf(Globals.getUser().get_scores().get("Rapid")));
                }
                else if("Blitz".equals(choice)){
                    imageView.setImageResource(R.drawable.blitz);
                    score.setText(String.valueOf(Globals.getUser().get_scores().get("Blitz")));
                }
                else{
                    imageView.setImageResource(R.drawable.bullet);
                    score.setText(String.valueOf(Globals.getUser().get_scores().get("Bullet")));
                }
                PieChart mPieChart = view.findViewById(R.id.piechart);
                mPieChart.clearChart();


                int won = 0;
                int lost = 0;
                int tie = 0;

                for(GameInfo gameInfo : Globals.user._gameArchive){
                    if(gameInfo.getGameType().equals(game_type)) {
                        switch (gameInfo.getGameResult()) {
                            case 0:
                                won++;
                                break;
                            case 1:
                                tie++;
                                break;
                            case -1:
                                lost++;
                                break;
                        }
                    }
                }
                int total = won + tie + lost;
                float winP = 0 == total ? (float)0 : ((float)won / (float)total) * 100;

                num_games.setText("number of games " + (String.valueOf(total)));
                winPercentage.setText("Winning percentage: " + winP + " %");
                if(won == 0 && tie == 0 && lost == 0){
                    mPieChart.addPieSlice(new PieModel("there is not data", 0, Color.parseColor("#FE6DA8")));
                    mPieChart.startAnimation();
                }
                else {
                    if(won != 0) {
                        mPieChart.addPieSlice(new PieModel("won", won, Color.parseColor("#FE6DA8")));
                    }
                    if(lost != 0) {
                        mPieChart.addPieSlice(new PieModel("lost", lost, Color.parseColor("#56B7F1")));
                    }
                    if(tie != 0) {
                        mPieChart.addPieSlice(new PieModel("tie", tie, Color.parseColor("#CDA67F")));
                    }
                    mPieChart.startAnimation();
                }
            }
        });
        changeGameTypeDialog.show();
        changeGameTypeDialog.setCancelable(false);
        changeGameType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeGameTypeDialog.show();
            }
        });


        return view;
    }

}