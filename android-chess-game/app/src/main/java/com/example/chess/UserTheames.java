package com.example.chess;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserTheames#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserTheames extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    boolean checked = true;
    public UserTheames() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserTheames.
     */
    // TODO: Rename and change types and number of parameters
    public static UserTheames newInstance(String param1, String param2) {
        return new UserTheames();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_user_theames, container, false);
        Context context = container.getContext();
        SharedPreferences sharedPref  = context.getSharedPreferences(context.getString(R.string.themes), Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        CheckBox box1 = view.findViewById(R.id.box1);
        CheckBox box2 = view.findViewById(R.id.box2);
        checked = sharedPref.getBoolean(context.getString(R.string.themeChoice), true);
        if(checked){box1.setChecked(true);}
        else{box2.setChecked(true);}


        box1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(box1.isChecked()){
                    box1.setChecked(true);
                    box2.setChecked(false);
                    editor.putBoolean(context.getString(R.string.themeChoice), true);
                    editor.apply();
                }
                else{
                    box2.setChecked(true);
                    box1.setChecked(false);
                    editor.putBoolean(context.getString(R.string.themeChoice), false);
                    editor.apply();
                }
            }
        });

        box2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(box2.isChecked()){
                    box2.setChecked(true);
                    box1.setChecked(false);
                    editor.putBoolean(context.getString(R.string.themeChoice), false);
                    editor.apply();
                }
                else{
                    box1.setChecked(true);
                    box2.setChecked(false);
                    editor.putBoolean(context.getString(R.string.themeChoice), true);
                    editor.apply();
                }
            }
        });

        return view;
    }
}