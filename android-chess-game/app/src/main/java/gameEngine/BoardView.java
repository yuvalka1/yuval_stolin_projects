package gameEngine;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.chess.Globals;
import com.example.chess.R;
import com.example.chess.UserFriends;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import chesslogic.Board;
import chesslogic.Game;
import chesslogic.Move;
import chesslogic.OpCodes;
import chesslogic.Piece;
import chesslogic.Point;
import gameEngine.classes.ChatDB;
import gameEngine.classes.GAME_STATE;
import gameEngine.classes.GameDB;
import gameEngine.classes.GameInfo;
import gameEngine.classes.Message;
import gameEngine.classes.MessageAdapter;
import gameEngine.classes.Square;
import gameEngine.classes.ThreadClock;
import gameEngine.classes.UserInfo;
import gameEngine.dialogs.EndGameDialog;
import gameEngine.dialogs.GameOptionsDialog;
import gameEngine.dialogs.PawnPromotionDialog;

public class BoardView extends View  {
    private final boolean choice;
    Square[][] _squares;
    Context _context;
    private static final String TAG = "BoardView";
    Canvas canvas;
    Game _chessgame;
    String _boardStr;
    int size;
    int touch = 0;
    OpCodes result;
    int addTime;
    String _key;
    private boolean turn;
    boolean isWhite;
    DatabaseReference _gameReference;

    DatabaseReference databaseReference;
    FirebaseFirestore db;
    GameDB _gameDB;
    TextView opponentTime, userTime;
    TextView opponentName, myName, movesList;
    ImageView userImage, opponentImage, userCountry, opponentCountry;
    View opponentView;
    View userView;
    View gameView;

    ImageButton options, chat;

    int move_number = 1;
    ValueEventListener gameDBListener;
    Point src, dst;

    UserInfo player1, player2;

    Handler opponentCounter, userCounter;
    ThreadClock userClock, opponentClock;
    boolean whichPlayer;

    //chat layout
    ValueEventListener chatListener;
    MessageAdapter messageAdapter;
    ChatDB chatDB;
    ListView listView;
    View chatView;
    TextInputEditText inputEditText;
    Toolbar chat_toolbar;
    FloatingActionButton sendText;
    DatabaseReference _chatReference;
    private String game_time;


    public void set_context(Context _context) {
        this._context = _context;
    }

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        _context = context;
        src = new Point(-1,-1);
        dst = new Point(-1,-1);
        result = OpCodes.START_GAME;
        chatDB = new ChatDB();
        messageAdapter = new MessageAdapter(context, 0, 0, chatDB.getMessageArrayList());
        choice = _context.getSharedPreferences(_context.getString(R.string.themes), Activity.MODE_PRIVATE).
                getBoolean(_context.getString(R.string.themeChoice), true);
       
    }



    public void setViews(){
        gameView = ((Activity)_context).findViewById(R.id.gameLayout);

        opponentView =  gameView.findViewById(R.id.opponentLayout);
        opponentName = opponentView.findViewById(R.id.userName);
        opponentTime = opponentView.findViewById(R.id.textClock);
        opponentImage = opponentView.findViewById(R.id.userProfileImage);
        opponentCountry = opponentView.findViewById(R.id.country);

        userView = gameView.findViewById(R.id.userLayout);
        myName = userView.findViewById(R.id.userName);
        userTime = userView.findViewById(R.id.textClock);
        userImage = userView.findViewById(R.id.userProfileImage);
        userCountry = userView.findViewById(R.id.country);
        gameView.findViewById(R.id.tv_moves).setSelected(true);
        movesList = gameView.findViewById(R.id.tv_moves);

        options = gameView.findViewById(R.id.options);

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameOptionsDialog gameOptionsDialog = new GameOptionsDialog(_context, new GameOptionsDialog.OnSelectedButton() {
                    @Override
                    public void run() {
                        //abort
                        closeGame();
                        removeListeners();
                        GAME_STATE state;
                        if((isWhite && _gameDB.get_white_moves() != null && !_gameDB.get_white_moves() .isEmpty())
                            || (!isWhite && _gameDB.get_black_moves() != null && !_gameDB.get_black_moves().isEmpty())){
                            state = isWhite ?  GAME_STATE.WHITE_RESIGN : GAME_STATE.BLACK_RESIGN;
                            updateFirestoreDB(-10, -1);
                        }else {state = GAME_STATE.ABORT;}
                        _gameDB.set_state(state);
                        _gameReference.child("_state").setValue(state);
                        //open end game dialog
                        launchEndGameDialog();
                    }
                });
                gameOptionsDialog.show();
            }
        });

        chat = gameView.findViewById(R.id.chat);
        chat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(INVISIBLE);
                chatView.setVisibility(VISIBLE);
            }
        });


        //chat layout views
        chatView = ((Activity)_context).findViewById(R.id.chatView);
        inputEditText = chatView.findViewById(R.id.input);
        chat_toolbar = chatView.findViewById(R.id.chatToolbar);
        sendText = chatView.findViewById(R.id.fab);
        listView = chatView.findViewById(R.id.list_of_messages);
        listView.setAdapter(messageAdapter);


        //chat_toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_black_24);
        chat_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatView.setVisibility(GONE);
                setVisibility(VISIBLE);
            }
        });

        sendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = Objects.requireNonNull(inputEditText.getText()).toString();
                if(!text.isEmpty()){
                    chatDB.add(new Message(text, Globals.userInfo.get_display_name()));
                    _chatReference.setValue(chatDB);
                    messageAdapter.notifyDataSetChanged();
                }
            }
        });


    }

    public void setData(String key, GameDB gameDB, UserInfo p1, UserInfo p2){
        Log.d(TAG, key);
        _key = key;
        _gameDB = gameDB;
        player1 = p1;
        player2 = p2;
        whichPlayer = Globals.user.get_public_key().equals(gameDB.get_player1());
        databaseReference = FirebaseDatabase.getInstance().getReference();
        db = FirebaseFirestore.getInstance();
        _squares = new Square[Board.BOARD_SIZE][Board.BOARD_SIZE];
        //String min = gameDB._minutes;
        opponentCounter = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull android.os.Message msg) {
                int totalSecs = msg.arg1;
                int minutes = (totalSecs % 3600) / 60;
                int seconds = totalSecs % 60;

                opponentTime.setText(String.format("%02d:%02d", minutes, seconds));
                return true;
            }
        });

        userCounter = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull android.os.Message msg) {
                int totalSecs = msg.arg1;
                int minutes = (totalSecs % 3600) / 60;
                int seconds = totalSecs % 60;

                userTime.setText(String.format("%02d:%02d", minutes, seconds));
                return true;
            }
        });
        game_time = gameDB.get_minutes();
        String[] array = game_time.split("\\|");
        int minutes = Integer.parseInt(array[0]);
        if(array.length == 2){
            addTime = Integer.parseInt(array[0]);
        }
        else{
            addTime = 0;
        }
        userClock = new ThreadClock(userCounter, minutes * 60, new ThreadClock.OnFinished() {
            @Override
            public void run() {
                userClock.setFlag(false);
                userClock = null;
                removeListeners();
                updateFirestoreDB(-10, -1);
                _gameDB.set_state(GAME_STATE.TIMEOUT);

                _gameReference.setValue(_gameDB.toMap());
                //closeGame();
                //Looper.prepare();
                //launchEndGameDialog();
                //Toast.makeText(_context, "Your time is over", Toast.LENGTH_LONG).show();
                ((Activity)_context).finish();
            }
        });
        opponentClock = new ThreadClock(opponentCounter, minutes*60, null);
        // true player1
        if (whichPlayer){
            isWhite = gameDB.is_starting();
            _boardStr = gameDB.is_starting() ? _context.getString(R.string.reverseBoard) :
                    _context.getString(R.string.boardStr);
            turn = gameDB.is_starting();
            myName.setText(player1.get_display_name());
            opponentName.setText(player2.get_display_name());

            //set profile image and country
        }
        else {
            isWhite = !gameDB.is_starting();
            _boardStr = !gameDB.is_starting() ? _context.getString(R.string.reverseBoard) :
                    _context.getString(R.string.boardStr);
            turn = !gameDB.is_starting();
            myName.setText(player2.get_display_name());
            opponentName.setText(player1.get_display_name());
            //set profile image and country
        }
        if(isWhite){
            userClock.start();
            opponentClock.setRun(false);
            opponentClock.start();
            opponentTime.setText(minutes + ":00");
        }
        else{
            opponentClock.start();
            userClock.setRun(false);
            userClock.start();
            userTime.setText(minutes + ":00");
        }

        String opponent_image_path = whichPlayer ? player2.get_image_url() : player1.get_image_url();
        if(opponent_image_path != null){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(opponent_image_path);
            storageReference.getBytes(UserFriends.ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    opponentImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false));
                }
            });
        }
        else{
            opponentImage.setImageDrawable(_context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        }

        if(Globals.imageByteArray != null) {
            Bitmap bmp = BitmapFactory.decodeByteArray(Globals.imageByteArray, 0, Globals.imageByteArray.length);
            userImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false));
        }
        else{
            userImage.setImageDrawable(_context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        }


        _chessgame = new Game(_boardStr, isWhite, _context);
        _gameReference = databaseReference.child(FirebasePlayerMatchMaker.GAMES_RECORD).child(key)
                .child(FirebasePlayerMatchMaker.ROOM_ID);

        gameDBListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                GameDB db = snapshot.getValue(GameDB.class);
                //assert db != null;
                if(db == null){return;}
                Log.d(TAG, "db = " + db.toString());
                Log.d(TAG, "isWhite = " + isWhite);
                turn = db.is_turn();
                GAME_STATE state = db.get_state();
                if(state == GAME_STATE.TIMEOUT || state == GAME_STATE.ABORT || GAME_STATE.BLACK_RESIGN == state || GAME_STATE.WHITE_RESIGN == state){
                    updateFirestoreDB(10, 0);
                    closeGame();
                    removeListeners();
                    launchEndGameDialog();
                }
                else if(isWhite == turn) {
                    if ((isWhite && db.get_black_moves() != null && !db.get_black_moves().isEmpty()) ||
                            (!isWhite && db.get_white_moves() != null && !db.get_white_moves().isEmpty())) {
                        _gameDB = new GameDB(db);

                        Move lastMove = new Move(_gameDB.lastMove());
                        result = lastMove.getMoveCode();
                        _chessgame.setOpponentMove(lastMove);
                        String moveNotation = lastMove.getMoveNotaion();
                        if(isWhite){ movesList.append(" " + moveNotation + " "); move_number += 1;}
                        else{movesList.append(move_number + ". " + moveNotation);}
                        src = lastMove.get_src();
                        dst = lastMove.get_dst();
                        opponentClock.setRun(false);
                        userClock.setRun(true);
                        opponentClock.setNum(_gameDB.get_time());

                        //userClock.start();
                        invalidate();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w(TAG, "loadPost:onCancelled", error.toException());
            }
        };
        _gameReference.addValueEventListener(gameDBListener);

        _chatReference = databaseReference.child(FirebasePlayerMatchMaker.GAMES_RECORD).child(_key).child(_context.getString(R.string.chatDB));
        chatListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                boolean flag = false;
                ChatDB db = snapshot.getValue(ChatDB.class);
                if(db == null){return; }
                Message message = db.lastMessage();
                if(message == null || message.getMessageUser().equals(Globals.user.get_public_key())){return;}
                chatDB = db;
                messageAdapter.add(message);
                messageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        };
        _chatReference.addValueEventListener(chatListener);

    }

    private void launchEndGameDialog() {
        EndGameDialog endGameDialog = new EndGameDialog(_context, _gameDB, userImage.getDrawable(), opponentImage.getDrawable(), player1, player2);
        endGameDialog.setCanceledOnTouchOutside(false);
        endGameDialog.show();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(isWhite == turn) {

            Log.d(TAG, "touch =  " + touch);
            if (touch == 0) {
                if (event.getX() > 0 && event.getX() < (size * 8) && event.getY() > 0 && event.getY() < (size * 8)) {
                    int x = (int) (event.getY() / (size));
                    int y = (int) (event.getX() / (size));
                    Log.d(TAG, x + "," + y);
                    src = new Point(x, y);
                    if(_chessgame.pieceExist(src) && _chessgame.checkSelfPiece(src)) {
                        touch++;
                        result = OpCodes.None;
                        invalidate();
                    }

                }
            } else {
                if (event.getX() > 0 && event.getX() < (size * 8) && event.getY() > 0 && event.getY() < (size * 8)) {

                    int new_x = (int) (event.getY() / (size));
                    int new_y = (int) (event.getX() / (size));
                    Log.d(TAG, new_x + "," + new_y);

                    dst = new Point(new_x, new_y);
                    if(!_chessgame.pieceExist(src) && _chessgame.checkSelfPiece(dst)) {
                        src = new Point(dst);
                        result = OpCodes.None;
                        invalidate();
                    }
                    else {
                        touch = 0;

                        Move move = new Move(src, dst);
                        this._chessgame.play(move);
                        result = move.getMoveCode();
                        if (result == OpCodes.VALID || result == OpCodes.CHECK ||
                                result == OpCodes.CASTLING || result == OpCodes.ENPASSENT
                                || result == OpCodes.MATE || result == OpCodes.EAT ||
                                result == OpCodes.STALEMATE) {

                            opponentClock.setRun(true);
                            userClock.setRun(false);
                            userClock.addTime(addTime);
                            _gameDB.set_time(userClock.getNum());

                            updateDatabaseAndGame(move);
                        } else if (result == OpCodes.PROMOTING) {
                            PawnPromotionDialog customDialog = new PawnPromotionDialog(_context, _chessgame.is_turn(), new PawnPromotionDialog.OnSelectedPiece() {
                                @Override
                                public void run(char type) {
                                    _chessgame.executePromotion(move, type);
                                    updateDatabaseAndGame(move);
                                }
                            });
                            customDialog.setCanceledOnTouchOutside(false);
                            customDialog.show();

                        }
                    }
                }
            }
        }

        return super.onTouchEvent(event);

    }

    private void updateDatabaseAndGame(Move move){
        turn = !_gameDB.is_turn();
        Log.d(TAG, isWhite + " updated list moves");
        _gameDB.addMove(isWhite, move);
        _gameDB.set_turn(turn);
        Log.d(TAG, "turn = " + turn);
        Map<String, Object> gametValues = _gameDB.toMap();
        _gameReference.updateChildren(gametValues);
        if(!isWhite){ movesList.append(" " + move.getMoveNotaion() + " "); move_number += 1;}
        else{movesList.append(move_number + ". " + move.getMoveNotaion());}
        invalidate();
    }

    private void updateFirestoreDB(int points, int status){
        Log.d(TAG, "ENTERED updateFirestoreDB");
        String gameType = "Rapid";
        switch (game_time){
            case "10":
            case "30":
            case "15|10":
            case "20":
            case "60":
            case "45|45":
                gameType="Rapid";
                break;

            case "1":
            case "1|1":
            case "2|1":
                gameType="Bullet";
                break;
            case "3":
            case "3|2":
            case "5":
            case "5|5":
                gameType = "Blitz";
                break;
            default:
                break;
        }
        DocumentReference user = db.collection("Users").
                document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
        if(Globals.user.get_scores().get(gameType)  + points > 0){
            Globals.getUser().addPoints(gameType, points);
            user.update("_scores." + gameType, FieldValue.increment(points));
        }

        if(isWhite && _gameDB.get_state() != GAME_STATE.ABORT){
            writeGameToDatabase();
        }
        GameInfo gameInfo = new GameInfo(this.whichPlayer ? _gameDB.get_player2()
                :  _gameDB.get_player1()
                , status, gameType, this._key );
        List<GameInfo> list = Globals.user.get_gameArchive();
        list.add(gameInfo);
        user.update("_gameArchive",list);
    }



    @Override
    protected void onDraw(Canvas canvas) {
        boolean flag = true;
        super.onDraw(canvas);
        size = getWidth() / Board.BOARD_SIZE;
        this.canvas = canvas;
        drawBoard();
        MediaPlayer mediaPlayer = null;
        switch (result){
            case START_GAME:
                mediaPlayer = MediaPlayer.create(_context, R.raw.start_game);
                break;
            case CASTLING:
                mediaPlayer = MediaPlayer.create(_context, R.raw.castling);
                break;
            case CHECK:
            case MATE:
                mediaPlayer = MediaPlayer.create(_context, R.raw.check);
                break;
            case PROMOTING:
            case EAT:
            case VALID:
            case STALEMATE:
            case ENPASSENT:
                mediaPlayer = MediaPlayer.create(_context, R.raw.chess_move);
                break;
            default:
                flag = false;

        }
        if (flag){ mediaPlayer.start();}
        if(result == OpCodes.MATE){
            StringBuilder msg = new StringBuilder();
            String winner;
            if((isWhite && !turn) || (!isWhite && !turn)){winner = "white";
            _gameDB.set_state(GAME_STATE.WHITE_WON);}
            else {winner = "black"; _gameDB.set_state(GAME_STATE.BLACK_WON);}
            msg.append(winner).append(" won by mate");
            Toast.makeText(_context, msg, Toast.LENGTH_LONG).show();
            removeListeners();
            if(!((winner.equals("white") && isWhite) || (winner.equals("black") && !isWhite))){
                closeGame();
                updateFirestoreDB(-10, -1);
            }else{
                updateFirestoreDB(10, 0);
            }
            launchEndGameDialog();
        }
        else if(result == OpCodes.STALEMATE){
            Toast.makeText(_context, "game ended in a tie", Toast.LENGTH_LONG).show();
            updateFirestoreDB(1, 1);
            removeListeners();
            _gameDB.set_state(GAME_STATE.TIE);
            if(isWhite){
                closeGame();
            }

            launchEndGameDialog();
        }
    }

    public void removeListeners(){
        _gameReference.removeEventListener(this.gameDBListener);
    }

    private void closeGame(){

        _gameReference.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable @org.jetbrains.annotations.Nullable DatabaseError error, @NonNull @NotNull DatabaseReference ref) {
                Log.d(TAG, "game closed");
            }
        });

        _chatReference.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable @org.jetbrains.annotations.Nullable DatabaseError error, @NonNull @NotNull DatabaseReference ref) {
                Log.d(TAG, "chat closed");
            }
        });

    }

    private void writeGameToDatabase(){
        db.collection(_context.getString(R.string.gameDB)).document(_key).set(_gameDB).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "game was added to firestore database");
                }
                else{
                    Log.d(TAG, Objects.requireNonNull(task.getException()).getLocalizedMessage());
                }
            }
        });
    }



    public static  final HashMap<Character, Integer> pieces1 = new HashMap<Character, Integer>() {
        {
            put('r', R.drawable.wrook);
            put('n', R.drawable.wknight);
            put('b', R.drawable.wbishop);
            put('k', R.drawable.wking);
            put('q', R.drawable.wqueen);
            put('p', R.drawable.wpawn);
            put('R', R.drawable.brook);
            put('N', R.drawable.bknight);
            put('B', R.drawable.bbishop);
            put('K', R.drawable.bking);
            put('Q', R.drawable.bqueen);
            put('P', R.drawable.bpawn);
        }

    };

    public static final HashMap<Character, Integer>  pieces2 = new HashMap<Character, Integer>() {
        {
            put('r', R.drawable.white_rook);
            put('n', R.drawable.white_knight);
            put('b', R.drawable.white_bishop);
            put('k', R.drawable.white_king);
            put('q', R.drawable.white_queen);
            put('p', R.drawable.white_pawn);
            put('R', R.drawable.black_rook);
            put('N', R.drawable.black_knight);
            put('B', R.drawable.black_bishop);
            put('K', R.drawable.black_king);
            put('Q', R.drawable.black_queen);
            put('P', R.drawable.black_pawn);
        }

    };
    private void drawBoard(){

        HashMap<Character, Integer> pieces = !choice ? pieces1 : pieces2;

        Piece[][] boardArr = _chessgame.get_gameBoard().getBoardArr();
        int x = 0, y = 0, h = canvas.getWidth() / Board.BOARD_SIZE, w = canvas.getWidth() / Board.BOARD_SIZE, color = 0;
        for (int i = 0; i <  _squares.length ; i++){
            for(int j = 0; j < _squares.length; j++){
                if(dst.get_y() == j && dst.get_x() == i ){
                    color = android.R.color.holo_purple;
                }
                else if(src.get_y() == j && src.get_x() == i){
                    color = android.R.color.holo_orange_light;
                }
                else if(i%2 == 0){
                    color = j % 2 == 0 ? R.color.colorBoardLight : R.color.colorBoardDark;
                }
                else{
                    color = j % 2 == 1 ? R.color.colorBoardLight : R.color.colorBoardDark;
                }
                Piece piece =  boardArr[i][j];
                char type = null == piece ? '#' : piece.get_type();

                if(type == '#'){
                    _squares[i][j] = new Square(x,y,w,h,color, _context);
                }
                else{
                    _squares[i][j] = new Square(x,y,w,h,color, _context,  pieces.get(type));
                }
                _squares[i][j].draw(canvas);
                x += w;
            }
            y += h;
            x = 0;

        }
    }
}
