package gameEngine;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.chess.ActiveUser;
import com.example.chess.Globals;
import com.example.chess.R;
import com.example.chess.SignupActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.gson.Gson;

import java.util.Objects;

import gameEngine.classes.GameDB;
import gameEngine.classes.UserInfo;


public class ChessGameActivty extends AppCompatActivity {
    private final String TAG = "ChessGameActivity";


    BoardView boardView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_chess_game);
        boardView = findViewById(R.id.gameBoard);
        Gson gson = new Gson();
        Intent intent = getIntent();
        String key = intent.getStringExtra("key");
        GameDB selectedgame = gson.fromJson(intent.getStringExtra("game"), GameDB.class);
        UserInfo p1 = gson.fromJson(intent.getStringExtra("p1"), UserInfo.class);
        UserInfo p2 = gson.fromJson(intent.getStringExtra("p2"), UserInfo.class);
        boardView.set_context(this);
        boardView.setViews();
        boardView.setData(key, selectedgame, p1, p2);
    }


    @Override
    protected void onStart() {
        super.onStart();
        DocumentReference userReference = FirebaseFirestore.getInstance().collection(getString(R.string.userDB)).document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
        userReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable @org.jetbrains.annotations.Nullable DocumentSnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                if (error != null){
                    Log.d(TAG, error.getLocalizedMessage());}
                else if(value != null && value.exists()){
                    ActiveUser user = value.toObject(ActiveUser.class);
                    Globals.setUser(user);
                }
                else{
                    Log.d(TAG, "Current data: null");
                    Intent intent = new Intent(ChessGameActivty.this, SignupActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}