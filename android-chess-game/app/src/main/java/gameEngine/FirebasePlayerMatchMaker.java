package gameEngine;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.chess.ActiveUser;
import com.example.chess.Globals;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import gameEngine.classes.GAME_STATE;
import gameEngine.classes.GameDB;
import gameEngine.classes.UserInfo;


public class FirebasePlayerMatchMaker {

    public static final String TAG = "MatchMaker";

    public  interface  OnMatchMadeCallback{
        public void run(FirebasePlayerMatchMaker matchMaker);
    }

    public interface OnFailCallback {
        public void onFail();
    }

    public static final String ROOM_ID = "GameRoom";
    public static final String GAMES_RECORD = "OpenGames";

    private DatabaseReference mUserRoomRef;
    private DatabaseReference mOwnChallengeRef;
    private OnMatchMadeCallback mOnComplete;

    private GameDB gameseletced;
    private String key;
    public int mLocalPlayerIndex;
    private boolean mClosed = false;
    private ActiveUser user;
    private String minutes;


    public boolean isClosed() {
        return mClosed;
    }
    FirebasePlayerMatchMaker(DatabaseReference userRoomRef, ActiveUser activeUser, String min  ,  OnMatchMadeCallback onComplete) {
        mUserRoomRef = userRoomRef;
        mOnComplete = onComplete;
        user = activeUser;
        minutes = min;
    }

    public void setGameData(ActiveUser activeUser, String min ){user = activeUser; minutes = min;}

    public GameDB getGameseletced() {
        return gameseletced;
    }

    public String getKey() {
        return key;
    }

    protected Matcher mMatcher;
    protected SelfChallengeManager mSelfChallengeManager;
    protected SelfChallengeCanceller mSelfChallengeCanceller;

    public void findMatch(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                OnFailCallback onMatchNotFoundFallback = new OnFailCallback() {
                    @Override
                    public void onFail() {
                        mMatcher = null;
                        mSelfChallengeManager = new SelfChallengeManager();
                        mUserRoomRef.runTransaction(mSelfChallengeManager);
                    }
                };
                mMatcher = new Matcher(onMatchNotFoundFallback);
                mUserRoomRef.runTransaction(mMatcher);
            }
        }).start();
    }

    public void stop() {
        if (mSelfChallengeManager == null ||
                mSelfChallengeCanceller != null) {
            return;
        }

        mSelfChallengeCanceller = new SelfChallengeCanceller(mSelfChallengeManager);
        mUserRoomRef.child(key).runTransaction(mSelfChallengeCanceller);
    }


    private boolean isChallengeCompat(GameDB  oppoChallenge) {
        return oppoChallenge.get_state() == GAME_STATE.OPEN && oppoChallenge.get_player2() == null
                && oppoChallenge.get_minutes().equals(minutes) &&
                ! user.equals(oppoChallenge.get_player1()) ;
    }

    private boolean mIsThisOpener;

    public boolean ismIsThisOpener(){return mIsThisOpener;}

    protected void onMatchFound(boolean isOpener) {
        mIsThisOpener = isOpener;
        mLocalPlayerIndex = (isOpener) ? 1 : 0;
        mOnComplete.run(this);
    }

    public boolean isThisOpener() {
        return mIsThisOpener;
    }




    protected class Matcher implements Transaction.Handler{
        private GameDB  mSelectedChallenge = null;
        private final OnFailCallback mFailCallback;


        public Matcher(@Nullable OnFailCallback failCallback) {
            mFailCallback = failCallback;
        }

        @NonNull
        @Override
        public Transaction.Result doTransaction(@NonNull MutableData currentData) {
            for (MutableData challengeData : currentData.getChildren()) {
                final GameDB  postedChallenge = challengeData.child(ROOM_ID).getValue(GameDB.class);
                assert postedChallenge != null;
                if (isChallengeCompat(postedChallenge)) {
                    postedChallenge.set_player2(Globals.user.get_public_key());
                    postedChallenge.set_state(GAME_STATE.ACTIVE);
                    mSelectedChallenge = postedChallenge;
                    key = challengeData.getKey();
                    assert key != null;
                    Map<String, Object> gametValues = mSelectedChallenge.toMap();
                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put(key + "/" + ROOM_ID, gametValues);
                    mUserRoomRef.updateChildren(childUpdates);
                    challengeData.setValue(null);
                    return Transaction.success(currentData);
                }
            }



            Log.d(TAG, "Didn't find any matching challenge");

            return Transaction.success(currentData);
        }

        @Override
        public void onComplete(@Nullable DatabaseError error, boolean committed, @Nullable DataSnapshot currentData) {
            Log.d("MatchMaker.Matcher", "entered on complete");
            if (mSelectedChallenge != null) {
                gameseletced = mSelectedChallenge;
                //mUserRoomRef = null;
                Log.d(TAG, "Found match, onComplete");

                //mSelfChallengeManager = null;
                //onMatchFound(true);
                onMatchFound(false);
            } else if (mFailCallback != null) {
                mFailCallback.onFail();
            }
        }
    }

    protected class SelfChallengeManager implements Transaction.Handler, ValueEventListener {

        protected final GameDB mUploadedChallenge;
        protected DatabaseReference mChallengeRef;
        protected DatabaseReference mGameRecordRef;

        public SelfChallengeManager() {
            Random random = new Random();
            boolean _starting = random.nextBoolean();
            UserInfo userInfo = Globals.getUserInfo();
            this.mUploadedChallenge = new GameDB(Globals.getUser().get_public_key(), null, GAME_STATE.OPEN, _starting, minutes);
        }

        @NonNull
        @Override
        public Transaction.Result doTransaction(@NonNull MutableData currentData) {
            mGameRecordRef = mUserRoomRef;
            mChallengeRef = mUserRoomRef.push();
            key = mChallengeRef.getKey();
            Log.d(TAG, "key = " + key);
            mChallengeRef.child(ROOM_ID).setValue(mUploadedChallenge);
            mChallengeRef.child(ROOM_ID).addValueEventListener(this);
            return Transaction.success(currentData);
        }

        @Override
        public void onComplete(@Nullable DatabaseError error, boolean committed, @Nullable DataSnapshot currentData) {
            Log.d("MatchMaker", "Published player challenge");
            gameseletced = this.mUploadedChallenge;
        }

        @Override
        public void onDataChange(@NonNull DataSnapshot snapshot) {
            if (snapshot.getValue() == null ) {
                Log.d("MatchMaker", "get value id null");
                //mChallengeRef = null;
                //mGameRecordRef = null;
                //mSelfChallengeManager = null;
                //onMatchFound(true);
            }
            else if(Objects.requireNonNull(snapshot.getValue(GameDB.class)).get_player2() != null){
                Log.d("MatchMaker", "different user joined my game");

                //assert mChallengeRef != null;
                mChallengeRef.child(ROOM_ID).removeEventListener(this);
                mGameRecordRef = null;
                mChallengeRef = null;
                mUserRoomRef = null;
                mSelfChallengeManager = null;
                // mUserRoomRef = null;
                //key = snapshot.getKey();
                mUploadedChallenge.set_player2(
                        Objects.requireNonNull(snapshot.getValue(GameDB.class)).get_player2());
                mUploadedChallenge.set_state(GAME_STATE.ACTIVE);
                onMatchFound(true);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError error) {
            Log.d("MatchMaker", "Cancelled: " + error.getMessage());
        }
    }

    protected class SelfChallengeCanceller implements Transaction.Handler{
        private final SelfChallengeManager mChallenger;

        private SelfChallengeCanceller(SelfChallengeManager challenger) {
            Log.d("MatchMaker.Cancel", "Opened cancel request");
            this.mChallenger = challenger;
        }

        @NonNull
        @Override
        public Transaction.Result doTransaction(@NonNull MutableData currentData) {
            mChallenger.mChallengeRef.child(ROOM_ID).removeEventListener(mChallenger);
            //FirebaseDatabase.getInstance().getReference().child("Games").child(key).removeValue();
            mChallenger.mChallengeRef.removeValue();
            /*
            final String challengeKey = mChallenger.mChallengeRef.getKey();

            for (MutableData challengeNode : currentData.getChildren()) {
                assert challengeKey != null;
                if (Objects.requireNonNull(challengeNode.getKey()).contentEquals(challengeKey)) {
                    challengeNode.child(key).setValue(null);
                }
            }


             */
            return Transaction.success(currentData);
        }

        @Override
        public void onComplete(@Nullable DatabaseError error, boolean committed, @Nullable DataSnapshot currentData) {
            mChallenger.mChallengeRef = null;
        }
    }





}
