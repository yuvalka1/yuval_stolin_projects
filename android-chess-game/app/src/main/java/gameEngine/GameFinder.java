package gameEngine;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.chess.ActiveUser;
import com.example.chess.AuthenticationActivity;
import com.example.chess.Globals;
import com.example.chess.R;
import com.example.chess.SignupActivity;
import com.example.chess.UserAccount;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.gson.Gson;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import java.util.Objects;

import gameEngine.SendNotificationPack.SendNotification;
import gameEngine.classes.GameDB;
import gameEngine.classes.UserInfo;

public class GameFinder extends AppCompatActivity {
    private static final String TAG = "GameFinder";
    DatabaseReference databaseReference;
    FirebasePlayerMatchMaker firebasePlayerMatchMaker;
    ShimmerTextView tv;
    Shimmer shimmer;
    Button select_time, search;
    private FirebaseFirestore db;
    String minutes;
    int LAUNCH_SECOND_ACTIVITY = 1;
    boolean started = false;
    SharedPreferences sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_game_finder);
        db = FirebaseFirestore.getInstance();
        search = findViewById(R.id.seachGame);
        select_time = findViewById(R.id.gameTime);
        tv = (ShimmerTextView) findViewById(R.id.shimmer_tv);
        databaseReference = FirebaseDatabase.getInstance().getReference().child(FirebasePlayerMatchMaker.GAMES_RECORD);
        String key;
        Intent intent = getIntent();
        try {
            key = intent.getExtras().getString("key");
            Toast.makeText(this, "your game will start in few moments", Toast.LENGTH_LONG).show();
            select_time.setClickable(false);
            search.setClickable(false);
        }
        catch (Exception e){
            key = null;
        }
        if(key != null){
            String finalKey = key;
            databaseReference.child(finalKey).child(FirebasePlayerMatchMaker.ROOM_ID).get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                @Override
                public void onSuccess(DataSnapshot dataSnapshot) {
                    GameDB gameDB = dataSnapshot.getValue(GameDB.class);
                    assert gameDB != null;
                    gameDB.set_player2(Globals.getUser().get_public_key());
                    databaseReference.child(finalKey).child(FirebasePlayerMatchMaker.ROOM_ID).setValue(gameDB.toMap()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            startIntent(finalKey, gameDB);
                        }
                    });

                }
            });


        }
        else {
            sharedPref = getSharedPreferences(
                    getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String time = sharedPref.getString(getString(R.string.saved_high_score_key), "10");
            select_time.setText(time);
            shimmer = new Shimmer();
            shimmer.setRepeatCount(0)
                    .setDuration(500)
                    .setStartDelay(300)
                    .setDirection(Shimmer.ANIMATION_DIRECTION_RTL);
            shimmer.start(tv);
            minutes = time;

            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!started) {
                        started = true;
                        search.setText("Cancel");

                        minutes = select_time.getText().toString();
                        minutes = minutes.replaceAll("\\s+", "");
                        minutes = minutes.replaceAll("min", "");
                        firebasePlayerMatchMaker = new FirebasePlayerMatchMaker(databaseReference, Globals.getUser(), minutes, new FirebasePlayerMatchMaker.OnMatchMadeCallback() {
                            @Override
                            public void run(FirebasePlayerMatchMaker matchMaker) {
                                firebasePlayerMatchMaker = null;
                                databaseReference = null;
                                startIntent(matchMaker.getKey(), matchMaker.getGameseletced());
                            }
                        });
                        //firebasePlayerMatchMaker.setGameData(Globals.getUser(), "5");

                        firebasePlayerMatchMaker.findMatch();
                    } else {

                        started = false;
                        search.setText("Play");
                        if (firebasePlayerMatchMaker != null) {
                            firebasePlayerMatchMaker.stop();
                        }
                    }
                }
            });


            select_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(GameFinder.this, GameTimeActivity.class);
                    startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);
                }
            });
        }

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK){
                assert data != null;
                String result=data.getStringExtra("time_selected");
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.saved_high_score_key), result);
                editor.apply();
                select_time.setText(result);
            }

        }
    }

    @Override
     protected void onDestroy() {
         super.onDestroy();
         if(firebasePlayerMatchMaker != null){
             firebasePlayerMatchMaker.stop();
         }
     }

    @Override
    protected void onStart() {
        super.onStart();

        DocumentReference userReference = db.collection(getString(R.string.userDB)).document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
        userReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable @org.jetbrains.annotations.Nullable DocumentSnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                if (error != null){
                    Log.d(TAG, error.getLocalizedMessage());}
                else if(value != null && value.exists()){
                    ActiveUser user = value.toObject(ActiveUser.class);
                    Globals.setUser(user);
                }
                else{
                    Log.d(TAG, "Current data: null");
                    Intent intent = new Intent(GameFinder.this, AuthenticationActivity.class);
                    startActivity(intent);
                }
            }
        });


    }

    public static  interface OnPublicDataCallback{
        void run(UserInfo userInfo);
    }

    private  void getUserData(String public_key, OnPublicDataCallback onPublicDataCallback){
        FirebaseFirestore.getInstance().collection(getString(R.string.userPublicData)).
                document(public_key).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                onPublicDataCallback.run(documentSnapshot.toObject(UserInfo.class));
            }
        });
    }

    public void startIntent(String key, GameDB gameDB  ) {
        if(gameDB.get_player2().equals(Globals.user.get_public_key())){
            getUserData(gameDB.get_player1(), new GameFinder.OnPublicDataCallback() {
                @Override
                public void run(UserInfo userInfo) {
                    Intent intent = new Intent(GameFinder.this, ChessGameActivty.class);
                    intent.putExtra("key", key);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(gameDB);
                    intent.putExtra("game", myJson);
                    intent.putExtra("p1", gson.toJson(userInfo));
                    intent.putExtra("p2", gson.toJson(Globals.getUserInfo()));
                    finish();
                    startActivity(intent);
                }
            });
        }else{
            getUserData(gameDB.get_player2(), new GameFinder.OnPublicDataCallback() {
                @Override
                public void run(UserInfo userInfo) {
                    Intent intent = new Intent(GameFinder.this, ChessGameActivty.class);
                    intent.putExtra("key", key);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(gameDB);
                    intent.putExtra("game", myJson);
                    intent.putExtra("p1", gson.toJson(Globals.getUserInfo()));
                    intent.putExtra("p2", gson.toJson(userInfo));
                    finish();
                    startActivity(intent);
                }
            });
        }

    }


}