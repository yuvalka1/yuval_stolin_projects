package gameEngine;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.TransitionInflater;
import android.view.View;
import android.widget.Button;

import com.example.chess.R;

import java.util.Objects;

public class GameTimeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_game_time);
        setupWindowAnimations();

    }

    private void setupWindowAnimations() {
        Fade fade = (Fade) TransitionInflater.from(this).inflateTransition(R.transition.activity_fade);
        getWindow().setEnterTransition(fade);
    }

    @Override
    public void onClick(View v) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("time_selected",((Button) v).getText());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}