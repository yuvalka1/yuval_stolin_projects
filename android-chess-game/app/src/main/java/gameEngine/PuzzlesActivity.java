package gameEngine;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.chess.R;

import java.util.Objects;

public class PuzzlesActivity extends AppCompatActivity {
    PuzzlesView puzzlesView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_puzzles);
        puzzlesView = findViewById(R.id.puzzleBoard);
        puzzlesView.setContext(this);
        puzzlesView.setViews();
    }
}