package gameEngine;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.chess.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import chesslogic.Board;
import chesslogic.Game;
import chesslogic.Move;
import chesslogic.OpCodes;
import chesslogic.Piece;
import chesslogic.Point;
import gameEngine.classes.Square;

public class PuzzlesView extends View  {
    private static final String TAG = "PuzzlesView";
    private final boolean choice;
    Context _context;
    private String [] puzzle1 = {
                "#####R##",
                "#####K##",
                "##P#r###",
                "##P####p",
                "##b###P#",
                "#p######",
                "##R#####",
                "#####k##"};
    private ArrayList<Move> moves1 = new ArrayList<Move>(){
        {
            add(new Move(new Point(2,4), new Point(6, 4), OpCodes.VALID));
            add(new Move(new Point(1,5), new Point(2,5), OpCodes.VALID));
            add(new Move(new Point(6,4), new Point(6,2), OpCodes.VALID));
        }
    };
    private String title1 = "White: Winning position in 2";

    private String[] puzzle2 = {
            "########",
            "########",
            "######P#",
            "#######K",
            "###R##np",
            "b#####pk",
            "##BR#r##",
            "########"
    };
    private ArrayList<Move> moves2 = new ArrayList<Move>(){
        {
            add(new Move(new Point(4,6), new Point(2, 5), OpCodes.CHECK));
            add(new Move(new Point(3,7), new Point(2,7), OpCodes.VALID));
            add(new Move(new Point(5,0), new Point(0,5), OpCodes.MATE));
        }
    };
    private String title2 = "White: Winning Position in 2";

    private String[] puzzle3 = {
            "########",
            "#k#r##Rp",
            "##qb#P##",
            "##p#####",
            "#p##p###",
            "#PnbP##P",
            "PB#W####",
            "K#####R#"
    };
    private ArrayList<Move> moves3 = new ArrayList<Move>(){
        {
            add(new Move(new Point(1,6), new Point(1, 3), OpCodes.CHECK));
            add(new Move(new Point(2,2), new Point(1,3), OpCodes.VALID));
            add(new Move(new Point(7,6), new Point(1,6), OpCodes.VALID));
            add(new Move(new Point(1,1), new Point(2,2), OpCodes.VALID));
            add(new Move(new Point(1,6), new Point(1,3), OpCodes.VALID));
        }
    };
    private String title3 = "Black: Winning Position in 3";



    private String[] puzzle4 = {
            "KBk#####",
            "PP######",
            "#p######",
            "########",
            "########",
            "########",
            "########",
            "r#######"
    };
    private ArrayList<Move> moves4 = new ArrayList<Move>(){
        {
            add(new Move(new Point(7,0), new Point(2, 0), OpCodes.VALID));
            add(new Move(new Point(1,1), new Point(2,0), OpCodes.VALID));
            add(new Move(new Point(2,1), new Point(1,1), OpCodes.MATE));
        }
    };
    private String title4 = "White: Mate in 2";


    private String[] puzzle5 = {
            "#R#####K",
            "######PP",
            "#######n",
            "########",
            "########",
            "########",
            "q#######",
            "########"
    };
    private ArrayList<Move> moves5 = new ArrayList<Move>(){
        {
            add(new Move(new Point(6,0), new Point(0, 6), OpCodes.CHECK));
            add(new Move(new Point(0,1), new Point(0,6), OpCodes.VALID));
            add(new Move(new Point(2,7), new Point(1,5), OpCodes.MATE));
        }
    };
    private String title5 = "White: Mate in 2";

    ArrayList<Puzzle> puzzleArrayList = new ArrayList<Puzzle>(){
        {
            add(new Puzzle(puzzle1, moves1, title1));
            add(new Puzzle(puzzle2, moves2, title2));
            add(new Puzzle(puzzle3, moves3, title3));
            add(new Puzzle(puzzle4, moves4, title4));
            add(new Puzzle(puzzle5, moves5, title5));
        }
    };

    Game chessgame;
    Square[][] _squares;
    private Canvas canvas;
    int puzzle_num;
    private int touch;
    int move;
    Point src, dst;
    private int size;
    private RelativeLayout l;
    private TextView title;


    public PuzzlesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this._context = context;
        puzzle_num = 0;
        touch = 0;
        this.move = 0;
        Collections.shuffle(puzzleArrayList);
        chessgame = new Game(puzzleArrayList.get(puzzle_num).boardStr);
        _squares = new Square[Board.BOARD_SIZE][Board.BOARD_SIZE];
        src = new Point(-1,-1);
        dst = new Point(-1,-1);
        choice = _context.getSharedPreferences(_context.getString(R.string.themes), Activity.MODE_PRIVATE).
                getBoolean(_context.getString(R.string.themeChoice), true);
    }

    public void setViews(){
        l = ((Activity)_context).findViewById(R.id.relativeLayout);
        title = l.findViewById(R.id.puzzleTitle);
        title.setText(puzzleArrayList.get(puzzle_num).title);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (touch == 0) {
            if (event.getX() > 0 && event.getX() < (size * 8) && event.getY() > 0 && event.getY() < (size * 8)) {
                int x = (int) (event.getY() / (size));
                int y = (int) (event.getX() / (size));
                Log.d(TAG, x + "," + y);
                src = new Point(x, y);
                if(chessgame.pieceExist(src)) {
                    touch++;
                    invalidate();
                }
            }
        }
        else {
            if (event.getX() > 0 && event.getX() < (size * 8) && event.getY() > 0 && event.getY() < (size * 8)) {
                int new_x = (int) (event.getY() / (size));
                int new_y = (int) (event.getX() / (size));
                Log.d(TAG, new_x + "," + new_y);
                dst = new Point(new_x, new_y);
                if(dst.equals(src)){
                    return super.onTouchEvent(event);
                }
                touch = 0;
                Move puzzleMove = puzzleArrayList.get(puzzle_num).moves.get(move);
                if(puzzleMove.get_src().equals(src) && puzzleMove.get_dst().equals(dst)){
                    chessgame.play_without_validation(puzzleMove);
                    move++;
                    invalidate();
                    if(move == puzzleArrayList.get(puzzle_num).moves.size()){
                        puzzle_num++;
                        if(puzzle_num == puzzleArrayList.size()){
                            new AlertDialog.Builder(_context)
                                    .setTitle("Well Done")
                                    .setMessage("You finished all your puzzles?")
                                    .setPositiveButton("Go to Home Page", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ((Activity)_context).finish();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setCancelable(false)
                                    .show();
                        }
                        else{
                            new AlertDialog.Builder(_context)
                                    .setTitle("Well Done")
                                    .setMessage("You successfully completed  the  puzzles")
                                    .setPositiveButton("Try Another puzzle", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            touch = 0;
                                            move = 0;
                                            chessgame = new Game(puzzleArrayList.get(puzzle_num).boardStr);
                                            title.setText(puzzleArrayList.get(puzzle_num).title);
                                            src = new Point(-1,-1);
                                            dst = new Point(-1,-1);
                                            invalidate();
                                        }
                                    })
                                    .setNegativeButton("Go to Home Page", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ((Activity)_context).finish();
                                        }
                                    })
                                    .setCancelable(false)
                                    .show();
                        }

                    }
                    else {
                        Snackbar snackbar = Snackbar.make(l,"correct move",Snackbar.LENGTH_LONG);
                        snackbar.show();
                        puzzleMove = puzzleArrayList.get(puzzle_num).moves.get(move);
                        src = puzzleMove.get_src();
                        dst = puzzleMove.get_dst();
                        chessgame.play_without_validation(puzzleMove);
                        move++;
                        invalidate();

                    }
                }
                else{
                    Snackbar snackbar = Snackbar.make(l,"try again",Snackbar.LENGTH_LONG);
                    snackbar.show();
                    src = new Point(-1,-1);
                    dst = new Point(-1,-1);
                    invalidate();
                }

            }
        }
        return super.onTouchEvent(event);
    }



    public void setContext(Context context) {
        this._context = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        boolean flag = true;
        super.onDraw(canvas);
        size = getWidth() / Board.BOARD_SIZE;
        this.canvas = canvas;
        drawBoard();


    }
    private void drawBoard(){

        HashMap<Character, Integer> pieces = !choice ? BoardView.pieces1 : BoardView.pieces2;

        Piece[][] boardArr = chessgame.get_gameBoard().getBoardArr();;
        int x = 0, y = 0, h = canvas.getWidth() / Board.BOARD_SIZE, w = canvas.getWidth() / Board.BOARD_SIZE, color = 0;
        for (int i = 0; i <  _squares.length ; i++){
            for(int j = 0; j < _squares.length; j++){
                if(dst.get_y() == j && dst.get_x() == i ){
                    color = android.R.color.holo_purple;
                }
                else if(src.get_y() == j && src.get_x() == i){
                    color = android.R.color.holo_orange_light;
                }
                else if(i%2 == 0){
                    color = j % 2 == 0 ? R.color.colorBoardLight : R.color.colorBoardDark;
                }
                else{
                    color = j % 2 == 1 ? R.color.colorBoardLight : R.color.colorBoardDark;
                }
                Piece piece =  boardArr[i][j];
                char type = null == piece ? '#' : piece.get_type();

                if(type == '#'){
                    _squares[i][j] = new Square(x,y,w,h,color, _context);
                }
                else{
                    _squares[i][j] = new Square(x,y,w,h,color, _context,  pieces.get(type));
                }
                _squares[i][j].draw(canvas);
                x += w;
            }
            y += h;
            x = 0;

        }
    }





    public class Puzzle{
        String boardStr;
        ArrayList<Move> moves;
        String title;
        public Puzzle(String boardStr, ArrayList<Move> moves, String title) {
            this.boardStr = boardStr;
            this.moves = moves;
            this.title = title;
        }
        public Puzzle(String [] boardArray, ArrayList<Move> moves, String title){
            this.moves = moves;
            boardStr = "";
            for (String string : boardArray) {
                boardStr += string;
            }
            this.title = title;
        }


        public String getBoardStr() {
            return boardStr;
        }

        public void setBoardStr(String boardStr) {
            this.boardStr = boardStr;
        }

        public ArrayList<Move> getMoves() {
            return moves;
        }

        public void setMoves(ArrayList<Move> moves) {
            this.moves = moves;
        }
    }




}
