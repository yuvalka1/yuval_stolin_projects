package gameEngine;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.chess.Globals;
import com.example.chess.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.Objects;

import gameEngine.SendNotificationPack.SendNotification;
import gameEngine.classes.GameDB;
import gameEngine.classes.UserInfo;

public class ReviewGame extends AppCompatActivity {

    ReviewGameView reviewGameView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_review_game);
        reviewGameView = findViewById(R.id.reviewgameBoard);
        Intent intent = getIntent();
        Gson gson = new Gson();
        GameDB selectedgame = gson.fromJson(intent.getStringExtra("game"), GameDB.class);

        reviewGameView.set_context(this);
        reviewGameView.setViews();

        if(selectedgame.get_player2().equals(Globals.user.get_public_key())){
            getUserData(selectedgame.get_player1(), new GameFinder.OnPublicDataCallback() {
                @Override
                public void run(UserInfo userInfo) {
                    reviewGameView.setData(selectedgame, userInfo, Globals.getUserInfo());
                }
            });
        }else{
            getUserData(selectedgame.get_player2(), new GameFinder.OnPublicDataCallback() {
                @Override
                public void run(UserInfo userInfo) {
                    reviewGameView.setData(selectedgame,Globals.getUserInfo(), userInfo);
                }
            });
        }


    }

    private void getUserData(String public_key, GameFinder.OnPublicDataCallback onPublicDataCallback){
        FirebaseFirestore.getInstance().collection(getString(R.string.userPublicData)).
                document(public_key).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                onPublicDataCallback.run(documentSnapshot.toObject(UserInfo.class));
            }
        });
    }

}