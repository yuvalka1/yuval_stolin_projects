package gameEngine;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.chess.Globals;
import com.example.chess.R;
import com.example.chess.UserFriends;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;

import chesslogic.Board;
import chesslogic.Game;
import chesslogic.Move;
import chesslogic.OpCodes;
import chesslogic.Piece;
import gameEngine.classes.GameDB;
import gameEngine.classes.Square;
import gameEngine.classes.UserInfo;

public class ReviewGameView extends View {
    private static final String TAG = "ReviewGameView";
    private final boolean choice;
    private  Context _context;
    ArrayList<Board> boardArrayMap;
    ArrayList<OpCodes> opCodesArrayList;
    private GameDB _gameDB;
    private Canvas canvas;
    private boolean isWhite;
    private String _boardStr;
    TextView opponentTime, userTime;
    TextView opponentName, myName, movesList;
    ImageView userImage, opponentImage, userCountry, opponentCountry;
    View opponentView;
    View userView;
    View gameView;
    Square[][] _squares;
    ImageButton forward, backwards;

    UserInfo player1, player2;

    private int index = 0, max_size = 0;


    public ReviewGameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        _context = context;
        boardArrayMap = new ArrayList<>();
        _squares = new Square[Board.BOARD_SIZE][Board.BOARD_SIZE];
        opCodesArrayList = new ArrayList<>();
        choice = _context.getSharedPreferences(_context.getString(R.string.themes), Activity.MODE_PRIVATE).
                getBoolean(_context.getString(R.string.themeChoice), true);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvas = canvas;
        if(this.boardArrayMap.size() > 0) {
            drawBoard();
            OpCodes result = opCodesArrayList.get(index);
            boolean flag = true;
            MediaPlayer mediaPlayer = null;
            switch (result){
                case START_GAME:
                    mediaPlayer = MediaPlayer.create(_context, R.raw.start_game);
                    break;
                case CASTLING:
                    mediaPlayer = MediaPlayer.create(_context, R.raw.castling);
                    break;
                case CHECK:
                case MATE:
                    mediaPlayer = MediaPlayer.create(_context, R.raw.check);
                    break;
                case PROMOTING:
                case EAT:
                case VALID:
                case STALEMATE:
                case ENPASSENT:
                    mediaPlayer = MediaPlayer.create(_context, R.raw.chess_move);
                    break;
                default:
                    flag = false;

            }
            if (flag){ mediaPlayer.start();}
        }
    }

    public void setViews(){
        gameView = ((Activity)_context).findViewById(R.id.gameLayout);

        opponentView =  gameView.findViewById(R.id.opponentLayout);
        opponentName = opponentView.findViewById(R.id.userName);
        opponentTime = opponentView.findViewById(R.id.textClock);
        opponentImage = opponentView.findViewById(R.id.userProfileImage);
        opponentCountry = opponentView.findViewById(R.id.country);

        userView = gameView.findViewById(R.id.userLayout);
        myName = userView.findViewById(R.id.userName);
        userTime = userView.findViewById(R.id.textClock);
        userImage = userView.findViewById(R.id.userProfileImage);
        userCountry = userView.findViewById(R.id.country);

        movesList = gameView.findViewById(R.id.tv_moves);
        forward = gameView.findViewById(R.id.forward);
        backwards = gameView.findViewById(R.id.back);

        forward.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index < max_size -1){
                    index++;
                    invalidate();
                }
            }
        });

        backwards.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index > 0){
                    index--;
                    invalidate();
                }
            }
        });
    }
    private void drawBoard(){

        HashMap<Character, Integer> pieces = !choice ? BoardView.pieces1 : BoardView.pieces2;

        Piece[][] boardArr = boardArrayMap.get(index).getBoardArr();
        int x = 0, y = 0, h = canvas.getWidth() / Board.BOARD_SIZE, w = canvas.getWidth() / Board.BOARD_SIZE, color = 0;
        for (int i = 0; i <  _squares.length ; i++){
            for(int j = 0; j < _squares.length; j++){
                if(i%2 == 0){
                    color = j % 2 == 0 ? R.color.colorBoardLight : R.color.colorBoardDark;
                }
                else{
                    color = j % 2 == 1 ? R.color.colorBoardLight : R.color.colorBoardDark;
                }
                Piece piece =  boardArr[i][j];
                char type = null == piece ? '#' : piece.get_type();

                if(type == '#'){
                    _squares[i][j] = new Square(x,y,w,h,color, _context);
                }
                else{
                    _squares[i][j] = new Square(x,y,w,h,color, _context,  pieces.get(type));
                }
                _squares[i][j].draw(canvas);
                x += w;
            }
            y += h;
            x = 0;

        }
    }



    public void set_context(Context _context) {
        this._context = _context;
    }



    public void setData(GameDB game, UserInfo p1, UserInfo p2){
        Log.d("Game", game.toString());
        _gameDB = game;
        player1 = p1;
        player2 = p2;
        boolean whichPlayer = Globals.user.get_public_key().equals(_gameDB.get_player1());
        if (whichPlayer) {
            isWhite = _gameDB.is_starting();
            _boardStr = _gameDB.is_starting() ? _context.getString(R.string.reverseBoard) : _context.getString(R.string.boardStr);
            myName.setText(player1.get_display_name());
            opponentName.setText(player2.get_display_name());
        }
        else{
            isWhite = !_gameDB.is_starting();
            _boardStr = !_gameDB.is_starting() ? _context.getString(R.string.reverseBoard) :
                    _context.getString(R.string.boardStr);
            myName.setText(player2.get_display_name());
            opponentName.setText(player1.get_display_name());
        }

        String opponent_image_path = whichPlayer ? player2.get_image_url() : player1.get_image_url();
        if(opponent_image_path != null){
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(opponent_image_path);
            storageReference.getBytes(UserFriends.ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    opponentImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false));
                }
            });
        }
        else{
            opponentImage.setImageDrawable(_context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        }

        if(Globals.imageByteArray != null) {
            Bitmap bmp = BitmapFactory.decodeByteArray(Globals.imageByteArray, 0, Globals.imageByteArray.length);
            userImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false));
        }
        else{
            userImage.setImageDrawable(_context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        }


        ArrayList<Move> white_moves = game.get_white_moves();
        ArrayList<Move> black_moves = game.get_black_moves();
        int j = 0;
        boolean turn = true;
        Game _chessgame = new Game(_boardStr);

        boardArrayMap.add(_chessgame.get_gameBoard());//initial board
        opCodesArrayList.add(OpCodes.START_GAME);
        max_size = white_moves.size() + black_moves.size();
        for(int i = 0; i < max_size; i++){
            if(!isWhite){
                Move move = null;
                if(turn){
                    if(white_moves.size() > j) {
                        move = white_moves.get(j);
                        Log.d("Game", i + move.toString());
                        _chessgame.setOpponentMove(white_moves.get(j));
                        opCodesArrayList.add(white_moves.get(j).getMoveCode());
                    }
                    turn = false;
                }
                else{
                    move = black_moves.get(j);
                    Log.d("Game", i + move.toString());
                    _chessgame.play_without_validation(black_moves.get(j));
                    opCodesArrayList.add(black_moves.get(j).getMoveCode());
                    turn = true;
                    j++;
                }
                _chessgame.printBoard();
            }
            else{
                if(turn){
                    if(white_moves.size() > j) {
                        _chessgame.play_without_validation(white_moves.get(j));
                        turn = false;
                        opCodesArrayList.add(white_moves.get(j).getMoveCode());
                    }
                }
                else{
                    _chessgame.setOpponentMove(black_moves.get(j));
                    turn = true;
                    opCodesArrayList.add(black_moves.get(j).getMoveCode());
                    j++;
                }
            }
            boardArrayMap.add(_chessgame.get_gameBoard());
        }
        max_size++;
        invalidate();
    }
}
