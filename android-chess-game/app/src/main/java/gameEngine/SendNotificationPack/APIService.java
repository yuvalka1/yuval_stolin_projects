package gameEngine.SendNotificationPack;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAOycwplk:APA91bHdvh8M0XDES95qNXXY1AjFNqp76I2W9-wxOfgqBnRIYmtUQ0JB24s3SEcM2CeQkcPS4jsXm7ls9GGz25DFcf2TKCmaB4bQuCk8maD03S8kGigaTrkSl9EBMhZTXZo-kT8zV2g7" // Your server key refer to video for finding your server key
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotifcation(@Body NotificationSender body);
}
