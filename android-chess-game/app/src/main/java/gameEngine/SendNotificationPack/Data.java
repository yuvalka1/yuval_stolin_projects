package gameEngine.SendNotificationPack;

public class Data {
    private String Title;
    private String Message;
    private String game_code;



    public Data() {
    }

    public Data(String title, String message, String game_code) {
        Title = title;
        Message = message;
        this.game_code = game_code;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getGame_code() {
        return game_code;
    }

    public void setGame_code(String game_code) {
        this.game_code = game_code;
    }
}
