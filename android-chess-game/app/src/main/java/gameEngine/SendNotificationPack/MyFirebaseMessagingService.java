package gameEngine.SendNotificationPack;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.res.ResourcesCompat;

import com.example.chess.Globals;
import com.example.chess.MainActivity;
import com.example.chess.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;
import java.util.Random;

import gameEngine.ChessGameActivty;
import gameEngine.GameFinder;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String TAG = "MyFirebaseMsgService";
    public static final String MY_PREFS_NAME = "Tokens";
    private static final String CHANNEL_ID = "1000";
    String title,message, gKey;
    private static String token;

    @Override
    public void onNewToken(@NonNull @NotNull String s) {
        super.onNewToken(s);
        token  = s;
        Log.d(TAG, "onNewToken");
        SharedPreferences pref = getSharedPreferences(MY_PREFS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(String.valueOf(R.string.token), s);
        editor.apply();

    }

    public String getToken(Context context){
        if(token == null) {
            SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Activity.MODE_PRIVATE);
            token = prefs.getString(String.valueOf(R.string.token), null);
        }
        return token;
    }


    public void updateToken(String token){
        Token tokenClass= new Token(token);

        //FirebaseDatabase.getInstance().getReference(getString(R.string.tokenDB)).child(user_id).setValue(token1);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("Tokens").document(Globals.getUser().get_public_key()).
                set(tokenClass).
                addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        Log.d(TAG, "updated token");
                    }else{
                        Log.d(TAG, Objects.requireNonNull(task.getException()).getLocalizedMessage());
                    }
                });
    }


    @Override
    public void onMessageReceived(@NonNull @NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "onMessageReceived");

        title=remoteMessage.getData().get("Title");
        message=remoteMessage.getData().get("Message");
        gKey=remoteMessage.getData().get("game_code");
        pushNotification(title, message, gKey);
    }

    private void pushNotification(String title, String message, String key){
        int icon = R.drawable.chess_invatation_logo;
        String ticket = " this is ticket message";
        long when = System.currentTimeMillis();
        String ticker = "ticker";
        String text="text";
        //phase 2

        Intent intent = new Intent(this, GameFinder.class);
        intent.putExtra("key", key);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "M_CH_ID");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "YOUR_CHANNEL_ID";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
        }
        //phase 3
        Notification notification = builder.setContentIntent(pendingIntent)
                .setSmallIcon(icon).setTicker(ticker).setWhen(when)
                .setAutoCancel(true).setContentTitle(title)
                .setContentText(message).build();
        notificationManager.notify(1, notification);

    }



}