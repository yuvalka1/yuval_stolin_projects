package gameEngine.SendNotificationPack;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chess.Globals;
import com.example.chess.MainActivity;
import com.example.chess.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Random;

import gameEngine.ChessGameActivty;
import gameEngine.FirebasePlayerMatchMaker;
import gameEngine.GameFinder;
import gameEngine.classes.GAME_STATE;
import gameEngine.classes.GameDB;
import gameEngine.classes.UserInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendNotification extends AppCompatActivity {
    private static final String TAG = "SendNotification";
    private APIService apiService;
    String title = "Game Invitation";
    private GameDB mUploadedChallenge;
    private String minutes;
    private ValueEventListener valueEventListener;
    private String game_id;
    DatabaseReference game;
    AVLoadingIndicatorView avi;
    Button cancel;
    private boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_send_notification);
        avi = findViewById(R.id.avi);
        openGame();
        avi.show();
        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);
        cancel = findViewById(R.id.cancelGame);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = false;
                cancelGame();
                finish();
            }
        });
        Intent intent = getIntent();
        String tokenParentId =  intent.getExtras().getString("user_friend_id");
        minutes = intent.getExtras().getString("min", "10");
        if(tokenParentId != null) {
            FirebaseFirestore.getInstance().collection(getString(R.string.tokenDB))
                    .document(tokenParentId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        Token usertoken = Objects.requireNonNull(task.getResult()).toObject(Token.class);
                        Log.d(TAG, "sendNotifications " + usertoken);
                        String message = "You have been invited to play with " + Globals.userInfo.get_display_name();

                        assert usertoken != null;
                        sendNotifications(usertoken.getToken(),
                                title,
                                message);

                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void openGame(){
        DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child(FirebasePlayerMatchMaker.GAMES_RECORD);
        game = firebaseDatabase.push();
        game_id = game.getKey();
        Random random = new Random();
        boolean _starting = random.nextBoolean();
        minutes = "10";
        this.mUploadedChallenge = new GameDB(Globals.getUser().get_public_key(), null, GAME_STATE.ACTIVE, _starting, minutes);
        this.valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                GameDB db = snapshot.getValue(GameDB.class);
                if(db == null){return;}
                Log.d(TAG, "db = " + db.toString());
                if(db.get_player2() != null){
                    //load data
                    game.child(FirebasePlayerMatchMaker.ROOM_ID).removeEventListener(valueEventListener);
                    flag = false;
                    mUploadedChallenge = db;
                    startIntent(game_id, mUploadedChallenge);
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
                Log.w(TAG, "loadPost:onCancelled", error.toException());
            }
        };

        game.child(FirebasePlayerMatchMaker.ROOM_ID).setValue(this.mUploadedChallenge).
                addOnSuccessListener(unused -> Log.d(TAG, "uploaded challenge")).
                addOnFailureListener(e -> Log.d(TAG, e.getLocalizedMessage()));

        //game.child(FirebasePlayerMatchMaker.ROOM_ID).addListenerForSingleValueEvent(valueEventListener);
        FirebaseDatabase.getInstance().getReference().child(FirebasePlayerMatchMaker.GAMES_RECORD).
                child(game_id).child(FirebasePlayerMatchMaker.ROOM_ID).
                addValueEventListener(valueEventListener);

    }

    private void cancelGame(){
        game.removeEventListener(valueEventListener);
        game.runTransaction(new Transaction.Handler() {
            @NonNull
            @NotNull
            @Override
            public Transaction.Result doTransaction(@NonNull @NotNull MutableData currentData) {
                GameDB gameDB = currentData.child(FirebasePlayerMatchMaker.ROOM_ID).getValue(GameDB.class);
                if(gameDB == null){
                    return Transaction.success(currentData);
                }
                else if(gameDB.get_player2() != null){
                    startIntent(game_id, gameDB);
                    finish();
                }
                else{
                    game.removeValue();
                    flag = false;
                }
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(@Nullable @org.jetbrains.annotations.Nullable DatabaseError error,
                                   boolean committed,
                                   @Nullable @org.jetbrains.annotations.Nullable DataSnapshot currentData) {
                finish();

            }
        });

    }
    public void startIntent(String key, GameDB gameDB) {
        if(gameDB.get_player2().equals(Globals.user.get_public_key())){
            getUserData(gameDB.get_player1(), new GameFinder.OnPublicDataCallback() {
                @Override
                public void run(UserInfo userInfo) {
                    Intent intent = new Intent(SendNotification.this, ChessGameActivty.class);
                    intent.putExtra("key", key);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(gameDB);
                    intent.putExtra("game", myJson);
                    intent.putExtra("p1", gson.toJson(userInfo));
                    intent.putExtra("p2", gson.toJson(Globals.getUserInfo()));
                    finish();
                    startActivity(intent);
                }
            });
        }else{
            getUserData(gameDB.get_player2(), new GameFinder.OnPublicDataCallback() {
                @Override
                public void run(UserInfo userInfo) {
                    Intent intent = new Intent(SendNotification.this, ChessGameActivty.class);
                    intent.putExtra("key", key);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(gameDB);
                    intent.putExtra("game", myJson);
                    intent.putExtra("p1", gson.toJson(Globals.getUserInfo()));
                    intent.putExtra("p2", gson.toJson(userInfo));
                    finish();
                    startActivity(intent);
                }
            });
        }
        /*
        getUserData(gameDB.get_player1(), new GameFinder.OnPublicDataCallback() {
            @Override
            public void run(UserInfo userInfo) {
                UserInfo p1 = userInfo;
                getUserData(gameDB.get_player2(), new GameFinder.OnPublicDataCallback() {
                    @Override
                    public void run(UserInfo userInfo) {
                        Intent intent = new Intent(SendNotification.this, ChessGameActivty.class);
                        intent.putExtra("key", key);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(gameDB);
                        intent.putExtra("game", myJson);
                        intent.putExtra("p1", gson.toJson(p1));
                        intent.putExtra("p2", gson.toJson(userInfo));
                        finish();
                        startActivity(intent);
                    }
                });
            }
        });

         */


    }
    private  void getUserData(String public_key, GameFinder.OnPublicDataCallback onPublicDataCallback){
        FirebaseFirestore.getInstance().collection(getString(R.string.userPublicData)).
                document(public_key).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                onPublicDataCallback.run(documentSnapshot.toObject(UserInfo.class));
            }
        });
    }



    /*
    private void UpdateToken(){
        FirebaseUser firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<String> task) {
                if(task.isSuccessful()) {
                    String refreshToken = task.getResult();
                    Token token= new Token(refreshToken);
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    assert firebaseUser != null;
                    db.collection(getString(R.string.tokenDB)).document(firebaseUser.getUid()).update(token.toMap()).
                            addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull @NotNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Log.d(TAG, "updated token");
                                    }else{
                                        Log.d(TAG, Objects.requireNonNull(task.getException()).getLocalizedMessage());
                                    }
                                }
                            });

                }
            }
        });

    }
    */

    public void sendNotifications(String usertoken, String title, String message) {
        Data data = new Data(title, message, game_id);
        NotificationSender sender = new NotificationSender(data, usertoken);
        apiService.sendNotifcation(sender).enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(@NotNull Call<MyResponse> call, @NotNull Response<MyResponse> response) {
                Log.d(TAG, "onResponse, code: " + String.valueOf(response.code()));
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().success != 1) {
                        Toast.makeText(SendNotification.this, "Failed ", Toast.LENGTH_LONG).show();
                    }
                    boolean handler = new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(flag) {
                                cancelGame();
                            }
                        }
                    }, 5 * 60* 1000);

                }
            }

            @Override
            public void onFailure(@NotNull Call<MyResponse> call, @NotNull Throwable t) {
                Log.d(TAG, t.getLocalizedMessage());
            }
        });
    }
}