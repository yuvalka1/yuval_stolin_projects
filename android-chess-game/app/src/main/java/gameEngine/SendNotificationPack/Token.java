package gameEngine.SendNotificationPack;

import java.util.HashMap;

public class Token {
    private String token;

    public Token(String token) {
        this.token = token;
    }

    public Token() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String , Object> map = new HashMap<>();
        map.put("token", token);
        return map;
    }
}
