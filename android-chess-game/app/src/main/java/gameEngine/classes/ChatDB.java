package gameEngine.classes;

import java.util.ArrayList;

public class ChatDB {
    ArrayList<Message> messageArrayList;
    public ChatDB(){
        messageArrayList = new ArrayList<>();
    }

    public void setMessageArrayList(ArrayList<Message> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }

    public ArrayList<Message> getMessageArrayList() {
        return messageArrayList;
    }

    public void add(Message msg){
        if(messageArrayList == null){
            messageArrayList = new ArrayList<>();
        }
        messageArrayList.add(msg);
    }

    public Message lastMessage(){
        if(messageArrayList == null || messageArrayList.isEmpty()){return null;}
        return messageArrayList.get(messageArrayList.size() - 1);
    }
}
