package gameEngine.classes;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import chesslogic.Move;


public class GameDB  {

    String _player1;
    String _player2;
    ArrayList<Move> _white_moves;
    ArrayList<Move> _black_moves;
    GAME_STATE _state;
    String _minutes;
    boolean _starting;// true - player 1 is white else back
    int _time;


    boolean _turn; // true player1, false player2
    public GameDB(){}
    public GameDB( String player1, String player2, GAME_STATE state, boolean starting, String minutes){
        _turn = true;
        _player2 = player2;
        _player1 = player1;
        set_state(state);
        set_starting(starting);
        set_minutes(minutes);
        _white_moves = new ArrayList<>();
        _black_moves = new ArrayList<>();
        _time = 0;

    }

    public GameDB(GameDB db) {
        _turn = db._turn;
        _player2 = db._player2;
        _player1 = db._player1;
        set_state(db._state);
        set_starting(db._starting);
        set_minutes(db._minutes);
        _white_moves = db._white_moves;
        _black_moves = db._black_moves;
        _time = db._time;
    }


    @Exclude
    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("_player1", _player1);
        result.put("_player2", _player2);
        result.put("_white_moves", _white_moves);
        result.put("_black_moves", _black_moves);
        result.put("_state", _state);
        result.put("_minutes", _minutes);
        result.put("_starting", _starting);
        result.put("_turn", _turn);
        result.put("_time", _time);

        return result;
    }


    public  void addMove(boolean color, Move move){
        if(color){
            if(_white_moves == null){_white_moves = new ArrayList<>();}
            _white_moves.add(move);
        }
        else{
            if(_black_moves == null){_black_moves = new ArrayList<>();}
            _black_moves.add(move);
        }
    }

    public Move lastMove(){
        if(_white_moves == null || _white_moves.isEmpty()){return null;}
        if(_black_moves == null){return _white_moves.get(0);}
        int white_size = _white_moves.size();
        int black_size = _black_moves.size();
        return white_size > black_size ? new Move(_white_moves.get(white_size - 1))
                : new Move(_black_moves.get(black_size - 1));
    }

    public int get_time() {
        return _time;
    }

    public String get_minutes() {
        return _minutes;
    }


    public String get_player1() {
        return _player1;
    }

    public String get_player2() {
        return _player2;
    }

    public ArrayList<Move> get_black_moves() {
        return _black_moves;
    }

    public ArrayList<Move> get_white_moves() {
        return _white_moves;
    }

    public GAME_STATE get_state() {
        return _state;
    }

    public void set_time(int _time) {
        this._time = _time;
    }

    public void set_minutes(String _minutes) {
        this._minutes = _minutes;
    }


    public void set_black_moves(ArrayList<Move> _black_moves) {
        this._black_moves = _black_moves;
    }

    public void set_player1(String _player1) {

        this._player1 = _player1;
    }

    public void set_player2(String _player2) {
        this._player2 = _player2;
    }

    public void set_state(GAME_STATE _state) {
        this._state = _state;
    }

    public void set_white_moves(ArrayList<Move> _white_moves) {
        this._white_moves = _white_moves;
    }

    public void set_starting(boolean _starting) {
        this._starting = _starting;
    }

    public boolean is_starting() {
        return _starting;
    }

    public boolean is_turn() {
        return _turn;
    }

    public void set_turn(boolean _turn) {
        this._turn = _turn;
    }

    @Override
    public String toString() {
        return "GameDB{" +
                "_player1=" + _player1 +
                ", _player2=" + _player2 +
                ", _white_moves=" + _white_moves +
                ", _black_moves=" + _black_moves +
                ", _state=" + _state +
                ", _minutes='" + _minutes + '\'' +
                ", _starting=" + _starting +
                ", _turn=" + _turn +
                '}';
    }
}
