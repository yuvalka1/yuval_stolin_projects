package gameEngine.classes;

import com.example.chess.ActiveUser;
import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Objects;

public class GameInfo {
    String  opponent_id;
    int gameResult; // 0- won, -1 - lost, 1 -tie
    String gameType;

    String key;

    public GameInfo(){}
    public GameInfo(String opponent, int gameResult, String gameType, String key) {
        this.opponent_id = opponent;
        this.gameResult = gameResult;
        this.gameType = gameType;
        this.key = key;
    }


    public String getOpponent_id() {
        return opponent_id;
    }

    public void setOpponent_id(String opponent_id) {
        this.opponent_id = opponent_id;
    }

    public int getGameResult() {
        return gameResult;
    }

    public void setGameResult(int gameResult) {
        this.gameResult = gameResult;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "GameInfo{" +
                "opponent=" + opponent_id +
                ", gameResult=" + gameResult +
                ", gameType='" + gameType + '\'' +
                '}';
    }

    @Exclude
    private HashMap<String, Object> toMap(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("opponent_id", opponent_id);
        map.put("gameResult", gameResult);
        map.put("gameType", gameType);
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameInfo gameInfo = (GameInfo) o;
        return gameResult == gameInfo.gameResult &&
                opponent_id.equals(gameInfo.opponent_id) &&
                gameType.equals(gameInfo.gameType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(opponent_id, gameResult, gameType);
    }
}
