package gameEngine.classes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.chess.Globals;
import com.example.chess.R;
import com.example.chess.UserFriends;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class GameInfoAdapter extends ArrayAdapter<GameInfo> {

    private static final String TAG = "MessageAdapter";
    List<GameInfo> gameList;
    Map<String, UserInfo > opponents;
    Context mContext;
    public GameInfoAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<GameInfo> objects) {
        super(context, resource, textViewResourceId, objects);
        mContext = context;
        gameList = objects;
        opponents = new HashMap<>();
        for(GameInfo info: gameList){
            String opponent_id = info.opponent_id;
            if(!opponents.containsKey(opponent_id)){
                FirebaseFirestore.getInstance().collection(mContext.getString(R.string.userPublicData)).document(opponent_id)
                        .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        UserInfo userInfo = Objects.requireNonNull(documentSnapshot.toObject(UserInfo.class));
                        opponents.put(opponent_id, userInfo);
                    }
                });
            }
        }
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        GameInfo gameInfo = gameList.get(position);
        LayoutInflater layoutInflater = ((Activity)mContext).getLayoutInflater();
        View view  = layoutInflater.inflate(R.layout.gameinfo_layout,parent,false);


        ImageView gameType = view.findViewById(R.id.gameType);
        ImageView gameStatus = view.findViewById(R.id.gameStatus);
        TextView displayName = view.findViewById(R.id.displayName);

        if(opponents.containsKey(gameInfo.opponent_id)) {
            displayName.setText(Objects.requireNonNull(opponents.get(gameInfo.opponent_id))._display_name);

        }else{
            displayName.setText("user");
        }
        String choice = gameInfo.gameType;
        if("Rapid".equals(choice)){
            gameType.setImageResource(R.drawable.rapid);
        }
        else if("Blitz".equals(choice)){
            gameType.setImageResource(R.drawable.blitz);
        }
        else{
            gameType.setImageResource(R.drawable.bullet);
        }

        int result = gameInfo.gameResult;
        if(result == 0){
            gameStatus.setImageResource(R.drawable.ic_baseline_add_24);
        }
        else if(result == -1){
            gameStatus.setImageResource(R.drawable.ic_baseline_close_24);
        }
        else{
            gameStatus.setImageResource(R.drawable.equal);
        }


        return view;

    }
}
