package gameEngine.classes;

import java.util.Objects;

public class Message {
    String message;
    String messageUser;
    public Message(){}
    public Message(String message,  String messageUser) {
        this.message = message;

        this.messageUser = messageUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }

    @Override
    public String toString() {
        return "Messege{" +
                "message='" + message + '\'' +
                ", messageUser='" + messageUser + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return message.equals(message1.message) &&
                messageUser.equals(message1.messageUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, messageUser);
    }
}
