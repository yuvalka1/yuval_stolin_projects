package gameEngine.classes;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.example.chess.Globals;
import com.example.chess.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;
import java.util.Objects;


public class MessageAdapter  extends ArrayAdapter<Message>{
    private static final String TAG = "MessageAdapter";
    List<Message> messageList;
    Context context;
    public MessageAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<Message> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context=context;
        this.messageList=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Message messege = messageList.get(position);
        LayoutInflater layoutInflater = ((Activity)context).getLayoutInflater();
        View view;
        TextView textView;
        CircularImageView circularImageView;
        Log.d(TAG, messege.toString());
        if(messege.getMessageUser().equals(Globals.getUserInfo().get_display_name())){
           view  = layoutInflater.inflate(R.layout.msg,parent,false);
           textView = view.findViewById(R.id.message_body);
        }
        else{
            view  = layoutInflater.inflate(R.layout.their_message,parent,false);
            circularImageView = view.findViewById(R.id.avatar);
            circularImageView.setImageResource(R.drawable.ic_baseline_person_24);
            textView = view.findViewById(R.id.message);
            TextView name = view.findViewById(R.id.name);
            name.setText(messege.getMessageUser());
        }
        textView.setText(messege.getMessage());


        return view;
    }


}
