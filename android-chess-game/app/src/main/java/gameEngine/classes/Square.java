package gameEngine.classes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;

public class Square {
    float _x,_y,_w,_h;
    int _color;
    Paint _p;
    Context _context;
    int _image_id;

    public Square(float x, float y , float w, float h, int color, Context context){
        _w = w;
        _color = color;
        _h = h;
        _x  = x;
        _y = y;
        _p = new Paint();
        _context = context;
        _image_id = 0;
        _p.setColor(ContextCompat.getColor(context, color));

    }

    public void set_p(Paint _p) {
        this._p = _p;
    }

    public Square(float x, float y , float w, float h, int color, Context context, int image_id){
        _w = w;
        _color = color;
        _h = h;
        _x  = x;
        _y = y;
        _p = new Paint();
        _context = context;
        _image_id = image_id;
        _p.setColor(ContextCompat.getColor(context, color));

    }
    public Square(Square square){
        this._x = square._x;
        _y = square._y;
        _w = square._w;
        _h = square._h;
        _color = square._color;
        _p = square._p;
        _image_id = square._image_id;
    }




    public void draw(Canvas canvas){
        canvas.drawRect(_x,_y,_w+_x,_y+_h, _p);
        if (_image_id!=0){
            Drawable drawable = ContextCompat.getDrawable(_context, _image_id);
            assert drawable != null;
            drawable.setBounds((int)_x,(int)_y,(int)(_x+_w),(int)(_y+_h));
            drawable.draw(canvas);
        }
    }

    public boolean isXYInSquare(float x0, float y0){
        return _image_id != 0 && x0 > _x && x0 < _x + _w && y0 > _y && y0 < y0 +_h;
    }


}
