package gameEngine.classes;

import android.os.Handler;

public class ThreadClock extends  Thread{
    int num;
    boolean flag;
    public interface OnFinished{
        public void run();
    }
    boolean isRun=true;
    OnFinished onFinished;
    public void setRun(boolean run) {
        isRun = run;
    }

    Handler handler;
    public ThreadClock(Handler handler,int num, OnFinished finished)
    {
        flag = true;
        this.handler=handler;
        this.num=num;
        onFinished = finished;
    }
    @Override
    public void run() {
        super.run();
        while(true)
        {
            if(isRun&&num>0)
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                android.os.Message msg = new android.os.Message();
                msg.arg1 = num;
                num--;
                handler.sendMessage(msg);

            }
            if(num == 0 ){
                break;
            }

        }
        if (onFinished != null) {
            onFinished.run();
        }

    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void addTime(int seconds){num += seconds;}

    public void setFlag(boolean flag) {
        this.flag = flag;
    }


}
