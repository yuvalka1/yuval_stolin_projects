package gameEngine.classes;

import com.example.chess.ActiveUser;

import java.util.HashMap;

public class UserInfo {
    String _display_name;
    String _country;
    String _image_url;

    public UserInfo(){

    }

    public UserInfo(String _display_name, String _country, String _image_url) {
        this._display_name = _display_name;
        this._country = _country;
        this._image_url = _image_url;
    }

    public String get_country() {
        return _country;
    }

    public String get_display_name() {
        return _display_name;
    }

    public void set_display_name(String _display_name) {
        this._display_name = _display_name;
    }

    public void set_country(String _country) {
        this._country = _country;
    }

    public String get_image_url() {
        return _image_url;
    }

    public void set_image_url(String _image_url) {
        this._image_url = _image_url;
    }

    public HashMap<String,Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("_display_name", _display_name);
        result.put("_country", _country);
        result.put("_image_url", _image_url);
        return result;
    }

    public UserInfo(UserInfo user){
        this._country = user.get_country();
        this._image_url = user.get_image_url();
        this._display_name = user.get_display_name();
    }


    @Override
    public String toString() {
        return "UserInfo{" +
                "_display_name='" + _display_name + '\'' +
                ", _country='" + _country + '\'' +
                ", _image_url='" + _image_url + '\'' +
                '}';
    }
}
