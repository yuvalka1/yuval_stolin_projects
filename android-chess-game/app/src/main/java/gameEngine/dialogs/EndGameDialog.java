package gameEngine.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;


import com.example.chess.MainMenu;
import com.example.chess.R;
import com.google.gson.Gson;

import gameEngine.ChessGameActivty;
import gameEngine.GameFinder;
import gameEngine.ReviewGame;
import gameEngine.classes.GameDB;
import gameEngine.classes.UserInfo;

public class EndGameDialog extends Dialog {
    private static final String TAG = "EndGameDialog";
    private final Context mContext;
    private final GameDB db;

    ImageView iv1, iv2;
    Button newGame, gameReport;
    Toolbar toolbar;
    Drawable img1, img2;
    UserInfo player1, player2;

    public EndGameDialog(@NonNull Context context, GameDB gameDB, Drawable image1, Drawable image2,
                         UserInfo p1, UserInfo p2) {
        super(context);
        mContext = context;
        db = gameDB;
        img1 = image1;
        img2 = image2;
        player1 = p1;
        player2 = p2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.end_game_dialog);


        iv1 = findViewById(R.id.iv1);
        iv2 = findViewById(R.id.iv2);
        iv1.setImageDrawable(img1);
        iv2.setImageDrawable(img2);

        newGame = findViewById(R.id.newGame);
        gameReport = findViewById(R.id.gameReport);


        toolbar = findViewById(R.id.end_game_toolbar);
        Log.d(TAG, db.toString());
        toolbar.setTitle(db.get_state().toString());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });




        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent((Activity)mContext, MainMenu.class);
                mContext.startActivity(intent);
                dismiss();
            }
        });
        gameReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent((Activity)mContext, ReviewGame.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(db);
                intent.putExtra("game", myJson);
                mContext.startActivity(intent);
                dismiss();
            }
        });
    }


}
