package gameEngine.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.example.chess.R;

public class GameOptionsDialog extends Dialog implements View.OnClickListener{


    public interface OnSelectedButton{
        public void run();// true abort, false draw
    }

    OnSelectedButton onSelectedButton;
    Context mContext;

    Button abort;
    public GameOptionsDialog(@NonNull Context context, OnSelectedButton selectedButton) {
        super(context);
        onSelectedButton = selectedButton;
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.game_options_dialog);
        abort = findViewById(R.id.abort_game);
        abort.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == abort){
            validateAbortClick();
        }
    }

    private void validateAbortClick(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setTitle("Abort");
        builder1.setMessage("Are you sure?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        dismiss();
                        onSelectedButton.run();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
