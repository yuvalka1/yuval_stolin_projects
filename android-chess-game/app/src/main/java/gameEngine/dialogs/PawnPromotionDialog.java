package gameEngine.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import androidx.annotation.NonNull;

import com.example.chess.R;

import org.jetbrains.annotations.NotNull;

public class PawnPromotionDialog extends Dialog implements View.OnClickListener {
    private Context _context;
    private Boolean _color;

    private ImageButton queen, rook, knight, bishop;



    public interface OnSelectedPiece{
        public void run(char type);
    }

    OnSelectedPiece _onSelectedPiece;

    public PawnPromotionDialog(@NonNull @NotNull Context context, Boolean _color, OnSelectedPiece onSelectedPiece) {
        super(context);
        this._context = context;
        this._color = _color;
        _onSelectedPiece = onSelectedPiece;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.promote_piece);
        queen = findViewById(R.id.queen);
        rook = findViewById(R.id.rook);
        knight = findViewById(R.id.knight);
        bishop = findViewById(R.id.bishop);
        if (_color){
            queen.setImageDrawable(_context.getResources().getDrawable(R.drawable.wqueen));
            rook.setImageDrawable(_context.getResources().getDrawable(R.drawable.wrook));
            knight.setImageDrawable(_context.getResources().getDrawable(R.drawable.wknight));
            bishop.setImageDrawable(_context.getResources().getDrawable(R.drawable.wbishop));
        }
        else{
            queen.setImageDrawable(_context.getResources().getDrawable(R.drawable.bqueen));
            rook.setImageDrawable(_context.getResources().getDrawable(R.drawable.brook));
            knight.setImageDrawable(_context.getResources().getDrawable(R.drawable.bknight));
            bishop.setImageDrawable(_context.getResources().getDrawable(R.drawable.bbishop));
        }
        queen.setOnClickListener(this);
        rook.setOnClickListener(this);
        knight.setOnClickListener(this);
        bishop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        char type = 0;
        switch (v.getId()){
            case R.id.queen:
                type = 'q';
                break;
            case R.id.rook:
                type = 'r';
                break;
            case R.id.knight:
                type = 'n';
                break;
            case R.id.bishop:
                type = 'b';
                break;
            default:
                break;
        }
        if(type != 0){
            _onSelectedPiece.run(type);
            dismiss();
        }
    }
}
