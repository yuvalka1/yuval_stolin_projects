import cv2
import albumentations as A
import numpy as np
import matplotlib.pyplot as plt
from config import *

def show_pic(image, landmarks):
    clone = image.copy()
    for (y, x) in landmarks:
        clone[x, y] = (255, 0, 0)
    plt.imshow((clone * 255).astype(np.uint8))
    plt.show()


def show_landmarks(image, landmarks, axis=None):
    i = 0
    clone = image.copy()

    while i < len(landmarks) - 1:
        x = landmarks[i + 1] if landmarks[i + 1] < 96 else 95
        y = landmarks[i] if landmarks[i] < 96 else 95
        clone[x, y] = (255, 0, 0)
        i += 2

    if axis is None:
        plt.imshow((clone * 255).astype(np.uint8))
        plt.show()
    else:
        axis.imshow((clone * 255).astype(np.uint8))


def append(image, points, au_images, au_landmarks):
    if len(points) == 8:
        au_images.append(image)
        au_landmarks.append(points)


def left_right_flip(image, keypoints):
    SIZE = 150
    flipped_image = np.flip(image, axis=1)  # Flip column-wise (axis=2)
    flipped_keypoints = []
    for (y, x) in keypoints:
        y = SIZE - y if SIZE - y < 150 else 149
        flipped_keypoints.append((y, x))
    return [flipped_image, flipped_keypoints]


def blur(image, keypoints):
    return [cv2.blur(image, (5, 5)), keypoints]


def noise(img, keypoints):
    return [cv2.medianBlur(img, 5), keypoints]


def augment_images(images, keypoints):
    au_images, au_landmarks = [], []

    for index, image in enumerate(images):
        landmark = keypoints[index]
        shiftScaleRight = A.Compose(
            [A.ShiftScaleRotate(p=0.5, always_apply=True, rotate_limit=30, shift_limit=0.05)],
            keypoint_params=A.KeypointParams(format='xy')
        )

        resultBlur = blur(image, landmark)
        resultFlipBlur = left_right_flip(resultBlur[0], resultBlur[1])
        resultRotateBlur = shiftScaleRight(image=resultBlur[0], keypoints=resultBlur[1])
        resultFlipRotateBlur = shiftScaleRight(image=resultFlipBlur[0], keypoints=resultFlipBlur[1])
        resultNoiseBlur = noise(resultBlur[0], resultBlur[1])

        resultFlip = left_right_flip(image, landmark)
        resultRotate = shiftScaleRight(image=image, keypoints=landmark)
        resultFlipRotate = shiftScaleRight(image=resultFlip[0], keypoints=resultFlip[1])
        resultNoise = noise(image, landmark)

        au_images.extend([image, resultBlur[0], resultFlipBlur[0], resultFlip[0], resultNoiseBlur[0], resultNoise[0]])
        au_landmarks.extend([landmark, resultBlur[1], resultFlipBlur[1], resultFlip[1],
                             resultNoiseBlur[1], resultNoise[1]])

        append(resultRotateBlur['image'], resultRotateBlur['keypoints'], au_images, au_landmarks)
        append(resultFlipRotateBlur['image'], resultFlipRotateBlur['keypoints'], au_images, au_landmarks)
        append(resultRotate['image'], resultRotate['keypoints'], au_images, au_landmarks)
        append(resultFlipRotate['image'], resultFlipRotate['keypoints'], au_images, au_landmarks)

    return au_images, au_landmarks


def count(arr, element):
    return sum(1 for e in arr if np.array_equal(e, element) or (e == element).all())


def rgb2gray(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    gray = np.broadcast_to(gray[..., np.newaxis], (gray.shape[0], gray.shape[1], 3))
    gray.setflags(write=1)
    return gray


def convertImg(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = r + g + b
    gray = np.broadcast_to(gray[..., np.newaxis], (gray.shape[0], gray.shape[1], 1))
    gray.setflags(write=1)
    return gray


def load_dataset(file_paths):
    print("loading data...")
    images = []
    landmarks = []
    length = 0
    for file_path in file_paths:
        print("reading file ", file_path)
        file = open(file_path, "r")
        data = file.read().split("\n")[:-1]
        arr_size = len(data)
        length += arr_size

    for file_path in file_paths:
        images = np.empty((length, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL), dtype='float')
        landmarks = np.empty((length, MARK_SIZE*2), dtype='float')
        file = open(file_path, "r")
        data = file.read().split("\n")[:-1]
        for i, line in enumerate(data):
            protocol = line.split("#")
            image_path, face_box, key_points = protocol
            img = cv2.imread(image_path)
            x, y, w, h = [int(i) for i in face_box.split(";")]
            img = img[y:y + h, x:x + w]
            img = cv2.resize(img, dsize=(IMG_HEIGHT, IMG_WIDTH))
            img = rgb2gray(img) 
            img = img.astype('float32')
            points = key_points.split(",")

            points = [(float(points[i]) / IMG_HEIGHT) for i in range(0, len(points))]
            images[i] = img
            landmarks[i] = points

    print(str(len(images)), " images")
    print(images.shape, " ", landmarks.shape)
    length = len(images)

    train_images, train_points = images[int(0.2 * length):], landmarks[int(0.2 * length):]
    test_images, test_points = images[:int(0.1 * length)], landmarks[:int(0.1 * length)]
    eval_images, eval_points = images[int(0.1 * length): int(0.2 * length)], landmarks[
                                                                             int(0.1 * length): int(0.2 * length)]
    print("finished :)")
    return train_images, train_points, test_images, test_points, eval_images, eval_points


