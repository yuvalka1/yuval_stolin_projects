import keras
from keras.layers import Flatten, Dense, Conv2D, BatchNormalization, Activation, Dropout, MaxPooling2D, Input
from config import *


def cnnModel():
    img_input = Input(shape=(IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL))

    # Block 1 #
    x = Conv2D(32, (3, 3), strides=(1, 1), name='S1_conv1')(img_input)
    x = BatchNormalization()(x)
    x = Activation('relu', name='S1_relu_conv1')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='S1_pool1')(x)

    # Block 2 #
    x = Conv2D(64, (3, 3), strides=(1, 1), name='S1_conv2')(x)
    x = BatchNormalization()(x)
    x = Activation('relu', name='S1_relu_conv2')(x)
    x = Conv2D(64, (3, 3), strides=(1, 1), name='S1_conv3')(x)
    x = BatchNormalization()(x)
    x = Activation('relu', name='S1_relu_conv3')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='S1_pool2')(x)

    # Block 3 #
    x = Conv2D(64, (3, 3), strides=(1, 1), name='S1_conv4')(x)
    x = BatchNormalization()(x)
    x = Activation('relu', name='S1_relu_conv4')(x)
    x = Conv2D(64, (3, 3), strides=(1, 1), name='S1_conv5')(x)
    x = BatchNormalization()(x)
    x = Activation('relu', name='S1_relu_conv5')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='S1_pool3')(x)

    # Block 4 #
    x = Conv2D(256, (3, 3), strides=(1, 1), name='S1_conv8')(x)
    x = BatchNormalization()(x)
    x = Activation('relu', name='S1_relu_conv8')(x)
    x = Dropout(0.2)(x)

    # Block 5 #
    x = Flatten(name='S1_flatten')(x)
    x = Dense(2048, activation='relu', name='S1_fc1')(x)
    x = Dense(MARK_SIZE*2, activation=None, name='output')(x)
    model = keras.Model([img_input], x, name='facial_landmark_model')

    return model
