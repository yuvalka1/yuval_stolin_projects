import argparse
import matplotlib.pyplot as plt
import numpy as np
import os
import cv2
import dlib
from imutils import face_utils
import face_recognition
import tensorflow as tf
from config import *


class FaceDetector:
    """
    Detect human face from image
    """
    def __init__(self):
        self.proto_txt = "deploy.prototxt"
        self.caffe_model = "res10_300x300_ssd_iter_140000.caffemodel"
        self.net = cv2.dnn.readNetFromCaffe(self.proto_txt, self.caffe_model)
        self.detector = dlib.get_frontal_face_detector()

    def detect_faces(self, img, min_confidence=0.5):
        list_faces = []
        image = img.copy()
        (h, w) = image.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(image, (180, 180)), 1.0,
                                     (180, 180), (104.0, 177.0, 123.0))
        self.net.setInput(blob)
        detections = self.net.forward()

        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the
            # prediction
            confidence = detections[0, 0, i, 2]
            # filter out weak detections by ensuring the `confidence` is
            # greater than the minimum confidence
            if confidence > min_confidence:
                # compute the (x, y)-coordinates of the bounding box for the
                # object
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                list_faces.append([startX, startY, endX, endY])
        if len(list_faces) == 1:
            (x, y, w, h) = list_faces[0]
            if x > 0 and y > 0 and h > 0 and x > 0:
                list_faces = [dlib.rectangle(x, y, w, h)]
                return list_faces

        list_faces = []
        rect = self.detector(image, 1)
        if rect is None or len(rect) == 0 or len(rect) > 1:
            face = face_recognition.face_locations(image)
            if face is None or len(face) == 0 or len(face) > 1:
                return None
            else:
                face = face[0]
                rect = [dlib.rectangle(face[3], face[0], face[1], face[2])]

        list_faces.append(rect[0])
        return list_faces


class Rescale(object):
    """Rescale the image in a sample to a given SIZE.

    Args:
        output_size list of keypoints [y1,x1.y1.x1,.....] of rescaled image
    """

    @staticmethod
    def resize(image, key_pts, output_size):
        h, w = image.shape[:2]
        new_h, new_w = output_size, output_size
        i = 0
        while i < len(key_pts) - 1:
            new_x = int(key_pts[i] * new_w / w)
            new_y = int(key_pts[i + 1] * new_h / h)
            key_pts[i] = new_x if new_x < IMG_HEIGHT else IMG_HEIGHT - 1
            key_pts[i + 1] = new_y if new_y < IMG_HEIGHT else IMG_HEIGHT - 1
            i += 2

        return key_pts


def show_landmarks(image, landmarks):
    i = 0
    clone = image.copy()

    for num in range(0, len(landmarks), 2):
        clone[landmarks[num+1], landmarks[num]] = (255, 0, 0)
        i += 2
    plt.imshow(clone)
    plt.show()


def get_all_files(path):
    data_files = []
    for root, _, f in os.walk(path):
        for file in f:
            if ".jpg" in file or ".png" in file:
                data_files.append(root.replace("\\", "/") + "/" + file)
    return data_files


def get_landmarks(image, detector, predictor, output):
    landmarks = []
    facial_landmarks = [('right_eye', (17, 21, 36, 42)), ('left_eye', (22, 26, 42, 48))]
    faces = detector.detect_faces(image)
    if faces is None or len(faces) == 0 or len(faces) > 1:
        return None, None, None
    rect = faces[0]
    (x, y, w, h) = face_utils.rect_to_bb(rect)

    face = image[y:y + h, x:x + w, :]
    if face.shape[0] <= 0 or face.shape[1] <= 0 or face.size == 0:
        return None, None, None

    shape = predictor(image, rect)
    shape = face_utils.shape_to_np(shape)
    if output == 16:
        for (name, (i, j, k, p)) in facial_landmarks:
            arr = np.append(shape[i], shape[int((i + j) / 2)], axis=0)
            arr = np.append(arr, shape[j], axis=0)
            eye = np.array(shape[k:p])
            x_sum = 0
            y_sum = 0
            for (y_pos, x_pos) in eye:
                x_sum += x_pos
                y_sum += y_pos

            arr = np.append(arr, [int(y_sum / 6), int(x_sum / 6)], axis=0)
            landmarks.extend(arr)
    else:
        for (py, px) in shape:
            landmarks.extend([py, px])

    i = 0

    while i < len(landmarks) - 1:
        landmarks[i] = abs(x - landmarks[i])
        landmarks[i + 1] = abs(y - landmarks[i + 1])
        i += 2

    return [x, y, w, h], landmarks, face


def create_txt_file(dest_file, data, output):
    log = open("data/datasets/log.txt", 'a')
    i = 0

    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    skipped_images = 0
    face_detector = FaceDetector()
    for image_path in data:
        image = cv2.imread(image_path)
        box, landmarks, image = get_landmarks(image,  face_detector, predictor, output)
        if landmarks is None:
            skipped_images += 1
            print("skipped image ", image_path)
            continue
        landmarks = Rescale.resize(image, landmarks, IMG_HEIGHT)
        string = image_path + "#" + ';'.join(list(map(str, box))) + "#" + ",".join(
            list(map(str, landmarks))) + "\n"
        i += 1
        print("processing image" + str(i) + " : " + image_path)
        dest_file.write(string)
    log.write(str(dest_file.name + " has " + str(i) + " images and skipped - " + str(skipped_images) + "\n"))


def prepare_dataset(args):
    paths = args.paths
    exports = args.save_txt
    dataset = []
    for index, path in enumerate(paths):
        arr = []
        paths[index] = path.replace("\\", "/")
        files = get_all_files(path)
        for image_path in files:
            arr.append(image_path)
        dataset.append(arr)

    for i, data in enumerate(dataset):
        name = ""
        try:
            name = exports[i]
        except Exception:
            if paths[i][-1] == "/":
                paths[i] = paths[i][:-1]
            name = paths[i].split("/")[-1]
        file = open("data/datasets/" + name + ".txt", "w")
        create_txt_file(file, data, args.marks)


def main():
    parser = argparse.ArgumentParser(description='enter paths to project datasets')
    parser.add_argument("--paths", nargs='+', required=True, help="list of path to datasets separated bu space\n --paths dir1 dir2")
    parser.add_argument("--save_txt", nargs='+',  required=False, help="name to save the annotations file")
    parser.add_argument("--marks", required=True, type=int, help="number of landmarks (16 or 136")
    args = parser.parse_args()
    paths = ["../../data/keypoints/helen.txt"]

    for path in args.paths:
        assert os.path.isdir(path)
    assert args.marks == 136 or args.marks == 16
    prepare_dataset(args)


if __name__ == '__main__':
    main()

