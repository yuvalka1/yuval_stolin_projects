import tensorflow as tf
import sys
import os


def get_all_files():
    data_files = []
    for root, _, f in os.walk(path):
        for file in f:
            if ".h5" in file:
                data_files.append(root.replace("\\", "/") + "/" + file)
    return data_files


path = "./data/output/model"#sys.argv[1]
files = get_all_files()
for file in files:
    name = file.split("/")[-1]
    model = tf.keras.models.load_model(file, compile=False)
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    tflite_model = converter.convert()
    open(file.replace(name, name.split(".")[0] + ".tflite"), "wb").write(tflite_model)
