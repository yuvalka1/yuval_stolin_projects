import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['KMP_WARNINGS'] = '0'
os.environ['KMP_BLOCKTIME'] = '0'
import argparse
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.python.keras.models import load_model
from facial_landmarks_cnn import cnnModel
from Augmentate_images import load_dataset
from keras.callbacks import CSVLogger, EarlyStopping
import keras
import keras.backend as K
from config import *


def show_landmarks(image, landmarks, axis=None):
    i = 0
    clone = image.copy()

    while i < len(landmarks) - 1:
        x = landmarks[i + 1] * IMG_HEIGHT
        x = x if x < IMG_HEIGHT else IMG_HEIGHT - 1
        y = landmarks[i] * IMG_HEIGHT
        y = y if y < IMG_HEIGHT else IMG_HEIGHT - 1
        clone[int(x), int(y)] = (255, 0, 0)
        i += 2

    if axis is None:
        plt.imshow(clone)
        plt.show()
    else:
        axis.imshow(clone)


def show_history(history, epochs, save_fig):
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training acc')
    plt.plot(epochs_range, val_acc, label='Validation acc')
    plt.legend(loc='lower right')
    plt.title('Training and Validation accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')

    arr = save_fig.split("/")
    name = arr[-1].split(".")[0]
    plt.savefig("/".join(arr[:len(arr) - 1]) + "/" + name + ".png")


def smoothL1(y_true, y_pred):
    HUBER_DELTA = 0.5
    x = K.abs(y_true - y_pred)
    x = K.switch(x < HUBER_DELTA, 0.5 * x ** 2, HUBER_DELTA * (x - 0.5 * HUBER_DELTA))
    return K.sum(x)


def getCallbacks():
    callbacks = [
        # Interrupt training if `val_loss` stops improving for over 2 epochs
        keras.callbacks.EarlyStopping(patience=10, monitor='val_loss'),
        keras.callbacks.TensorBoard(log_dir="data/output/pose_model"),
        keras.callbacks.ReduceLROnPlateau(monitor='val_loss',
                                          factor=0.1,
                                          patience=3,
                                          verbose=1),
        CSVLogger('data/output/pose_model/log.csv', append=False, separator=',')
    ]
    return callbacks


def main(arguments):
    NUM_EPOCHS = arguments.epochs

    train_images, train_keypoints, test_images, test_keypoints, eval_images, eval_keypoints = load_dataset(
        arguments.paths)

    print("using " + str(len(train_images)) + " images for training")
    print("using " + str(len(test_images)) + " images for evaluating")
    print("using " + str(len(test_images)) + " images for testing")

    if not os.path.isfile(arguments.save_model):
        print('building model')
        model = cnnModel()
        model.compile(optimizer=keras.optimizers.Adam(lr=0.001),
                      loss=smoothL1,
                      metrics=['accuracy'])
        print(model.summary())
        history = model.fit(train_images, train_keypoints, epochs=NUM_EPOCHS, callbacks=getCallbacks(),
                            validation_data=(eval_images, eval_keypoints), shuffle=True, verbose=1)
        model.save(arguments.save_model)
        show_history(history, NUM_EPOCHS, arguments.save_model)

    model = keras.models.load_model(arguments.save_model, compile=False)
    print('loaded model')
    test_predictions = model.predict(test_images)
    fig = plt.figure(figsize=(20, 16))
    for i in range(20):
        axis = fig.add_subplot(4, 5, i + 1, xticks=[], yticks=[])
        show_landmarks(test_images[i], test_predictions[i], axis)
    plt.savefig(arguments.save_model[:len(arguments.save_model) - 3] + "_result_sample.png")

    test_loss, test_acc = model.evaluate(test_images, test_keypoints, verbose=2)
    print('\nTest accuracy:', test_acc)
    print("Test loss", test_loss)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='enter paths to project datasets')

    parser.add_argument("--paths", nargs='+', required=True,
                        help="list of path to datasets separated bu space\n --paths txt1 txt2")
    parser.add_argument("--epochs", required=True, type=int, help="number of epochs for training the model")
    parser.add_argument("--save_model", required=True, type=str, help="path to save the model")

    args = parser.parse_args()
    print("starting code")
    main(args)
