import math
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from scipy.spatial import distance as dist
import numpy as np
from .mark_detector import MarkDetector
from .config import *
from modules import *
import modules
import time
from alarm import play_alarm

ANGLE = 50
model_points = np.array([
    (0.0, 0.0, 0.0),  # Nose tip
    (0.0, -330.0, -65.0),  # Chin
    (-225.0, 170.0, -135.0),  # Left eye left corner
    (225.0, 170.0, -135.0),  # Right eye right corne
    (-150.0, -150.0, -125.0),  # Left Mouth corner
    (150.0, -150.0, -125.0)  # Right mouth corner
])
cap = cv2.VideoCapture(0)
# Camera internals
ret, img = cap.read()
size = img.shape
focal_length = size[1]
center = (size[1] / 2, size[0] / 2)
camera_matrix = np.array(
    [[focal_length, 0, center[0]],
     [0, focal_length, center[1]],
     [0, 0, 1]], dtype="double"
)


def eye_aspect_ratio(eye):
    # compute the euclidean distances between the two sets of
    # vertical eye landmarks (x, y)-coordinates
    A = dist.euclidean(eye[1], eye[5])
    B = dist.euclidean(eye[2], eye[4])
    # compute the euclidean distance between the horizontal
    # eye landmark (x, y)-coordinates
    C = dist.euclidean(eye[0], eye[3])
    # compute the eye aspect ratio
    ear = (A + B) / (2.0 * C)
    # return the eye aspect ratio
    return ear


def head_pose_points(img, rotation_vector, translation_vector, camera_matrix):
    rear_size = 1
    rear_depth = 0
    front_size = img.shape[1]
    front_depth = front_size * 2
    val = [rear_size, rear_depth, front_size, front_depth]
    point_2d = get_2d_points(rotation_vector, translation_vector, camera_matrix, val)
    y = (point_2d[5] + point_2d[8]) // 2
    x = point_2d[2]

    return x, y


def get_2d_points(rotation_vector, translation_vector, camera_matrix, val):
    """Return the 3D points present as 2D for making annotation box"""
    point_3d = []
    dist_coeffs = np.zeros((4, 1))
    rear_size = val[0]
    rear_depth = val[1]
    point_3d.append((-rear_size, -rear_size, rear_depth))
    point_3d.append((-rear_size, rear_size, rear_depth))
    point_3d.append((rear_size, rear_size, rear_depth))
    point_3d.append((rear_size, -rear_size, rear_depth))
    point_3d.append((-rear_size, -rear_size, rear_depth))

    front_size = val[2]
    front_depth = val[3]
    point_3d.append((-front_size, -front_size, front_depth))
    point_3d.append((-front_size, front_size, front_depth))
    point_3d.append((front_size, front_size, front_depth))
    point_3d.append((front_size, -front_size, front_depth))
    point_3d.append((-front_size, -front_size, front_depth))
    point_3d = np.array(point_3d, dtype=np.float).reshape(-1, 3)

    # Map to 2d img points
    (point_2d, _) = cv2.projectPoints(point_3d,
                                      rotation_vector,
                                      translation_vector,
                                      camera_matrix,
                                      dist_coeffs)
    point_2d = np.int32(point_2d.reshape(-1, 2))
    return point_2d


def estimatePos(marks):
    image_points = np.array([
        marks[30],  # Nose tip
        marks[8],  # Chin
        marks[36],  # Left eye left corner
        marks[45],  # Right eye right corne
        marks[48],  # Left Mouth corner
        marks[54]  # Right mouth corner
    ], dtype="double")
    dist_coeffs = np.zeros((4, 1))  # Assuming no lens distortion
    (success, rotation_vector, translation_vector) = cv2.solvePnP(model_points, image_points, camera_matrix,
                                                                  dist_coeffs, flags=cv2.SOLVEPNP_UPNP)

    (nose_end_point2D, jacobian) = cv2.projectPoints(np.array([(0.0, 0.0, 1000.0)]), rotation_vector,
                                                     translation_vector, camera_matrix, dist_coeffs)

    p1 = (int(image_points[0][0]), int(image_points[0][1]))
    p2 = (int(nose_end_point2D[0][0][0]), int(nose_end_point2D[0][0][1]))
    x1, x2 = head_pose_points(img, rotation_vector, translation_vector, camera_matrix)

    try:
        m = (p2[1] - p1[1]) / (p2[0] - p1[0])
        ang1 = int(math.degrees(math.atan(m)))
    except:
        ang1 = 90

    try:
        m = (x2[1] - x1[1]) / (x2[0] - x1[0])
        ang2 = int(math.degrees(math.atan(-1 / m)))
    except:
        ang2 = 90

    return ang1, ang2, p1, x1


def alarmNotLooking(ang1, ang2, img):
    font = cv2.FONT_HERSHEY_SIMPLEX
    if ang1 >= ANGLE:
        print('Head down')
        cv2.putText(img, 'Head down', (30, 30), font, 2, (255, 255, 128), 3)

    elif ang1 <= -ANGLE:
        print('Head up')
        cv2.putText(img, 'Head up', (30, 30), font, 2, (255, 255, 128), 3)

    if ang2 >= ANGLE:
        print('Head right')
        cv2.putText(img, 'Head right', (30, 30), font, 2, (255, 255, 128), 3)
    elif ang2 <= -ANGLE:
        print('Head left')
        cv2.putText(img, 'Head left', (30, 30), font, 2, (255, 255, 128), 3)

    # print("y: %d       x: %d" % (ang1, ang2))
    return ang1 >= ANGLE or ang1 <= -ANGLE or ang2 >= ANGLE or ang2 <= -ANGLE


def main(folder):
    dont_show = False
    save_output = True
    COUNTER = 0
    FACE_COUNTER = 0
    POSE_COUNTER = 0

    EYE_AR_THRESH = 0.27
    EYE_AR_CONSEC_FRAMES = 45
    FACE_AR_CONSEC_FRAMES = 30
    POSE_AR_CONSEC_FRAMES = 45

    (lStart, lEnd) = 42, 48
    (rStart, rEnd) = 36, 42

    model = '/home/pi/Desktop/safe-driving/Facial_LandmarkDetector/data/output/model/facial_landmark_cnn.tflite'
    model = './Facial_LandmarkDetector/data/output/model/facial_landmark_cnn.tflite'

    mark_detector = MarkDetector(model)

    if save_output:
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(cap.get(cv2.CAP_PROP_FPS)) / 6
        i = 0

        output_path = "../logs/" + folder + "/face" + str(i) + ".avi"
        while os.path.isfile(output_path):
            i += 1
            output_path = "../logs/" + folder + "/face" + str(i) + ".avi"
        print(output_path)
        out = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(*'XVID'), int(fps), (width, height))

    while cap.isOpened():
        return_value, img_captured = cap.read()

        # print(modules.SPEED)
        if modules.SPEED > 1 and not modules.ALARM_ON:
            img_captured = cv2.cvtColor(img_captured, cv2.COLOR_BGR2RGB)
            if not return_value:
                continue
            copy = img_captured.copy()
            facebox = mark_detector.extract_cnn_facebox(img_captured)
            if facebox is not None:
                print("program found face")
                cv2.rectangle(copy, (facebox[0], facebox[1]), (facebox[2], facebox[3]), (0, 255, 0), 2)
                face = img_captured[facebox[1]: facebox[3], facebox[0]: facebox[2]]
                h = face.shape[1]
                resized_face = cv2.resize(face, (IMG_HEIGHT, IMG_WIDTH))
                face_img = cv2.cvtColor(resized_face, cv2.COLOR_BGR2GRAY)
                face_img = face_img.reshape(1, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL)
                marks = mark_detector.detect_marks(face_img.astype(np.float32))
                marks *= h
                marks[:, 0] += facebox[0]
                marks[:, 1] += facebox[1]
                mark_detector.draw_marks(copy, marks)
                ang1, ang2, p1, x1 = estimatePos(marks)
                if alarmNotLooking(ang1, ang2, copy):
                    POSE_COUNTER += 1
                    if POSE_COUNTER >= POSE_AR_CONSEC_FRAMES:
                        cv2.putText(copy, "LOOK FORWARD", (30, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                                    (0, 0, 255),
                                    2)
                        # if the alarm is not on, turn it on
                        if not modules.ALARM_ON:
                            """
                            t = Thread(target=play_alarm)
                            t.daemon = True
                            t.start()
                            """
                            play_alarm()

                else:
                    leftEye = marks[lStart:lEnd]
                    rightEye = marks[rStart:rEnd]
                    leftEAR = eye_aspect_ratio(leftEye)
                    rightEAR = eye_aspect_ratio(rightEye)
                    # average the eye aspect ratio together for both eyes
                    ear = (leftEAR + rightEAR) / 2.0
                    if ear <= EYE_AR_THRESH:
                        COUNTER += 1
                        # if the eyes were closed for a sufficient number of
                        # then sound the alarm
                        if COUNTER >= EYE_AR_CONSEC_FRAMES:
                            cv2.putText(copy, "WAKE UP", (30, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                                        (0, 0, 255),
                                        2)
                            # if the alarm is not on, turn it on
                            if not modules.ALARM_ON:
                                """
                                t = Thread(target=play_alarm)
                                t.daemon = True
                                t.start()
                                """
                                play_alarm()
                            print("ALERT!! wake up")

                    # otherwise, the eye aspect ratio is not below the blink
                    # threshold, so reset the counter and alarm
                    else:
                        POSE_COUNTER = 0
                        COUNTER = 0
                        FACE_COUNTER = 0
                        print("EAR: {:.2f}".format(ear))
                        cv2.putText(copy, "EAR: {:.2f}".format(ear), (30, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                                    (0, 0, 255),
                                    2)

            else:
                FACE_COUNTER += 1
                print("cant locate face")
                # if the eyes were closed for a sufficient number of
                # then sound the alarm
                if FACE_COUNTER >= FACE_AR_CONSEC_FRAMES:
                    cv2.putText(copy, "CANT LOCATE FACE", (30, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                                (0, 0, 255),
                                2)
                    # if the alarm is not on, turn it on
                    if not modules.ALARM_ON:
                        play_alarm()
                        """
                        t = Thread(target=play_alarm)
                        t.daemon = True
                        t.start()
                        """
                    print("ALERT!!")


        else:
            COUNTER = 0
            FACE_COUNTER = 0
            time.sleep(0.5)

        if save_output:
            out.write(cv2.cvtColor(copy, cv2.COLOR_RGB2BGR))
        if not dont_show:
            cv2.imshow("", cv2.cvtColor(copy, cv2.COLOR_RGB2BGR))

        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break

# if __name__ == '__main__':
#    main()
