import serial
import time
from decimal import *
from subprocess import call
from gsmHat import GPS

import modules
import os


# Enable Serial Communication


def find(str, ch):
    for i, ltr in enumerate(str):
        if ltr == ch:
            yield i
 
 
def init():
    # Transmitting AT Commands to the Modem
    # '\r\n' indicates the Enter key

    port.write(str.encode('AT\r\n'))            
    rcv = port.read(100)
    print(rcv)
    if bytes.decode(rcv) == '':
        return False
    time.sleep(.1)
     
    port.write(str.encode('AT+CGNSPWR=1\r\n'))             # to power the GPS
    rcv = port.read(100)
    if bytes.decode(rcv) == '':
        return False
    print(rcv)
    time.sleep(.1)
     
    port.write(str.encode('AT+CGNSIPR=115200\r\n')) # Set the baud rate of GPS
    rcv = port.read(100)
    if bytes.decode(rcv) == '':
        return False
    print(rcv)
    time.sleep(.1)
     
    port.write(str.encode('AT+CGNSTST=1\r\n'))    # Send data received to UART
    rcv = port.read(100)
    if bytes.decode(rcv) == '':
        return False
    print(rcv)
    time.sleep(.1)
     
    port.write(str.encode('AT+CGNSINF\r\n'))       # Print the GPS information
    rcv = port.read(200)
    if bytes.decode(rcv) == '':
        return False
    print(rcv)
    time.sleep(.1)
    print("\n\n\n")
    return True

def getGPSData():
        
    fd = bytes.decode(port.read(200))        # Read the GPS data from UART
    #print("fd = ", fd)
    #time.sleep(.5)
    
    if '$GNRMC' in fd:        # To Extract Lattitude and 
        ps=fd.find('$GNRMC')        # Longitude
        dif=len(fd)-ps
        
        if dif > 50:
            data=fd[ps:(ps+50)]
            #print data
            ds=data.find('A')
            #print("ds = ", ds)
            # Check GPS is valid
            if ds > 0 and ds < 20:
                p=list(find(data, ","))
                lat=data[(p[2]+1):p[3]]
                lon=data[(p[4]+1):p[5]]
 
                # GPS data calculation
 
                s1=lat[2:len(lat)]
                s1=float(s1)
                s1=s1/60
                s11=int(lat[0:2])
                s1 = s11+s1
 
                s2=lon[3:len(lon)]
                s2=float(s2)
                s2=s2/60
                s22=int(lon[0:3])
                s2 = s22+s2
                
                print("lat = ", s1)
                print("log = ", s2)
                GPSObj = GPS()
                GPSObj.Latitude = s1    
                GPSObj.Longitude = s2
                return GPSObj
    return None

def speak(text):
    os.system("espeak ' " + text + " ' ")
    time.sleep(1)

def GpsModule():
    #os.system("amixer sset 'Master' 70%")
    global port
    #time.sleep(30)
    port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=1)
    while not init():
        print("cant connect")
        port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=1)
        continue
    start = 0
    end = 0
    obj1, obj2 = 0,0
    i = 0
    
    while True:
        for j in range(20):
            obj1 = getGPSData()
            if obj1 is not None:
                start = time.time()
                break
        if start == 0:
            time.sleep(1)
            init()
            continue
        if i is 0:
            modules.play_alarm("/home/pi/Downloads/intro.wav")
            i = 1
        time.sleep(2)
        while True:
            obj2 = getGPSData()
            if obj2 is not None:
                end = time.time()
                break
        
        delta_time = end - start
        distance = GPS.CalculateDeltaP(obj1, obj2)
        modules.SPEED = distance / delta_time
        #print("distance = ", distance)
        #print("speed = ", SPEED)
        #speak(text)
        print("delta time is {0}".format(delta_time))
        text = "speed is {0}".format(int(modules.SPEED))
        start = 0
        print(text)
        print("\n\n")
        time.sleep(2)
    
   
