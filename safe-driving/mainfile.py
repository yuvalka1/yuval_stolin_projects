from Facial_LandmarkDetector.validate_model import main as frontmodule
from object_detection.detect_video import main as objectmodule
# from gps import GpsModule
from threading import Thread
import modules
import time
import os
import datetime


def main():
    modules.initialize()

    folder = str(datetime.datetime.now()).split(".")[0].replace(" ", "-").replace(":", "-")
    th1 = Thread(target=frontmodule, args=(folder,))
    th2 = Thread(target=objectmodule, args=(folder,))
    try:
        os.mkdir("../logs/" + folder)
    except Exception as e:
        pass

    arr = [th2]#, th1]  # , th2]
    for t in arr:
        t.daemon = True
        t.start()


if __name__ == '__main__':
    main()
    time.sleep(3600)
