from threading import Lock
import cv2
import os
import winsound
lock = Lock()


def initialize():
    global SPEED, ALARM_ON, PREVIOS_DISTANCE, CONSE_TIMES, ALERT_DISTANCE
    SPEED, CONSE_TIMES = 10, 0
    ALERT_DISTANCE = 0
    PREVIOS_DISTANCE = 100
    ALARM_ON = False


def get_image(camera_id):
    with lock:
        cap = cv2.VideoCapture(camera_id)
        flag, img_captured = cap.read()
        cap.release()
        return flag, img_captured


