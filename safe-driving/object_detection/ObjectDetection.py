import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

from tensorflow.python.saved_model import tag_constants
import cv2
import numpy as np
from .core.yolov4 import filter_boxes
from .core import utils
from .core.config import *
import modules
from threading import Thread
from alarm import play_alarm


tflite = tf.lite


class ObjectDetection:
    def __init__(self, lite,  checkpoint="object_detection/checkpoints/custom-tiny-416"):
        self.use_tflite = lite
        if lite:
            self.interpreter = tflite.Interpreter(model_path=MODEL_PATH)
            self.interpreter.allocate_tensors()
            self.input_details = self.interpreter.get_input_details()
            self.output_details = self.interpreter.get_output_details()
        else:
            saved_model_loaded = tf.saved_model.load(checkpoint, tags=[tag_constants.SERVING])
            self.infer = saved_model_loaded.signatures['serving_default']

    def predict(self, image):
        if modules.SPEED == 0 or modules.ALARM_ON:
            return image
        frame = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_data = cv2.resize(frame, (INPUT_SIZE, INPUT_SIZE))
        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)
        if self.use_tflite:
            self.interpreter.set_tensor(self.input_details[0]['index'], image_data)
            self.interpreter.invoke()
            pred = [self.interpreter.get_tensor(self.output_details[i]['index']) for i in range(len(self.output_details))]
            boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25,
                                            input_shape=tf.constant([INPUT_SIZE, INPUT_SIZE]))
        else:
            batch_data = tf.constant(image_data)
            pred_bbox = self.infer(batch_data)
            for key, value in pred_bbox.items():
                boxes = value[:, :, 0:4]
                pred_conf = value[:, :, 4:]
        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=2,
            max_total_size=10,
            iou_threshold=IOU,
            score_threshold=SCORE
        )
        pred_bbox = [boxes.numpy(), scores.numpy(), classes.numpy(), valid_detections.numpy()]
        image, flag = utils.check_bbox(frame, pred_bbox, modules.SPEED)

        if flag and not modules.ALARM_ON:
            play_alarm()
            """
            t = Thread(target=play_alarm, args=("beep-02.wav",))
            t.daemon = True
            t.start()
            """
        return image
