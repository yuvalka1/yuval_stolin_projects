INPUT_SIZE = 416
CLASSES = "./object_detection/data/classes/obj.names"
MODEL_PATH = "./object_detection/checkpoints/object_detector.tflite"
IOU = 0.45
SCORE = 0.3
