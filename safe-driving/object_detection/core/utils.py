import cv2
import numpy as np
import winsound
from alarm import play_alarm
from .config import *
import modules
from threading import Thread


def read_class_names(class_file_name):
    arr = {0: "traffic sign", 1: "car", 2: "rider", 3: "train", 4: "motor", 5: "bus", 6: "truck", 7: "traffic light",
           8: "person", 9: "bike"}
    return arr


def distance_to_camera(object_height, image_height, object_class):
    heights = {"person": 1, "car": 2, "truck": 3.11, "bus": 3.2, "train": 4.11}
    focalLength = 3.04
    real_height = 1 if object_class not in heights.keys() else heights[object_class]
    real_height *= 1000
    sensor_height = 3.68
    # compute and return the distance from the maker to the camera
    return ((focalLength * real_height * image_height) / (object_height * sensor_height)) / 1000


def check_bbox(image, bboxes, speed, classes=read_class_names(CLASSES),
               allowed_classes=list(read_class_names(CLASSES).values()), show_label=True):
    num_classes = len(classes)
    image_h, image_w, _ = image.shape
    not_safe = False
    out_boxes, out_scores, out_classes, num_boxes = bboxes
    distances = []

    # find traffic light

    for i in range(num_boxes[0]):
        class_ind = int(out_classes[0][i])
        class_name = classes[class_ind]
        if class_name == classes[7]:
            coor = out_boxes[0][i]
            coor[0] = int(coor[0] * image_h)
            coor[2] = int(coor[2] * image_h)
            coor[1] = int(coor[1] * image_w)
            coor[3] = int(coor[3] * image_w)
            class_ind = int(out_classes[0][i])
            class_name = classes[class_ind]
            c1, c2 = (coor[1], coor[0]), (coor[3], coor[2])

            cv2.rectangle(image, c1, c2, (255, 0, 0), 2)
            return image, not_safe


    for i in range(num_boxes[0]):
        if int(out_classes[0][i]) < 0 or int(out_classes[0][i]) > num_classes:
            continue
        coor = out_boxes[0][i]
        coor[0] = int(coor[0] * image_h)
        coor[2] = int(coor[2] * image_h)
        coor[1] = int(coor[1] * image_w)
        coor[3] = int(coor[3] * image_w)

        fontScale = 0.5
        score = out_scores[0][i]
        class_ind = int(out_classes[0][i])

        class_name = classes[class_ind]

        # check if class is in allowed classes
        if class_name not in allowed_classes or class_name == classes[0]:
            continue
        else:

            mid_x = (coor[1] + coor[3]) / (2 * image_w)
            if 0.4 < mid_x < 0.6:
                c1, c2 = (coor[1], coor[0]), (coor[3], coor[2])
                cv2.rectangle(image, c1, c2, (255, 0, 0), 2)
                distance = distance_to_camera(abs(coor[3] - coor[1]), image_h, classes[class_ind])
                cv2.putText(image, "{:.2f}".format(distance), (c1[0], np.float32(c1[1] - 2)), cv2.FONT_HERSHEY_SIMPLEX,
                            1,
                            (255, 0, 255), 2, lineType=cv2.LINE_AA)
                cv2.putText(image, "{0}".format(class_name), (c1[0], np.float32(c1[1] - 40)), cv2.FONT_HERSHEY_SIMPLEX,
                            1,
                            (255, 0, 255), 2, lineType=cv2.LINE_AA)
                print("{0} was found {1} from canera".format(class_name, distance))

                # safe_distance = (0.75 + distance/speed) < 1.5
                distances.append(distance)
                if distance < 7:

                    if modules.ALERT_DISTANCE == 0:
                        # play_alarm()
                        modules.PREVIOS_DISTANCE = distance
                        modules.ALERT_DISTANCE = 1
                        return image, not not_safe
                    elif modules.PREVIOS_DISTANCE - distance > 0.2:
                        modules.PREVIOS_DISTANCE = distance
                        cv2.putText(image, "Danger!!!", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 2,
                                    lineType=cv2.LINE_AA)
                        # play_alarm()
                        modules.ALERT_DISTANCE = 0
                        return image, not not_safe

                if distance > 15:
                    continue
                if modules.PREVIOS_DISTANCE - distance > 1:
                    if modules.CONSE_TIMES == 2:
                        winsound.Beep(2000, 500)
                        modules.CONSE_TIMES = 0
                    else:
                        modules.CONSE_TIMES += 1
                else:
                    modules.CONSE_TIMES += 1

    if len(distances) > 0:
        modules.PREVIOS_DISTANCE = min(distances)

    return image, not_safe
