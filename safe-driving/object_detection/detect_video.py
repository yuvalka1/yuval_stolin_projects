import os
import time
import tensorflow as tf
import cv2
from .ObjectDetection import ObjectDetection


# python detect_video.py --weights ./checkpoints/custom-tiny-416 --size 416 --tiny --video ./data/video/car_driving.mp4
def main(folder):
    
    dont_show = False
    save_output = True
    # begin video capture
    
    video = 1

    video = "object_detection/data/video/collisions1.mp4"
        
    try:
        vid = cv2.VideoCapture(int(video))
    except:
        vid = cv2.VideoCapture(video)
        print("running vid")
        for i in range(6700):
            ret = vid.grab()
    
    if save_output:
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(vid.get(cv2.CAP_PROP_FPS)) / 6
        i = 1
        output_path = "../logs/" + folder + "/road" + str(i) + ".avi"
        while os.path.isfile(output_path):
            i += 1
            output_path = "../logs/" + folder + "/road" + str(i) + ".avi"

        print(output_path)
        out = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(*'XVID'), int(fps), (width, height))
        
    target = 2
    counter = 0
    detector = ObjectDetection(True)
    print("running object detention")

    while True:
        if counter == target:
            return_value, frame = vid.read()
            #if not return_value:
            #    continue
            #start = time.time()
            frame = detector.predict(frame)
            cv2.imwrite("object_detection/data/result.jpg", frame)

            #print(time.time() - start)
            if save_output:
                out.write(cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
            if not dont_show:
                cv2.namedWindow("result", cv2.WINDOW_AUTOSIZE)
                result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
                cv2.imshow("result", frame)

            if cv2.waitKey(1) & 0xFF == ord('q'): break

            counter = 0
        else:
            ret = vid.grab()
            #return_value, frame = get_image(video)
            counter += 1

    cv2.destroyAllWindows()
    


#if __name__ == '__main__':
#    try:
#        app.run(main)
#    except SystemExit:
#        pass
