#include "Communicator.h"

//usings
using std::cout;
using std::endl;
using std::cerr;

// using static const instead of macros 
static const unsigned short PORT = 8820;
static const unsigned int IFACE = 0;
static const char* WELCOME_MESSAGE = "Hello";
static const std::string EXIT_MESSAGE = "EXIT";

Communicator::Communicator()
{
}

Communicator::Communicator(RequestHandlerFactory& facory) : m_handlerFactory(facory)
{
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	printf(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);

		for (auto it = m_clients.begin(); it != m_clients.end(); it++)
		{
			::closesocket((*it).first);
			delete (*it).second;
		}
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	bindAndListen();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		handleNewClient();
	}
}


void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	printf("binded\n");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	printf("listening...\n");
}

void Communicator::handleNewClient()
{
	SOCKET client_socket = accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !\n");
	// create new thread for client	and detach from it
	std::thread tr(&Communicator::clientHandler, this, client_socket);
	tr.detach();
}
void Communicator::menu(SOCKET client_socket)
{
	bool logout = false;
	while (!logout)
	{
		RequestId code = (RequestId)Helper::getIntPartFromSocket(client_socket, 2);
		int len = Helper::getIntPartFromSocket(client_socket, sizeof(int));
		string json_msg = Helper::getStringPartFromSocket(client_socket, len);
		cout << (int)code << ": " << len << "  " << json_msg << endl;

		RequestInfo info;
		info.buffer = json_msg;
		info.id = code;
		info.receivalTime = std::time(nullptr);
		string msg_to_user;

		IRequestHandler* menuHandler = m_clients[client_socket];
		if (menuHandler->isRequestRelevent(info))
		{
			RequestResult result = menuHandler->handleRequest(info);
			if (info.id == RequestId::LOGOUT)
			{
				logout = true;
				std::map<SOCKET, IRequestHandler*>::iterator it = m_clients.find(client_socket);
				//delete it->second;
				it->second = result.newHandler;
			}
			msg_to_user = result.response;
			
		}
		else
		{
			ErrorResponse error("You must choose one of the options in the menu");
			msg_to_user = JsonResponsePacketSerializer::serializeResponse(error);
		}
		Helper::sendData(client_socket, msg_to_user);
	}
}


void Communicator::clientHandler(SOCKET client_socket)
{
	LoginRequestHandler* lrh = &m_handlerFactory.createLoginRequestHandler();
	m_clients[client_socket] = lrh;

	try
	{
		Helper::sendData(client_socket, WELCOME_MESSAGE);
		cout << Helper::getStringPartFromSocket(client_socket, strlen(WELCOME_MESSAGE)) << endl;
		do
		{
			connect(client_socket);
			menu(client_socket);
		} while (true);
	}
	catch (const std::exception& e)
	{
		cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << endl;
	}

	closesocket(client_socket);
}


void Communicator::connect(SOCKET client_socket)
{
	bool logined = false;
	while (!logined)
	{
		//user login
		RequestId code = (RequestId)Helper::getIntPartFromSocket(client_socket, 2);
		int len = Helper::getIntPartFromSocket(client_socket, sizeof(int));
		string json_msg = Helper::getStringPartFromSocket(client_socket, len);
		cout << (int)code << ": " << json_msg << endl;

		RequestInfo info;
		info.buffer = json_msg;
		info.id = code;
		info.receivalTime = std::time(nullptr);

		IRequestHandler* loginHandler = m_clients[client_socket];

		string msg_to_user;

		if (loginHandler->isRequestRelevent(info))
		{
			RequestResult result = loginHandler->handleRequest(info);

			if (result.newHandler != nullptr)
			{

				std::map<SOCKET, IRequestHandler*>::iterator it = m_clients.find(client_socket);
				//delete it->second;
				it->second = result.newHandler;
				logined = true;
			}
			msg_to_user = result.response;

		}
		else
		{
			ErrorResponse error("You must login\\signup before entering the app");
			msg_to_user = JsonResponsePacketSerializer::serializeResponse(error);
		}

		Helper::sendData(client_socket, msg_to_user);
	}
}
