#pragma once

#include <map>
#include <WinSock2.h>
#include <iostream>
#include <thread>
#include <string>
#include "IRequestHandler.h"
#include "Helper.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

using std::stringstream;

class Communicator
{
public:
	Communicator();
	Communicator(RequestHandlerFactory& facory);
	~Communicator();
	void startHandleRequests();

private:
	std::map<SOCKET, IRequestHandler*> m_clients;
	SOCKET _socket;
	RequestHandlerFactory m_handlerFactory;
	void connect(SOCKET client_socket);
	void menu(SOCKET client_socket);
	void bindAndListen();
	void handleNewClient();
	void clientHandler(SOCKET client_socket);
};
