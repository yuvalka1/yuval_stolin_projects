#pragma once

#define CURL_STATICLIB
#include <iostream>
#include <string>
#include <list>
#include "Question.h"
#include "../jsonCode/single_include/nlohmann/json.hpp"
#include "curl/curl.h"

#ifdef _DEBUG
#pragma comment(lib,"curl/libcurl_a_debug.lib")
#else
#pragma comment (lib, "curl/libcurl_a.lib")
#endif // DEBUG

#pragma comment (lib, "Normaliz.lib")
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Wldap32.lib")
#pragma comment (lib, "Crypt32.lib")
#pragma comment (lib, "advapi32.lib")

using std::string;
using std::list;
using json = nlohmann::json;

class IDataBase
{
public:
	virtual void open() = 0;
	virtual bool doesUserExist(string name) = 0;
	virtual bool doesPasswordMatch(string name, string pas) = 0;
	virtual bool addNewUser(string name, string pas, string email) = 0;
	virtual std::vector<std::string> getAllUsers() = 0;
	virtual list<Question> getQuestions(int x) = 0;
	virtual double getAveregeAnswerTime(string name) = 0;
	virtual int getNumCorrectAnswers(string name) = 0;
	virtual int getNumTotalAnswers(string name) = 0;
	virtual int getNumOfPlayerGame(string name) = 0;

private:
	virtual void initQuestions() = 0;
	virtual void initStatistics(string name) = 0;
};
