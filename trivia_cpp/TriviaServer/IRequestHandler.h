#pragma once

#include <ctime>
#include <iostream>

enum class RequestId
{
	LOGIN_ID,
	SIGNUP_ID,
	LOGOUT = 65, ROOMS, PLAYERS_IN_ROOM, STATISTICS, JOIN, CREATE, LEAVE_GAME, START_GAME, CLOSE_GAME, GET_ROOM_STATE
};

typedef struct RequestInfo
{
	RequestId id;
	std::time_t receivalTime;
	std::string buffer;

	RequestInfo() = default;

	RequestInfo(RequestId id, std::time_t receivalTime, std::string buffer)
	{
		this->id = id;
		this->receivalTime = receivalTime;
		this->buffer = buffer;
	}

} RequestInfo;

//class IRequestHandler;




class IRequestHandler
{
public:
	virtual bool isRequestRelevent(RequestInfo ri) = 0;
	virtual struct RequestResult handleRequest(RequestInfo ri) = 0;
};


struct RequestResult
{
	IRequestHandler* newHandler;
	std::string response;

	RequestResult() = default;

	RequestResult(IRequestHandler* newHandler, std::string response)
	{
		this->newHandler = newHandler;
		this->response = response;

	}



};

