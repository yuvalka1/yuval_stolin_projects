#include "JsonResponsePacketDeserializer.h"


LoginRequest JsonResponsePacketDeserializer::deserializeLoginRequest(string buffer)
{
	auto j = json::parse(buffer);
	std::cout << j.dump() <<j["password"] << std::endl;
	
	LoginRequest login;
	login.password = j["password"];
	login.username = j["username"];
	return login;
}

SignupRequest JsonResponsePacketDeserializer::deserializeSignupRequest(string buffer)
{
	auto j = json::parse(buffer);
	SignupRequest signup;
	signup.information = deserializeLoginRequest(buffer);
	signup.email = j["email"];
	return signup;
}

RoomRequest JsonResponsePacketDeserializer::deserializeRoomRequest(string buffer)
{
	auto j = json::parse(buffer);
	RoomRequest room;
	room.roomId = j["roomId"];
	return room;
}

CreateRoomRequest JsonResponsePacketDeserializer::deserializeCreateRoomRequest(string buffer)
{
	auto j = json::parse(buffer);
	CreateRoomRequest createRoom;
	createRoom.answerTimeout = j["answerTimeout"];
	createRoom.maxUsers = j["maxUsers"];
	createRoom.questionCount = j["questionCount"];
	createRoom.roomName = j["roomName"];
	return createRoom;
}
