#pragma once

#include <iostream>
#include "../jsonCode/single_include/nlohmann/json.hpp"

using std::string;
using json = nlohmann::json;

typedef struct LoginRequest
{
	string username;
	string password;

} LoginRequest;

typedef struct SignupRequest
{
	struct LoginRequest information;
	string email;
} SignupRequest;

typedef struct RoomRequest
{
	unsigned int roomId;
} RoomRequest, GetPlayersInRoomRequest, JoinRoomRequest;

typedef struct CreateRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} CreateRoomRequest;

class JsonResponsePacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(string buffer);
	static SignupRequest deserializeSignupRequest(string buffer);
	static RoomRequest deserializeRoomRequest(string buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(string buffer);
};
