#include "JsonResponsePacketSerializer.h"

string JsonResponsePacketSerializer::serializeResponse(ErrorResponse error)
{
	json j;
	j["msg"] = error.msg;
	string json_msg = j.dump();
	std::cout << string(1, char(responses::Error)) + padString(json_msg) + json_msg << std::endl;
	return string(1,char(responses::Error)) + padString(json_msg) + json_msg;
}

string JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
	json j;
	for (RoomData val : response._rooms)
	{
		to_json(j, val);
	}
	j["_status"] = response._status;

	return string(1, char(responses::GetRooms)) + padString(j.dump()) + j.dump();
}

string JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse response)
{
	json j;
	j["_status"] = response._status;
	j["_statistics"] = response._statistics;
	
	string json_msg = j.dump();

	return string(1, char(responses::GetStatistics)) + padString(json_msg) + json_msg;
}

string JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	json j;
	j["_answerTime"] = response._answerTime;
	j["_hasGameBegun"] = response._hasGameBegun;
	j["_players"] = response._players;
	j["_questionCount"] = response._questionCount;
	j["_status"] = response._status;

	string json_msg = j.dump();
	return string(1, char(responses::GetRoomStateResponse)) + padString(json_msg) + json_msg;
}

string JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	json j(response._rooms);
	string json_msg = j.dump();

	return string(1, char(responses::GetPlayerInRoom)) + padString(json_msg) + json_msg;
}

void JsonResponsePacketSerializer::to_json(json& j, const RoomData& val)
{
	j["id"] = val.id;
	j["name"] = val.name;
	j["maxPlayers"] = val.maxPlayers;
	j["timePerQuestion"] = val.timePerQuestion;
	j["isActive"] = val.isActive;
}

string JsonResponsePacketSerializer::padString(string json_msg)
{
	string  len = std::to_string(strlen(json_msg.c_str()));
	return  std::string(N_ZERO - len.length(), '0') + len;
}

string JsonResponsePacketSerializer::serializeResponse(ConnectionResponse response)
{
	json j;
	j["status"] = response.status;
	string json_msg = j.dump();
	std::cout << string(1, char(responses::Connection)) + padString(json_msg) + json_msg << std::endl;
	return string(1, char(responses::Connection)) + padString(json_msg) + json_msg;
}
