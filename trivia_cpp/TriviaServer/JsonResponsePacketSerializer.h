#pragma once

#include <iostream>
#include <vector> 
#include "RoomManager.h"
#include "../jsonCode/single_include/nlohmann/json.hpp"

#define N_ZERO 4

using std::string;
using json = nlohmann::json;
using std::vector;

;

typedef struct ErrorResponse
{
	string msg;
	ErrorResponse(string message)
	{
		this->msg = message;
	}
} ErrorResponse;

typedef struct ConnectionResponse
{
	unsigned int status;
	ConnectionResponse(unsigned int status)
	{
		this->status = status;
	}

} ConnectionResponse, LoginResponse, SignupResponse, LogoutResponse, JoinRoomResponse, 
CreateRoomResponse, CloseRoomResponse, StartRoomResponse, LeasveRoomResponse;

typedef struct GetPlayersInRoomResponse
{
	vector<string> _rooms;
	GetPlayersInRoomResponse(vector<string> rooms)
	{
		this->_rooms = rooms;
	}
} GetPlayersInRoomResponse;

typedef struct GetStatisticsResponse
{
	unsigned int _status;
	vector<string> _statistics;
	GetStatisticsResponse(unsigned int status, vector<string> statistics)
	{
		this->_statistics = statistics;
		this->_status = status;
	}
} GetStatisticsResponse;

typedef struct GetRoomsResponse
{
	unsigned int _status;
	vector<RoomData> _rooms;
	GetRoomsResponse(unsigned int status, vector<RoomData> rooms)
	{
		this->_rooms = rooms;
		this->_status = status;
	}

} GetRoomsResponse;

typedef struct GetRoomStateResponse
{
	unsigned int _status;
	vector<string> _players;
	bool _hasGameBegun;
	unsigned int _questionCount;
	unsigned int _answerTime;
	GetRoomStateResponse(unsigned int status, vector<string> players, bool begun, unsigned count, unsigned int time)
	{
		this->_answerTime = time;
		this->_hasGameBegun = begun;
		this->_players = players;
		this->_questionCount = count;
		this->_status = status;
	}
}GetRoomStateResponse;




enum class responses { Connection = 115, Error = 69, GetRooms = 82, GetStatistics = 116, GetPlayerInRoom = 112, GetRoomStateResponse = 63};

class JsonResponsePacketSerializer
{
public:
	static string serializeResponse(ErrorResponse response);
	static string serializeResponse(ConnectionResponse response);
	static string serializeResponse(GetRoomsResponse response);
	static string serializeResponse(GetPlayersInRoomResponse response);
	static string serializeResponse(GetStatisticsResponse response);
	static string serializeResponse(GetRoomStateResponse response);
	

private:
	static void to_json(json& j, const RoomData& val);
	static string padString(string len);
};


