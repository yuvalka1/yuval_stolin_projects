#include "LoggedUser.h"

LoggedUser::LoggedUser(string name) : m_username(name)
{
}

string LoggedUser::getUserName()
{
	return m_username;
}

bool LoggedUser::operator==(const string& other)
{
	return this->m_username == other;
}

bool LoggedUser::operator==(const LoggedUser& other)
{
	return m_username == other.m_username;
}
