#pragma once

#include <iostream>
#include <string>

using std::string;

class LoggedUser
{
public:
	LoggedUser() {};
	LoggedUser(string name);

	bool operator==(const string& other);
	bool operator==(const LoggedUser& other);
	
	string getUserName();

private:
	string m_username;
};

