#include "LoginManager.h"

void LoginManager::setDB(IDataBase* db)
{
	m_database = db;
}

bool LoginManager::signup(string name, string password, string email)
{
	if (m_database->addNewUser(name, password, email))
	{
		this->m_loggedUsers.push_back(LoggedUser(name));
		return true;
	}
	return false;
}

bool LoginManager::login(string name, string password)
{
	if (this->m_database->doesPasswordMatch(name, password))
	{
		this->m_loggedUsers.push_back(LoggedUser(name));
		return true;
	}
	return false;
}

bool LoginManager::logout(string name)
{
	if (this->m_database->doesUserExist(name))
	{
		for (vector<LoggedUser>::iterator it = this->m_loggedUsers.begin(); it != this->m_loggedUsers.end(); ++it)
		{
			if (*it == name)
			{
				this->m_loggedUsers.erase(it);
				return true;
			}
		}
		
	}
	return false;
}
