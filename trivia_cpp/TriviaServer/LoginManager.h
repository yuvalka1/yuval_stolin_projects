#pragma once

#include <vector>
#include "IDataBase.h"
#include "SqliteDataBase.h"
#include "LoggedUser.h"

using std::vector;

class LoginManager
{
public:
	void setDB(IDataBase* db);
	bool signup(string name, string passwprd, string email);
	bool login(string name, string passwprd);
	bool logout(string name);

private:
	IDataBase* m_database;
	vector<LoggedUser> m_loggedUsers;
};
