#include "LoginRequestHandler.h"

RequestResult LoginRequestHandler::login(RequestInfo ri)
{
	LoginRequest login = JsonResponsePacketDeserializer::deserializeLoginRequest(ri.buffer);
	LoginManager lm = m_handlerFactory->getLoginManager();
	if (lm.login(login.username, login.password))
	{
		ConnectionResponse response(1);
		string msg_to_user = JsonResponsePacketSerializer::serializeResponse(response);
		return RequestResult(new MenuRequestHandler(m_handlerFactory), msg_to_user);
	}

	return RequestResult(nullptr, "Error: Can't login");


}

RequestResult LoginRequestHandler::signup(RequestInfo ri)
{
	SignupRequest sign = JsonResponsePacketDeserializer::deserializeSignupRequest(ri.buffer);
	LoginManager lm = m_handlerFactory->getLoginManager();
	if (lm.signup(sign.information.username, sign.information.password, sign.email))
	{
		ConnectionResponse response(1);
		string msg_to_user = JsonResponsePacketSerializer::serializeResponse(response);
		return RequestResult(new MenuRequestHandler(m_handlerFactory), msg_to_user);
	}
	return RequestResult(nullptr, "Error: Can't signup");
}

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handlerFactory)
{
	m_handlerFactory = handlerFactory;
}
		

bool LoginRequestHandler::isRequestRelevent(RequestInfo ri)
{
	return ri.id == RequestId::LOGIN_ID || ri.id == RequestId::SIGNUP_ID;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo ri)
{
	RequestResult result;
	if (ri.id == RequestId::LOGIN_ID)
	{
		result = login(ri);
	}
	else if (ri.id == RequestId::SIGNUP_ID)
	{
		result = signup(ri);
	}
	
	return result;
}
