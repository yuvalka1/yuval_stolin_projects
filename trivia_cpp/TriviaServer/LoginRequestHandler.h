#pragma once

#include "IRequestHandler.h"
#include "JsonResponsePacketDeserializer.h"
#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* m_handlerFactory;
	struct RequestResult login(RequestInfo ri);
	struct RequestResult signup(RequestInfo ri);

public:
	LoginRequestHandler(RequestHandlerFactory* handlerFactory);
	bool isRequestRelevent(RequestInfo ri) override ;
	struct RequestResult handleRequest(RequestInfo ri) override;
};
