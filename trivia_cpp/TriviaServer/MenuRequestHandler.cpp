#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory* handlerFactory) : IRequestHandler()
{
	this->m_handlerFactory = handlerFactory;
}

bool MenuRequestHandler::isRequestRelevent(RequestInfo ri)
{
	return TestCheck::is_value((int)ri.id);
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo ri)
{
	RequestId req = (RequestId)ri.id;
	RequestResult result;
	switch (req)
	{
	case RequestId::LOGOUT:
		result = logout(ri);
		break;
	case RequestId::ROOMS:
		result = getRooms(ri);
		break;
	case RequestId::PLAYERS_IN_ROOM:
		result = getPlayersInRoom(ri);
		break;
	case RequestId::STATISTICS:
		result = getStatistics(ri);
		break;
	case RequestId::JOIN:
		result = joinRoom(ri);
		break;
	case RequestId::CREATE:
		result = createRoom(ri);
		break;
	default:
		break;
	}
	return result;
}

RequestResult MenuRequestHandler::logout(RequestInfo ri)
{
	ConnectionResponse response(1);
	return RequestResult(&this->m_handlerFactory->createLoginRequestHandler(), JsonResponsePacketSerializer::serializeResponse(response));
}

RequestResult MenuRequestHandler::getRooms(RequestInfo ri)
{
    RoomManager& manager = 	this->m_handlerFactory->getRoomManager();
	std::vector<RoomMetaData>rooms = manager.getRooms();
	vector<RoomData> roomData;
	string msg;
	
	for (int i = 0; i < rooms.size(); i++)
	{
		roomData.push_back(rooms.at(i).getRoomData());
	}
	GetRoomsResponse response(1, roomData);
	string msg_to_user = JsonResponsePacketSerializer::serializeResponse(response);
	return RequestResult(nullptr,msg_to_user);
	
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo ri)
{
	RoomRequest request = JsonResponsePacketDeserializer::deserializeRoomRequest(ri.buffer);
	RoomManager& manager = this->m_handlerFactory->getRoomManager();
	vector<RoomMetaData> rooms = manager.getRooms();
	vector<LoggedUser> usersInRoom;
	vector<string> users;

	for (auto it = rooms.begin(); it != rooms.end(); ++it)
	{
		if (it->getRoomData().id == request.roomId)
		{
			usersInRoom = it->getAllUsers();
		}
	}

	for (auto it = usersInRoom.begin(); it != usersInRoom.end(); it++)
	{
		users.push_back(it->getUserName());
	}

	GetPlayersInRoomResponse response(users);
	return RequestResult(nullptr, JsonResponsePacketSerializer::serializeResponse(response));
}

RequestResult MenuRequestHandler::getStatistics(RequestInfo ri)
{
	StatisticsManager& manager = this->m_handlerFactory->getStatisticsManager();
	StatisticsData data = manager.getStatistics();
	string statistics = data.to_string();
	return RequestResult(nullptr, statistics);
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo ri)
{
	JoinRoomRequest request = JsonResponsePacketDeserializer::deserializeRoomRequest(ri.buffer);
	RoomManager& manager = this->m_handlerFactory->getRoomManager();
	vector<RoomMetaData> vec = manager.getRooms();
	for (auto it = vec.begin(); it != vec.end(); ++it)
	{
		if (it->getRoomData().id == request.roomId)
		{
			it->addUser(this->m_user.getUserName());
			break;
		}
	}
	return RequestResult(nullptr /*will be room Hnadeler*/, "You have successfully joined");
}

RequestResult MenuRequestHandler::createRoom(RequestInfo ri)
{
	//do with yair
	CreateRoomRequest request = JsonResponsePacketDeserializer::deserializeCreateRoomRequest(ri.buffer);
	RoomManager& manager = this->m_handlerFactory->getRoomManager();
	manager.createRoom(this->m_user, request);
	
	ConnectionResponse response(1);
	return RequestResult(nullptr, JsonResponsePacketSerializer::serializeResponse(response));
}
