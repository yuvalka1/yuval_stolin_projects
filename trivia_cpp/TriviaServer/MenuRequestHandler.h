#pragma once

#include <stdio.h>
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include <algorithm>
#include <sstream>
#include <iterator>

class RequestHandlerFactory;

template<typename EnumType, EnumType... Values> class EnumCheck;

template<typename EnumType> class EnumCheck<EnumType>
{
public:
	template<typename IntType>
	static bool constexpr is_value(IntType) { return false; }
};

template<typename EnumType, EnumType V, EnumType... Next>
class EnumCheck<EnumType, V, Next...> : private EnumCheck<EnumType, Next...>
{
	using super = EnumCheck<EnumType, Next...>;

public:
	template<typename IntType>
	static bool constexpr is_value(IntType v)
	{
		return v == static_cast<IntType>(V) || super::is_value(v);
	}
};



using TestCheck = EnumCheck<RequestId, RequestId::CREATE, RequestId::LOGOUT, RequestId::ROOMS, RequestId::PLAYERS_IN_ROOM, RequestId::STATISTICS, RequestId::JOIN>;



class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory* handelerFactory);
	bool isRequestRelevent(RequestInfo ri) override;
	struct RequestResult handleRequest(RequestInfo ri) override;

private:
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;
	struct RequestResult logout(RequestInfo ri);
	struct RequestResult getRooms(RequestInfo ri);
	struct RequestResult getPlayersInRoom(RequestInfo ri);
	struct RequestResult getStatistics(RequestInfo ri);
	struct RequestResult joinRoom(RequestInfo ri);
	struct RequestResult createRoom(RequestInfo ri);
};
