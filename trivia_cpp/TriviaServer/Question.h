#pragma once

#include <iostream>
#include <string>
#include <vector>

using std::string;

class Question
{
private:
	string m_question;
	std::vector<string> m_possibleAnswers;
	string m_answer;

public:
	Question();
	Question(string q, std::vector<string> incorrect_answers,  string c);
	~Question();

	string getQuestion() { return this->m_question; }
	std::vector<string> getPossibleAnswers() { return this->m_possibleAnswers; }
	string getCorrectAnswer() { return this->m_answer; }
};

