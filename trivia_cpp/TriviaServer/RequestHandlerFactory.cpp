#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory()
{
	m_database = new SqliteDataBase();
	m_database->open();
	m_loginManager.setDB(m_database);
	m_statisticsManager.setDB(m_database);
}

LoginRequestHandler RequestHandlerFactory::createLoginRequestHandler()
{
	return LoginRequestHandler(this);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return m_loginManager;
}

MenuRequestHandler RequestHandlerFactory::createMenuRequestHandler()
{
	return MenuRequestHandler(this);
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return m_statisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return m_roomManager;
}
