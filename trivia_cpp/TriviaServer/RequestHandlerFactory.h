#pragma once

#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "IDataBase.h"
#include "SqliteDataBase.h"
#include "MenuRequestHandler.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;


class RequestHandlerFactory
{
private:
	IDataBase* m_database;
	LoginManager m_loginManager;
	RoomManager m_roomManager;
	StatisticsManager m_statisticsManager;

public:
	RequestHandlerFactory();
	LoginRequestHandler createLoginRequestHandler();
	LoginManager& getLoginManager();
	MenuRequestHandler createMenuRequestHandler();
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
};
