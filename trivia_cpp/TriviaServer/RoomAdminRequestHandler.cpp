#include "RoomAdminRequestHandler.h"

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo ri)
{
    RoomManager& manager = this->m_handlerFactory->getRoomManager();
    for (auto room : manager.getRooms())
    {
        if (room.getRoomData().id == m_room.id)
        {
            manager.deleteRoon(m_room.id);
            break;
        }
    }

    return RequestResult(nullptr, "room deleted");
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo ri)
{
    return RequestResult();
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo ri)
{
    return RequestResult();
}

bool RoomAdminRequestHandler::isRequestRelevent(RequestInfo ri)
{
    return ri.id == RequestId::CLOSE_GAME || ri.id == RequestId::GET_ROOM_STATE || ri.id == RequestId::START_GAME;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo ri)
{
    RequestId req = (RequestId)ri.id;
    RequestResult result;
    switch (req)
    {
    case RequestId::START_GAME:
        result = startGame(ri);
        break;
    case RequestId::CLOSE_GAME:
        result = closeRoom(ri);
        break;
    case RequestId::GET_ROOM_STATE:
        result = getRoomState(ri);
        break;
    default:
        break;
    }
    return RequestResult();
}

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory* handlerFactory) : m_handlerFactory(handlerFactory)
{
}
