#include "RoomManager.h"

void RoomMetaData::addUser(string name)
{
	if (std::find(m_users.begin(), m_users.end(), LoggedUser(name)) != m_users.end())
	{
		m_users.push_back(LoggedUser(name));
	}
}

void RoomMetaData::removeUser(string name)
{
	auto it = std::find(m_users.begin(), m_users.end(), LoggedUser(name));
	if (it != m_users.end())
	{
		m_users.erase(it);
	}
}

vector<LoggedUser> RoomMetaData::getAllUsers()
{
	return this->m_users;
}

RoomData RoomMetaData::getRoomData()
{
	return this->m_metadata;
}

void RoomMetaData::setRoomData(CreateRoomRequest data)
{
	this->m_metadata.maxPlayers = data.maxUsers;
	this->m_metadata.name = data.roomName;
	this->m_metadata.timePerQuestion = data.answerTimeout;
	this->m_metadata.isActive = ROOM_STATES::WAITING;
	this->m_metadata.questionCount = data.questionCount;
	srand(time(NULL));
	this->m_metadata.id = rand() % MAX;
}

RoomMetaData::RoomMetaData()
{
}

RoomMetaData::RoomMetaData(LoggedUser user) 
{
	m_users.push_back(user);
}



void RoomManager::createRoom(LoggedUser user, CreateRoomRequest data)
{
	RoomMetaData metaData(user);
	metaData.setRoomData(data);
	this->m_rooms.insert(std::pair<int, RoomMetaData>(metaData.getRoomData().id, metaData));
}

void RoomManager::deleteRoon(int ID)
{
	if (m_rooms.find(ID) != m_rooms.end())
	{
		m_rooms.erase(ID);
	}
}

unsigned int RoomManager::getRoomState(int ID)
{
	auto it = this->m_rooms.find(ID);
	if (it != this->m_rooms.end())
	{
		return (unsigned int)it->second.getRoomData().isActive;
	}
	return 0;
}

vector<RoomMetaData> RoomManager::getRooms()
{
	 vector<RoomMetaData> rooms;
	 for (auto it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	 {
		rooms.push_back(it->second);
	 }
	 return rooms;
}
