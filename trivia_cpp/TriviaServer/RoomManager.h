#pragma once

#include <string>
#include <vector>
#include <map>
#include "LoggedUser.h"
#include "JsonResponsePacketDeserializer.h"
#include <time.h> 
#include <stdlib.h> 


using std::string;
using std::vector;
using std::map;

enum class ROOM_STATES : unsigned { WAITING = 1, PLAYING };
#define MAX 100000
typedef struct RoomData
{
	int id;
	string name;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	unsigned int questionCount;
	ROOM_STATES isActive;

	RoomData()
	{

	}
	RoomData(int id, string name, unsigned int max, unsigned int time, unsigned int questionCount) :
		id(id), name(name), maxPlayers(max), timePerQuestion(time), questionCount(questionCount)
	{
		isActive = ROOM_STATES::WAITING;
	}

	
} RoomData;

class RoomMetaData
{
private:
	RoomData m_metadata;
	vector<LoggedUser> m_users;

public:
	void addUser(string name);
	void removeUser(string name);
	vector<LoggedUser> getAllUsers();
	RoomData getRoomData();
	void setRoomData(CreateRoomRequest data);
	RoomMetaData();
	RoomMetaData(LoggedUser user);
	
	
};

class RoomManager
{
private:
	map<int, RoomMetaData> m_rooms;

public:
	void createRoom(LoggedUser user, CreateRoomRequest data);
	void deleteRoon(int ID);
	unsigned int getRoomState(int ID);
	vector<RoomMetaData> getRooms();
};
