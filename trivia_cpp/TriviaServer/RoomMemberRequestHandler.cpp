#include "RoomMemberRequestHandler.h"

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo ri)
{
    RoomManager& manager = this->m_handlerFactory->getRoomManager();
    for (auto room : manager.getRooms())
    {
        if (room.getRoomData().id == m_room.id)
        {
            room.removeUser(m_user.getUserName());
            break;
        }
    }

    return RequestResult(nullptr, "user left the room");
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo ri)
{
    return RequestResult();
}

bool RoomMemberRequestHandler::isRequestRelevent(RequestInfo ri)
{
    return ri.id == RequestId::LEAVE_GAME || ri.id == RequestId::GET_ROOM_STATE;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo ri)
{
    RequestId req = (RequestId)ri.id;
    RequestResult result;
    switch (req)
    {
    case RequestId::LEAVE_GAME:
        result = leaveRoom(ri);
        break;
    case RequestId::GET_ROOM_STATE:
        result = getRoomState(ri);
        break;
    default:
        break;
    }
    return result;
}

RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory* handlerFactory) : m_handlerFactory(handlerFactory)
{
}
