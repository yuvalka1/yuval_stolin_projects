#pragma once
#include "RoomManager.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class RoomMemberRequestHandler : public IRequestHandler
{
private:
	RoomData m_room;
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;
	RequestResult leaveRoom(RequestInfo ri);
	RequestResult getRoomState(RequestInfo ri);
public:
	bool isRequestRelevent(RequestInfo ri) override;
	RequestResult handleRequest(RequestInfo ri) override;
	RoomMemberRequestHandler(RequestHandlerFactory* handlerFactory);
};



