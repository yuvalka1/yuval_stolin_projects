#include "Server.h"

static const std::string EXIT_MESSAGE = "EXIT";

Server::Server()
{
	m_communicator = new Communicator(this->m_handlerFactory);
}

Server::~Server()
{
}

void Server::run()
{
	std::thread t_connector(&Server::consoleHandler, this);
	t_connector.detach();
	m_communicator->startHandleRequests();
	
}

void Server::consoleHandler()
{
	std::string msg;
	std::cin >> msg;
	while (msg != EXIT_MESSAGE)
	{
		std::cin >> msg;
	}
	delete m_communicator;
}
