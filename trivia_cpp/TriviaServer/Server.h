#pragma once

#include <string>
#include <thread>
#include "Communicator.h"
#include "IDataBase.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	Server();
	~Server();
	void run();

private:
	IDataBase* m_database;
	RequestHandlerFactory m_handlerFactory;
	Communicator* m_communicator;

	void consoleHandler();
};
