#include "SqliteDataBase.h"

bool SqliteDataBase::doesUserExist(string name)
{
	string sql = "SELECT * FROM USERS WHERE USERNAME = '" + name + "';";

	return getDataFromSQL(sql).size() == 1;
}

bool SqliteDataBase::doesPasswordMatch(string name, string pas)
{
	if (doesUserExist(name))
	{
		string sql = "select * from Users where USERNAME ='" + name + "' and password='" + pas + "';";
		return getDataFromSQL(sql).size() == 1;
	}
	return false;
}

bool SqliteDataBase::addNewUser(string name, string pas, string email)
{
	if (!doesUserExist(name))
	{
		string sql = "INSERT INTO USERS(USERNAME, PASSWORD,EMAIL) VALUES("
			"'" + name + "','" + pas + "','" + email + "');";
		runSQL(sql);
		initStatistics(name);
		return true;
	}
	return false;
}

std::vector<std::string> SqliteDataBase::getAllUsers()
{
	std::vector<std::string> names;
	string sql = "SELECT username FROM users;";
	Records rds = getDataFromSQL(sql);
	for (auto name : rds)
	{
		names.push_back(name.at(0));
	}
	return names;
}



list<Question> SqliteDataBase::getQuestions(int x)
{
	return this->listQuestions;
}

double SqliteDataBase::getAveregeAnswerTime(string name)
{
	double answerTime = getAnswerTime(name);
	if (answerTime != 0.0f)
	{
		return getNumCorrectAnswers(name) / answerTime;
	}
	return 0.0f;
}

float SqliteDataBase::getAnswerTime(string name)
{
	string sql = "select totalCorrectedAnswerTime from statistics where username =  '" + name + "';";

	return std::stof(getDataFromSQL(sql).at(0).at(0));
}

int SqliteDataBase::getNumCorrectAnswers(string name)
{
	string sql = "select correctedAns from statistics where username =  '" + name + "';";

	return std::stoi(getDataFromSQL(sql).at(0).at(0));
}

int SqliteDataBase::getNumTotalAnswers(string name)
{
	string sql = "select totalAns from statistics where username =  '" + name + "';";

	return std::stoi(getDataFromSQL(sql).at(0).at(0));
}

int SqliteDataBase::getNumOfPlayerGame(string name)
{
	string sql = "select games from statistics where username =  '" + name + "';";

	return std::stoi(getDataFromSQL(sql).at(0).at(0));
}

void SqliteDataBase::initStatistics(string name)
{
	string sql = " insert into statistics(totalCorrectedAnswerTime,correctedAns,totalAns, games, username) values(0,0,0,0,'" + name + "');";
	runSQL(sql);
}

void SqliteDataBase::open()
{
	string dbName = "db.sqlite";

	string sqlUsersTABLE =
		"CREATE TABLE USERS("
		" USERNAME TEXT, "
		" PASSWORD TEXT, "
		" EMAIL TEXT, "
		" PRIMARY KEY(USERNAME));";

	string sqlQuestionsTable = "create table questions("
		"QUESTION TEXT, "
		"ANS1 TEXT, "
		"ANS2 TEXT, "
		"ANS3 TEXT, "
		"CORRECT TEXT, "
		"PRIMARY KEY(QUESTION));";

	string sqlStatisticsTable = "create table statistics("
		"totalCorrectedAnswerTime float, "
		"correctedAns int, "
		"totalAns int, "
		"games int, "
		"username text, "
		"FOREIGN KEY (username) REFERENCES USERS(USERNAME));";


	if (!checkIfExsistAndCreate(dbName, sqlUsersTABLE))
	{
		std::cerr << "there is a problem in opening table USERS \n" << sqlUsersTABLE << std::endl;
	}

	if (!checkIfExsistAndCreate(dbName, sqlQuestionsTable))
	{
		std::cerr << "there is a problem in opening table QUESTIONS \n" << sqlQuestionsTable << std::endl;
	}

	if (!checkIfExsistAndCreate(dbName, sqlStatisticsTable))
	{
		std::cerr << "there is a problem in opening table statistics \n" << sqlStatisticsTable << std::endl;
	}


}

bool SqliteDataBase::checkIfExsistAndCreate(string& fileName, string& sql)
{
	int doesFileExist = _access(fileName.c_str(), 0);
	int res = sqlite3_open(fileName.c_str(), &DB);
	if (res != SQLITE_OK)
	{
		DB = nullptr;
		return false;
	}
	if (doesFileExist == 0)
	{
		char* errMsg;
		int result = sqlite3_exec(DB, sql.c_str(), nullptr, nullptr, &errMsg);
		if (result  != SQLITE_OK && result != SQLITE_ACCESS_READWRITE)//ALBUM IS ALRESY EXISTS
		{
			std::cout << errMsg << "\n";
			sqlite3_free(errMsg);
			return false;
		}
	}

	return true;
}



void SqliteDataBase::runSQL(string& sql)
{
	char* errMsg;
	int res = sqlite3_exec(DB, sql.c_str(), nullptr, nullptr, &errMsg);
	if (res != SQLITE_OK)
	{
		std::cout << "error while running sql\n" << "ERROR" << errMsg << std::endl << sql << std::endl;
		sqlite3_free(errMsg);
	}
}

Records SqliteDataBase::getDataFromSQL(string& sql)
{
	char* errMsg;
	Records records;
	int res = sqlite3_exec(DB, sql.c_str(), &SqliteDataBase::select_callback, &records, &errMsg);
	if (res != SQLITE_OK)
	{
		std::cout << "error while running sql\n" << "ERROR" << errMsg << std::endl << sql << std::endl;
		sqlite3_free(errMsg);
	}
	return records;
}



int SqliteDataBase::select_callback(void* p_data, int num_fields, char** p_fields, char** p_col_names)
{
	Records* records = static_cast<Records*>(p_data);
	try {
		std::vector<string> temp;
		for (int i = 0; i < num_fields; i++)
		{
			temp.push_back(p_fields[i] ? p_fields[i] : "NULL");
		}
		records->push_back(temp);
	}
	catch (...) {
		// abort select on failure, don't let exception propogate thru sqlite3 call-stack
		return 1;
	}
	return 0;
}

void SqliteDataBase::initQuestions()
{
	CURL* curl;
	CURLcode res;
	std::string readBuffer;

	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, "https://opentdb.com/api.php?amount=10&type=multiple");
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);

		auto j = json::parse(readBuffer);
		std::cout << j["results"].size() << std::endl;
		for (auto& element : j["results"])
		{
			std::cout << element << "\n\n\n";
			std::vector<string> incorrect_answers = element["incorrect_answers"];
			string correct_answer = element["correct_answer"];
			string question = element["question"];
			listQuestions.push_back(Question(question, incorrect_answers, correct_answer));
			string sql = "INSERT INTO QUESTIONS(QUESTION, "
				"ANS1, ANS2 , ANS3 , CORRECT TEXT) VALUES("
				"'" + question + "','" + incorrect_answers.at(0) + "','" + incorrect_answers.at(1) + "','" + incorrect_answers.at(2) + "','"
				+ correct_answer + "');";
			runSQL(sql);
		}
	}
}



size_t SqliteDataBase::WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}
