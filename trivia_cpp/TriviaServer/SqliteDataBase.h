#pragma once

#include <string>
#include <vector>
#include <io.h>
#include "IDataBase.h"
#include "sqlite3.h"
#include "LoggedUser.h"

using Record = std::vector<std::string>;
using Records = std::vector<Record>;

class SqliteDataBase : public IDataBase
{
public:
	SqliteDataBase() = default;
	bool doesUserExist(string name) override;
	bool doesPasswordMatch(string name, string pas) override;
	bool addNewUser(string name, string pas, string email) override;
	std::vector<std::string> getAllUsers() override;
	void open() override;
	list<Question> getQuestions(int x) override;
	double getAveregeAnswerTime(string name)override;
	int getNumCorrectAnswers(string name) override;
	int getNumTotalAnswers(string name) override;
	int getNumOfPlayerGame(string name) override;

private:
	sqlite3* DB;

	bool checkIfExsistAndCreate(string& fileName, string& sql);
	float getAnswerTime(string name);
	void runSQL(string& sql);
	Records getDataFromSQL(string& sql);
	static int select_callback(void* p_data, int num_fields, char** p_fields, char** p_col_names);
	void  initQuestions() override;
	void  initStatistics(string name) override;
	list<Question> listQuestions;
	static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp);
};
