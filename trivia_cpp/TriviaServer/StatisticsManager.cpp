#include "StatisticsManager.h"

int StatisticsManager::calculateScore(PlayerStatistics ps)
{
	return ps.numOfTotalAnswers==0 ? 0 : ps.numOfCorrectAnswers / ps.numOfTotalAnswers;
}

void StatisticsManager::setDB(IDataBase* db)
{
	m_database = db;
}

StatisticsData StatisticsManager::getStatistics()
{
	StatisticsData data;
	std::vector<std::string> names = m_database->getAllUsers();
	for (auto username : names)
	{
		PlayerStatistics ps;
		ps.name = username;
		ps.averageAnswerTime = m_database->getAveregeAnswerTime(username);
		ps.numOfCorrectAnswers = m_database->getNumCorrectAnswers(username);
		ps.numOfGamesPlayed = m_database->getNumOfPlayerGame(username);
		ps.numOfTotalAnswers = m_database->getNumTotalAnswers(username);
		ps.numOfWrongAnswers = ps.numOfTotalAnswers - ps.numOfCorrectAnswers;
		data.players.push_back(ps);
		
		int score = calculateScore(ps);
		if (score > data.bestPlayers[0].second)
		{
			data.bestPlayers[2] = data.bestPlayers[1];
			data.bestPlayers[1] = data.bestPlayers[0];
			data.bestPlayers[0] = playerScore(username, score);
		}
		else if (score > data.bestPlayers[1].second)
		{
			data.bestPlayers[2] = data.bestPlayers[1];
			data.bestPlayers[1] = playerScore(username, score);
		}
		else if (score > data.bestPlayers[2].second)
		{
			data.bestPlayers[2] = playerScore(username, score);
		}

		
	}
	return data;
}



string StatisticsData::to_string()
{
	return this->bestPlayers[0].first + "," + this->bestPlayers[1].first + "," + this->bestPlayers[2].first + " ";
}
