#pragma once

#include <string>
#include <vector>
#include <utility>
#include "IDataBase.h"
#include "SqliteDataBase.h"
#include <sstream>


#define AMOUNT_OF_BEST_PLAYERS 3

using playerScore = std::pair<std::string, int>;

typedef struct PlayerStatistics
{
	std::string name;
	int numOfTotalAnswers;
	int numOfCorrectAnswers;
	int numOfWrongAnswers;
	int numOfGamesPlayed;
	double averageAnswerTime;
} PlayerStatistics;

typedef struct StatisticsData
{
	std::vector<PlayerStatistics> players;
	playerScore bestPlayers[AMOUNT_OF_BEST_PLAYERS];
	
	string to_string();
} StatisticsData;

class StatisticsManager
{
private:
	IDataBase* m_database;
	int calculateScore(PlayerStatistics ps);

public:
	void setDB(IDataBase* db);
	StatisticsData getStatistics();
};

