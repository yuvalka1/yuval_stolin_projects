import socket
import json
from enum import Enum

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 8820        # The port used by the server
username = ""

SIGNIN = 0
LOGIN = 1
SIGNOUT = 65
GET_ROOMS = 66
PLAYERS_IN_ROOM = 67
STATISTICS = 68
JOIN = 69
CREATE = 70

switcher = {
    1: SIGNOUT,
    2: GET_ROOMS,
    3: PLAYERS_IN_ROOM,
    4: JOIN,
    5: CREATE,
}


Connection = 115
Error=69
GetRooms=82
GetStatistics=116
GetPlayerInRoom=112


class Login:
    def __init__(self, username,password) :
        self.username = username
        self.password = password

class Signin:
    def __init__(self, username,password, email) :
        self.username = username
        self.password = password
        self.email  = email

class Room:
    def __init__(self, roomId):
        self.roomId = roomId

class CreateRoom:
    def __init__(self,roomName, maxUsers, questionCount, answerTimeout ):
        self.answerTimeout=answerTimeout
        self.roomName=roomName
        self.maxUsers= maxUsers
        self.questionCount=questionCount

def sendHello(s):
    data = s.recv(5)
    print('Received', repr(data))
    s.sendall(b'Hello')


def connectToTrivia(s):
    while (True):
        print("write 1 for login 0 signin\n")
        task = input()
        jsonStr = ""
        code = "1"
        if task == "1":
            print("enter your username and password")
            username = input()
            login = Login(username, input())
            jsonStr = json.dumps(login.__dict__)
            code = "0"
        elif task == "0":
            print("enter your username and password and email")
            sign = Signin(input(), input(), input())
            jsonStr = json.dumps(sign.__dict__)
        else:
            print("please select one of the options")

        msg = code.zfill(2) + str(len(jsonStr)).zfill(4) + jsonStr
        #print(msg)

        s.sendall(msg.encode())
        responseCode = int.from_bytes(s.recv(1), "big")


        lenght = int(s.recv(4))
        server_msg = s.recv(lenght)
        print(server_msg)
        if responseCode == Connection:
            break


def menu(s):
    while(True):
        print("""Please choose one option:
    1.SIGNOUT
    2.Get  Rooms
    3.Get Players In Room
    4.join room
    5.Create Room
        """)

        msg = ""
        jsonStr = ""
        code = switcher.get(int(input()), -1)
        if code == -1:
            print("please select one of the  options\n")
            continue
        else:
            if code == CREATE:
                myRoom = CreateRoom("yuvalRoom", 10, 10,60)
                jsonStr = json.dumps(myRoom.__dict__)
                msg = str(code) + str(len(jsonStr)).zfill(4) + jsonStr
            elif code == GET_ROOMS:
                msg = str(code) + "0000"
            else:
                print("enter room id")
                room = Room(int(input()))
                jsonStr = json.dumps(room.__dict__)
                print(jsonStr, code)
                msg = str(code) + str(len(jsonStr)).zfill(4) + jsonStr
            print(msg)

            s.sendall(msg.encode())
            responseCode = s.recv(1)
            lenght = int(s.recv(4))
            server_msg = s.recv(lenght)
            print('Received', responseCode, server_msg )


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        sendHello(s)
        while(True):
            connectToTrivia(s)
            menu(s)









if __name__ == '__main__':
    main()
