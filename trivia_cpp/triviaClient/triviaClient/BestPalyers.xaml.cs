﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for BestPalyers.xaml
    /// </summary>
    public partial class BestPalyers : Page
    {
        NetworkStream clientStream;
        public BestPalyers(NetworkStream clientStream)
        {
            this.clientStream = clientStream;
            InitializeComponent();
            string msg = ((int)UserOptions.STATISTICS).ToString() + "0000";
            Helper.sendData(clientStream, msg);
            int code = Helper.GetIntPartFromSocket(clientStream, 1);
            int len = Helper.GetIntPartFromSocket(clientStream, 4);
            string players = Helper.GetStringPartFromSocket(clientStream, len);
            if(code == (int)Responses.GetStatistics)
            {
                GetStatisticsResponse bestPalyers = JsonConvert.DeserializeObject<GetStatisticsResponse>(players);
                
                firstPlace.Text = bestPalyers._statistics[0];
                secondPlace.Text = bestPalyers._statistics[1];
                thirdPlace.Text = bestPalyers._statistics[2];
            }
            else
            {
                System.Windows.MessageBox.Show("Error");
            }


        }
    }
}
