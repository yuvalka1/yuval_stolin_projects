﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for GamesPage.xaml
    /// </summary>
    public partial class GamesPage : Page
    {
        NetworkStream clientStream;
        public GamesPage(NetworkStream clientStream)
        {
            this.clientStream = clientStream;
            InitializeComponent();
        }

        private void joinGame_Click(object sender, RoutedEventArgs e)
        {
            //should navigate to a rooms page 
            MessageBox.Show("click join");
        }

        private void createGame_Click(object sender, RoutedEventArgs e)
        {
            //need to open dialog to get ditails and then go to waiting room 
            MessageBox.Show("click create");
        }
    }
}
