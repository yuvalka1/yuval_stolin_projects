﻿using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace triviaClient
{
    class Helper
    {
        //fix ths function
        public static int GetIntPartFromSocket(NetworkStream clientStream, int bytesNum)
        {
            byte[] arr = GetPartFromSocket(clientStream, bytesNum);
            byte v = arr[0];

            return (int)Convert.ToChar(v);
        }
        public  static string GetStringPartFromSocket(NetworkStream clientStream, int bytesNum)
        {
            byte[] arr = GetPartFromSocket(clientStream, bytesNum);
            return System.Text.Encoding.Default.GetString(arr);
        }

        public static void sendData(NetworkStream clientStream, string data)
        {
            byte[] send = new ASCIIEncoding().GetBytes(data);
            clientStream.Write(send, 0, send.Length);
            clientStream.Flush();
        }

        private static byte[] GetPartFromSocket(NetworkStream clientStrem, int bytesNum)
        {
            byte[] arr = new byte[bytesNum];
            clientStrem.Read(arr, 0, bytesNum);
            return arr;
        }

    }
}
