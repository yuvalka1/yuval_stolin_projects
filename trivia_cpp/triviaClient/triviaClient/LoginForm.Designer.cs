﻿namespace triviaClient
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginPic = new System.Windows.Forms.PictureBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.signinLoginScreen = new System.Windows.Forms.PictureBox();
            this.signupLoginScreen = new System.Windows.Forms.PictureBox();
            this.signupPic = new System.Windows.Forms.PictureBox();
            this.nameSignup = new System.Windows.Forms.TextBox();
            this.emailSignup = new System.Windows.Forms.TextBox();
            this.passwordSignup = new System.Windows.Forms.TextBox();
            this.signinSignupScreen = new System.Windows.Forms.PictureBox();
            this.signupSignupScreen = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.loginPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signinLoginScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signupLoginScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signupPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signinSignupScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signupSignupScreen)).BeginInit();
            this.SuspendLayout();
            // 
            // loginPic
            // 
            this.loginPic.Enabled = false;
            this.loginPic.Image = global::triviaClient.Properties.Resources.loginFinalPic;
            this.loginPic.Location = new System.Drawing.Point(-5, 0);
            this.loginPic.Name = "loginPic";
            this.loginPic.Size = new System.Drawing.Size(816, 457);
            this.loginPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loginPic.TabIndex = 0;
            this.loginPic.TabStop = false;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(172, 225);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(215, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(189, 268);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(198, 20);
            this.passwordTextBox.TabIndex = 2;
            // 
            // signinLoginScreen
            // 
            this.signinLoginScreen.Image = global::triviaClient.Properties.Resources.signin1;
            this.signinLoginScreen.Location = new System.Drawing.Point(145, 360);
            this.signinLoginScreen.Name = "signinLoginScreen";
            this.signinLoginScreen.Size = new System.Drawing.Size(183, 50);
            this.signinLoginScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.signinLoginScreen.TabIndex = 3;
            this.signinLoginScreen.TabStop = false;
            // 
            // signupLoginScreen
            // 
            this.signupLoginScreen.Image = global::triviaClient.Properties.Resources.signup1;
            this.signupLoginScreen.Location = new System.Drawing.Point(570, 268);
            this.signupLoginScreen.Name = "signupLoginScreen";
            this.signupLoginScreen.Size = new System.Drawing.Size(187, 50);
            this.signupLoginScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.signupLoginScreen.TabIndex = 4;
            this.signupLoginScreen.TabStop = false;
            // 
            // signupPic
            // 
            this.signupPic.Enabled = false;
            this.signupPic.Image = global::triviaClient.Properties.Resources.signinFinal;
            this.signupPic.Location = new System.Drawing.Point(-5, 0);
            this.signupPic.Name = "signupPic";
            this.signupPic.Size = new System.Drawing.Size(816, 457);
            this.signupPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.signupPic.TabIndex = 5;
            this.signupPic.TabStop = false;
            this.signupPic.Click += new System.EventHandler(this.SigninSignupScreen_Click);
            // 
            // nameSignup
            // 
            this.nameSignup.Location = new System.Drawing.Point(482, 216);
            this.nameSignup.Name = "nameSignup";
            this.nameSignup.Size = new System.Drawing.Size(224, 20);
            this.nameSignup.TabIndex = 6;
            // 
            // emailSignup
            // 
            this.emailSignup.Location = new System.Drawing.Point(482, 268);
            this.emailSignup.Name = "emailSignup";
            this.emailSignup.Size = new System.Drawing.Size(224, 20);
            this.emailSignup.TabIndex = 7;
            // 
            // passwordSignup
            // 
            this.passwordSignup.Location = new System.Drawing.Point(511, 312);
            this.passwordSignup.Name = "passwordSignup";
            this.passwordSignup.Size = new System.Drawing.Size(195, 20);
            this.passwordSignup.TabIndex = 8;
            // 
            // signinSignupScreen
            // 
            this.signinSignupScreen.Image = global::triviaClient.Properties.Resources.signin2;
            this.signinSignupScreen.Location = new System.Drawing.Point(55, 268);
            this.signinSignupScreen.Name = "signinSignupScreen";
            this.signinSignupScreen.Size = new System.Drawing.Size(182, 50);
            this.signinSignupScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.signinSignupScreen.TabIndex = 9;
            this.signinSignupScreen.TabStop = false;
            // 
            // signupSignupScreen
            // 
            this.signupSignupScreen.Image = global::triviaClient.Properties.Resources.signup2;
            this.signupSignupScreen.Location = new System.Drawing.Point(466, 351);
            this.signupSignupScreen.Name = "signupSignupScreen";
            this.signupSignupScreen.Size = new System.Drawing.Size(190, 50);
            this.signupSignupScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.signupSignupScreen.TabIndex = 10;
            this.signupSignupScreen.TabStop = false;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.passwordSignup);
            this.Controls.Add(this.emailSignup);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.nameSignup);
            this.Controls.Add(this.signupLoginScreen);
            this.Controls.Add(this.loginPic);
            this.Controls.Add(this.signupPic);
            this.Controls.Add(this.signinSignupScreen);
            this.Controls.Add(this.signinLoginScreen);
            this.Controls.Add(this.signupSignupScreen);
            this.Name = "LoginForm";
            this.Text = "LoginForm";
            ((System.ComponentModel.ISupportInitialize)(this.loginPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signinLoginScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signupLoginScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signupPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signinSignupScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signupSignupScreen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox loginPic;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.PictureBox signinLoginScreen;
        private System.Windows.Forms.PictureBox signupLoginScreen;
        private System.Windows.Forms.PictureBox signupPic;
        private System.Windows.Forms.TextBox nameSignup;
        private System.Windows.Forms.TextBox emailSignup;
        private System.Windows.Forms.TextBox passwordSignup;
        private System.Windows.Forms.PictureBox signinSignupScreen;
        private System.Windows.Forms.PictureBox signupSignupScreen;
    }
}