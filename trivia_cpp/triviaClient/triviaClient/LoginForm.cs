﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;

namespace triviaClient
{

    public partial class LoginForm : Form
    {
       
        private NetworkStream clientStream;
        private string username;
        private bool connected = false;

        public bool getConnected => connected;
       
        public LoginForm(NetworkStream stream) 
        {
           
            InitializeComponent();
            this.clientStream = stream;
            this.signinLoginScreen.Click += SigninSigninScreen_Click;
            this.signupLoginScreen.Click += SingupLoginScreen_Click;
            this.signinSignupScreen.Click += SigninSignupScreen_Click;
            this.signupSignupScreen.Click += SignupSignupScreen_Click;
            switchPages(true);
            
        }

        private void switchPages(bool v)
        {
            this.signupSignupScreen.Visible = !v;
            this.signinSignupScreen.Visible = !v;
            this.nameSignup.Visible = !v;
            this.emailSignup.Visible = !v;
            this.passwordSignup.Visible = !v;

            this.signinLoginScreen.Visible = v;
            this.signupLoginScreen.Visible = v;
            this.nameTextBox.Visible = v;
            this.passwordTextBox.Visible = v;

            this.loginPic.Visible = v;
            this.signupPic.Visible = !v;


            
        }
        public string GetUsername => username;

        private void SignupSignupScreen_Click(object sender, EventArgs e)
        {
            if(nameSignup.Text != "" &&  passwordSignup.Text != "" && emailSignup.Text!="")
            {
                SigninStruct signinStruct = new SigninStruct(nameSignup.Text, passwordSignup.Text, emailSignup.Text);
                string result = JsonConvert.SerializeObject(signinStruct);
                string msg_to_server = "0" + ((int)UserOptions.SIGNIN).ToString() + result.Length.ToString().PadLeft(4, '0') + result;

                Helper.sendData(this.clientStream, msg_to_server);
                int code = Helper.GetIntPartFromSocket(this.clientStream, sizeof(byte));
                int len = Helper.GetIntPartFromSocket(this.clientStream, sizeof(int));
                string response = Helper.GetStringPartFromSocket(this.clientStream, len);

                if(code == (int)(Responses.Connection))
                {
                    connected = true;
                    this.username = nameSignup.Text;
                    this.Close();
                }
                else
                {

                    System.Windows.MessageBox.Show("Please choose another username, htis one is alredy taken");
                }
            }
            else
            {
                System.Windows.MessageBox.Show("You have to fill all the information");
            }
        }

        private void SigninSignupScreen_Click(object sender, EventArgs e)
        {
            switchPages(true);
        }

       


        private void SingupLoginScreen_Click(object sender, EventArgs e)
        {
            switchPages(false);
        }

        //finish this function
        private void SigninSigninScreen_Click(object sender, EventArgs e)
        {
            if (this.passwordTextBox.Text != "" && this.nameTextBox.Text != "")
            {
                LoginStruct loginStruct = new LoginStruct(this.nameTextBox.Text, this.passwordTextBox.Text);
                string result = JsonConvert.SerializeObject(loginStruct);
                string msg_to_server = "0" + ((int)UserOptions.LOGIN).ToString() + result.Length.ToString().PadLeft(4, '0') + result;
                Helper.sendData(this.clientStream, msg_to_server);

                int code = Helper.GetIntPartFromSocket(this.clientStream, sizeof(byte));
                int len = Helper.GetIntPartFromSocket(this.clientStream, sizeof(int));
                string response = Helper.GetStringPartFromSocket(this.clientStream, len);

                if (code == (int)(Responses.Connection))
                {
                    connected = true;
                    this.username = nameTextBox.Text;
                    this.Close();
                }
                else
                {

                    System.Windows.MessageBox.Show("wrong username or password");
                }

            }

            else
            {
                System.Windows.MessageBox.Show("You have to fill all the information");
            }
        }

        

    }
}
