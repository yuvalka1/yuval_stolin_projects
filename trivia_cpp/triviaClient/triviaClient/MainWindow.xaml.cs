﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NetworkStream clientStream;
        string username;
        bool home = true;
        public MainWindow()
        {
           
            InitializeComponent();
            
            try
            {
                ConnectToServer();
                manageMainWindow();
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                this.Close();
            }
           
            
        }

        private void manageMainWindow()
        {
           
            LoginForm login = new LoginForm(clientStream);
            Hide();
            login.ShowDialog();
            Show();
            if (login.getConnected)
            {
                username = login.GetUsername;
            }
            else
            {
                this.Close();
            }
            GC.Collect();
            
           
        }

        private void ConnectToServer()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            this.clientStream = client.GetStream();
            Console.WriteLine(Helper.GetStringPartFromSocket(this.clientStream, 5));
            Helper.sendData(clientStream, "hello");
           
        }

        private void menu_Home_Click(object sender, RoutedEventArgs e)
        {
            if(!home)
            {
                
                home = true;
                Frame.NavigationService.Navigate(new HomePage());
            }
        }


        private void menu_Games_Click(object sender, RoutedEventArgs e)
        {
            home = false;
            Frame.NavigationService.Navigate(new GamesPage(clientStream));
            
        }

        private void menu_Statistics_Click(object sender, RoutedEventArgs e)
        {
            home = false;
            Frame.NavigationService.Navigate(new BestPalyers(clientStream));
            
        }

        private void menu_Contact_Click(object sender, RoutedEventArgs e)
        {
            home = false;
            Frame.NavigationService.Navigate(new ContactPage());

        }

        private void menu_Logout_Click(object sender, RoutedEventArgs e)
        {
            string msg = ((int)UserOptions.LOGOUT).ToString() + "0000";
            Helper.sendData(clientStream, msg);
           
            manageMainWindow();
        }

        private void Frame_Loaded(object sender, RoutedEventArgs e)
        {
            Frame.NavigationService.Navigate(new HomePage());
        }
    }
}
