﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace triviaClient
{
    public struct LoginStruct
    {
        public string username;
        public string password;

        public LoginStruct(string name, string pass)
        {
            username = name;
            password = pass;
        }
    }

    public struct SigninStruct
    {
        public string username;
        public string password;
        public string email;
        public SigninStruct(string name, string pass, string email)
        {
            username = name;
            password = pass;
            this.email = email;
        }
    }

    public struct Room
    {
        public int roomId;
        public Room(int id)
        {
            roomId = id;
        }
    }


  

    public enum UserOptions : int
    {
        SIGNIN = 1,
        LOGIN = 0,
        LOGOUT = 65,
        GET_ROOMS = 66,
        PLAYERS_IN_ROOM = 67,
        STATISTICS = 68,
        JOIN = 69,
        CREATE = 70
    }

    public enum Responses : int
    {
        Connection = 115,
        Error = 69,
        GetRooms = 82,
        GetStatistics = 116,
        GetPlayerInRoom = 112
    }

    public struct GetStatisticsResponse
    {
        public int _status;
        public List<string> _statistics;
        public GetStatisticsResponse(int status, List<string> statistics)
        {
            this._statistics = statistics;
            this._status = status;
        }
    }
    


}
